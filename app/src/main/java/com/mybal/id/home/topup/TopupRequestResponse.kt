package com.mybal.id.home.topup

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TopupRequestResponse {

    @SerializedName("statusCode")
    @Expose
    var statusCode: Int = 0
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: List<TopupRequest>? = null

    inner class TopupRequest {
        @SerializedName("bank_code")
        @Expose
        var bankCode: String? = null
        @SerializedName("va_number")
        @Expose
        var vaNumber: String? = null
        @SerializedName("target_payment_amount")
        @Expose
        var targetPaymentAmount: Int = 0
        @SerializedName("expiration_time")
        @Expose
        var expirationTime: String? = null
    }
}
