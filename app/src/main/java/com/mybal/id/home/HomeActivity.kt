package com.mybal.id.home

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.facebook.accountkit.Account
import com.facebook.accountkit.AccountKit
import com.facebook.accountkit.AccountKitCallback
import com.facebook.accountkit.AccountKitError
import com.mybal.id.BuildConfig
import com.mybal.id.R
import com.mybal.id.accessibility.AccessibilityGuideActivity
import com.mybal.id.accessibility.MyBalAccessibilityService
import com.mybal.id.helpsupport.HelpSupportActivity
import com.mybal.id.home.activity.ActivityFragment
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.home.profile.ProfileFragment
import com.mybal.id.home.topup.PulsaTopupFragment
import com.mybal.id.invite.InviteActivity
import com.mybal.id.settings.SettingsActivity
import com.mybal.id.simhistory.SimHistoryActivity
import com.mybal.id.util.Constants
import com.mybal.id.util.RxBus
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.widget_toolbar.*

class HomeActivity : AppCompatActivity() {

    private var drawerToggle: ActionBarDrawerToggle? = null
    private var accessibilityPromptDialog: AlertDialog? = null

    /**
     * Check whether the accessibility popup exists and is shown. Then, check the system's setting,
     * if the Accessibility Permission hasn't been granted, show the popup.
     */
    private fun promptAccessibility() {

        // If user clicks Settings from the Home Screen, make sure after the accessibility is turned on
        // user comes back to the Home Screen.
        getSharedPreferences(Constants.PREF_KEY_IS_FIRST_ACCESSIBILITY_CONNECT, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(Constants.PREF_KEY_IS_FIRST_ACCESSIBILITY_CONNECT, true)
                .apply()

        // Build a dialog and show it if it hasn't shown
        if (accessibilityPromptDialog == null)
            accessibilityPromptDialog = AlertDialog.Builder(this)
                    .setTitle(getString(R.string.balanceHome_accessRequired))
                    .setMessage(getString(R.string.balanceHome_enableAccessInstruction))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.balanceHome_gotoSettings)) { dialog, _ ->

                        dialog.dismiss()
                        startActivity(Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS))

                        // When debugging, works only on API <= 22
                        // https://stackoverflow.com/a/36019034
                        // Will fail if .apk is sideloaded (installed standalone)
                        // Must be uploaded to the Play Store for automatic permission grant
                        startActivity(Intent(applicationContext, AccessibilityGuideActivity::class.java))

                    }.create()
        if (!accessibilityPromptDialog!!.isShowing)
            accessibilityPromptDialog!!.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setupToolbar()
        setupNavHeader()
        setupBottomBar()

        if (BuildConfig.BUILD_TYPE == "uiDev") {
            bottomBar.selectTabAtPosition(1)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        drawerToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navDrawer_open, R.string.navDrawer_close)
        drawerLayout.addDrawerListener(drawerToggle!!)
        navigationView.itemIconTintList = null // Disable Navigation Drawer icons coloring
        navigationView.setNavigationItemSelectedListener { item ->
            selectDrawerItem(item)
            true
        }
    }

    private fun setupNavHeader() {

        val headerView = navigationView.getHeaderView(0)
        val loginButton = headerView.findViewById<Button>(R.id.navHeader_Button_loginRegister)
        loginButton.setOnClickListener { startActivity(Intent(this@HomeActivity, LoginActivity::class.java)) }

        val accountContainer = headerView.findViewById<LinearLayout>(R.id.navHeader_LL_accountDetailContainer)
        val username = headerView.findViewById<TextView>(R.id.navHeader_TV_username)

        AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
            override fun onSuccess(account: Account) {
                loginButton.visibility = View.GONE
                accountContainer.visibility = View.VISIBLE
                username.text = account.phoneNumber.toString()
            }

            override fun onError(accountKitError: AccountKitError) {
                accountContainer.visibility = View.GONE
                loginButton.visibility = View.VISIBLE
            }
        })
    }

    private fun setupBottomBar() {
        bottomBar.setOnTabSelectListener { tabId ->
            when (tabId) {

                R.id.bottomBar_Home -> {
                    changeFragment(BalanceHomeFragment.newInstance())
                    supportActionBar!!.title = getString(R.string.actionBarTitle_balanceCheck)
                }

                R.id.bottomBar_Topup -> {
                    changeFragment(PulsaTopupFragment.newInstance())
                    supportActionBar!!.title = getString(R.string.actionBarTitle_topup)
                }

                R.id.bottomBar_Profile -> {
                    changeFragment(ProfileFragment.newInstance())
                    supportActionBar!!.title = getString(R.string.actionBarTitle_profile)
                }

                R.id.bottomBar_Activity -> {
                    changeFragment(ActivityFragment.newInstance())
                    supportActionBar!!.title = getString(R.string.actionBarTitle_activity)
                }
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToggle!!.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle!!.onConfigurationChanged(newConfig)
    }

    override fun onResume() {
        super.onResume()

        // Ensure the Accessibility is enabled
        if (!MyBalAccessibilityService.isServiceEnabled(this@HomeActivity)) {
            if (BuildConfig.BUILD_TYPE != "uiDev")
                promptAccessibility()
        }

        // By default, select "Home" from NavDrawer
        navigationView.menu.getItem(0).isChecked = true

        // Close the Floating Guide if it's shown
        RxBus.getInstance().publish(RxBus.Payload(RxBus.FLAG_CLOSE_ACCESSIBILITY_FLOATING_GUIDE, true))
    }

    private fun selectDrawerItem(item: MenuItem) {
        when (item.itemId) {

            R.id.navDrawer_Home ->
                changeFragment(BalanceHomeFragment.newInstance())

            R.id.navDrawer_SimUsage ->
                startActivity(Intent(this, SimHistoryActivity::class.java))

            R.id.navDrawer_HelpSupport ->
                startActivity(Intent(this, HelpSupportActivity::class.java))

            R.id.navDrawer_Settings ->
                startActivity(Intent(this, SettingsActivity::class.java))

            R.id.navDrawer_RateUs ->
                RateUsDialogFragment.newInstance().show(supportFragmentManager, "rateUs")

            R.id.navDrawer_Invite ->
                startActivity(Intent(this, InviteActivity::class.java))

            else -> {
            }
        }

        title = item.title
        drawerLayout.closeDrawers()
    }

    /**
     * Change the current fragment into the one selected.
     */
    private fun changeFragment(fragment: Fragment) {
        // https://stackoverflow.com/a/28845153
        if (!isFinishing) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainerFL, fragment)
                    .commitAllowingStateLoss()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (bottomBar.currentTabPosition != 0) {
            bottomBar.selectTabAtPosition(0)
        } else {
            super.onBackPressed()
        }
    }
}
