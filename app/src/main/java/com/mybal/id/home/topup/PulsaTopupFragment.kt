package com.mybal.id.home.topup

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mybal.id.R
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.network.RestClient
import com.mybal.id.util.Magic
import kotlinx.android.synthetic.main.fragment_pulsa_topup.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PulsaTopupFragment : Fragment() {

    private var phoneNumber: String? = null
    private var topupCodeItem: TopupCodeItem? = null
    private var bankCode: String? = null

    companion object {
        fun newInstance(): PulsaTopupFragment {
            return PulsaTopupFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pulsa_topup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val db: MyBalDatabase = MyBalDatabase.getInstance(activity)

        super.onViewCreated(view, savedInstanceState)

        phoneNumberET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {

                // In the cardNumberPrefix, the String is 5-character
                phoneNumber = if (p0.toString().length >= 5) p0.toString() else ""
                topupCodeItem = null
                promptPackSelection()
            }
        })

        purchasePackageCV.setOnClickListener({

            if (phoneNumber == null || phoneNumber == "" || phoneNumber!!.substring(0, 3) != "628" || phoneNumber!!.length < 10) {

                phoneNumberET.error = "Please enter a valid number (starts with 628)"

            } else {

                val cardId = db.cardNumberPrefixDao().getCardId(phoneNumber!!.substring(0, 5))
                val providerId = db.cardDao().getProviderFromCard(cardId)

                TopupNominalsDialogFragment.newInstance(providerId)
                        .show(childFragmentManager, "topupNominal")
            }
        })

        setupBanks()

        payButton.setOnClickListener({
            when {
                phoneNumber == null || phoneNumber == "" || phoneNumber!!.substring(0, 3) != "628" || phoneNumber!!.length < 10 ->
                    phoneNumberET.error = "Please enter a valid number (starts with 628)"
                topupCodeItem == null -> {
                    selectRechargeAmountTV.setTextColor(ContextCompat.getColor(context!!, R.color.palleteRed))
                    Toast.makeText(context, "Please select the amount of topup", Toast.LENGTH_SHORT).show()
                }
                bankCode == null ->
                    Toast.makeText(context, "Please select the bank first", Toast.LENGTH_SHORT).show()
                else ->
                    requestTopup()
            }
        })
    }

    private fun requestTopup() {

        Log.i("###", "phone number: " + phoneNumber + ", code: " + topupCodeItem!!.topupCode.oprCode!! + ", bankCode: " + bankCode!!)

        RestClient.getClient(activity).requestTopup(
                phoneNumber!!,
                topupCodeItem!!.topupCode.oprCode!!,
                "bank_transfer",
                bankCode!!
        ).enqueue(object : Callback<TopupRequestResponse> {
            override fun onFailure(call: Call<TopupRequestResponse>?, t: Throwable?) {
                Toast.makeText(activity!!, "Request failed due to " + t!!.localizedMessage + ". Please try again.", Toast.LENGTH_SHORT).show() // todo add proper error handling
            }

            override fun onResponse(call: Call<TopupRequestResponse>?, response: Response<TopupRequestResponse>) {
                if (response.isSuccessful) {

                    when (response.body()!!.statusCode) {
                        99 ->  // Invalid phone number
                            AlertDialog.Builder(activity!!)
                                    .setTitle("Invalid Phone Number")
                                    .setMessage("Please fill a valid Indonesian phone number (10-13 digit numeric).")
                                    .setPositiveButton("OK", { dialogInterface, _ -> dialogInterface.dismiss() })
                                    .show()
                        98 ->
                            AlertDialog.Builder(activity!!)
                                    .setTitle("Invalid Topup Code")
                                    .setMessage("Your topup code is invalid. Please refresh the screen and try again once more.")
                                    .setPositiveButton("OK", { dialogInterface, _ -> dialogInterface.dismiss() })
                                    .show()
                        97 -> {
                            val topupRequest = response.body()!!.data!![0]
                            AlertDialog.Builder(activity!!)
                                    .setTitle("Pending Transaction")
                                    .setMessage("You must finish this pending transaction first, Rp" + topupRequest.targetPaymentAmount + " to account number " + Magic.insertPeriodically(topupRequest.vaNumber, "", 4) + ".")
                                    .setPositiveButton("OK", { dialogInterface, _ -> dialogInterface.dismiss() })
                                    .show()
                        }

                        0 -> {
                            val topupRequest = response.body()!!.data!![0]
                            AlertDialog.Builder(activity!!)
                                    .setTitle("Request Success!")
                                    .setMessage("Please transfer Rp" + topupRequest.targetPaymentAmount + " to account number " + Magic.insertPeriodically(topupRequest.vaNumber, "", 4) + ".")
                                    .setPositiveButton("OK", { dialogInterface, _ -> dialogInterface.dismiss() })
                                    .show()
                        }

                    }

                } else {
                    Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setupBanks() {
        val banks = listOf(
                BankChannelItem(BankChannelItem.BANK_CODE_MANDIRI),
                BankChannelItem(BankChannelItem.BANK_CODE_BCA),
                BankChannelItem(BankChannelItem.BANK_CODE_BNI),
                BankChannelItem(BankChannelItem.BANK_CODE_KEB_HANA),
                BankChannelItem(BankChannelItem.BANK_CODE_PERMATA),
                BankChannelItem(BankChannelItem.BANK_CODE_BII_MAYBANK),
                BankChannelItem(BankChannelItem.BANK_CODE_CIMB),
                BankChannelItem(BankChannelItem.BANK_CODE_BRI),
                BankChannelItem(BankChannelItem.BANK_CODE_DANAMON)
        )

        val itemAdapter: ItemAdapter<IItem<*, *>> = ItemAdapter()
        val fastAdapter = FastAdapter.with<IItem<*, *>, ItemAdapter<IItem<*, *>>>(itemAdapter)

        banksRV.layoutManager = GridLayoutManager(activity, 2)
        banksRV.setHasFixedSize(true)
        banksRV.adapter = fastAdapter
        itemAdapter.add(banks)

        fastAdapter.withSelectable(true)
        fastAdapter.withOnClickListener { _, adapter, item, _ ->
            item as BankChannelItem

            if (!item.isChosen) {

                adapter.adapterItems.forEach { (it as BankChannelItem).isChosen = false }
                item.isChosen = true
                bankCode = item.bankCode

                fastAdapter.notifyAdapterDataSetChanged()
            }
            true
        }
    }

    fun promptPackSelection() {
        rechargeInfoLL.visibility = View.INVISIBLE
        selectRechargeAmountTV.visibility = View.VISIBLE
    }

    fun showTopupCodeItem(topupCodeItem: TopupCodeItem) {

        this.topupCodeItem = topupCodeItem

        selectRechargeAmountTV.visibility = View.GONE
        rechargeInfoLL.visibility = View.VISIBLE
        pulsaDenominationTV.text = topupCodeItem.topupCode.amount.toString()
        priceTV.text = topupCodeItem.topupCode.price.toString()
    }
}