package com.mybal.id.home.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mybal.id.R
import kotlinx.android.synthetic.main.fragment_notifupdate.*

class NotifUpdateFragment : Fragment() {

    companion object {

        val TYPE_NOTIFICATION = 0
        val TYPE_UPDATE = 1

        private val ACTIVITY_TYPE = "com.mybal.id.activity_type"

        fun newInstance(notifUpdateType: Int): NotifUpdateFragment {

            val args = Bundle()
            args.putInt(ACTIVITY_TYPE, notifUpdateType)

            val fragment = NotifUpdateFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notifupdate, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadNotifs()
    }

    private fun loadNotifs() {
        notifsRV.visibility = View.GONE
        zeroNotifLL.visibility = View.VISIBLE
        val activityType = arguments?.getInt(ACTIVITY_TYPE) ?: TYPE_NOTIFICATION
        zeroNotifTV.text =
                when (activityType) {
                    TYPE_NOTIFICATION ->
                        getString(R.string.no_new_notification)
                    TYPE_UPDATE ->
                        getString(R.string.no_new_update)
                    else ->
                        ""
                }
    }
}
