package com.mybal.id.home.topup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletHistoryItem {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("users_id")
    @Expose
    public int usersId;
    @SerializedName("type")
    @Expose
    public int type;
    @SerializedName("out")
    @Expose
    public Object out;
    @SerializedName("in")
    @Expose
    public int in;
    @SerializedName("current_balance")
    @Expose
    public int currentBalance;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

}
