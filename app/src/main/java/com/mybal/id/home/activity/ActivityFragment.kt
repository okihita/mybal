package com.mybal.id.home.activity

import android.os.Bundle

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mybal.id.R

import kotlinx.android.synthetic.main.fragment_activity.*

class ActivityFragment : Fragment() {

    companion object {
        fun newInstance(): ActivityFragment {
            return ActivityFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTabs()
    }

    private fun setupTabs() {
        viewPager.adapter = FragmentPagerAdapter(childFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

    inner class FragmentPagerAdapter internal constructor(fm: FragmentManager) : android.support.v4.app.FragmentPagerAdapter(fm) {

        private val pageCount = 2
        private val tabTitles = arrayOf(getString(R.string.activity_tabTitle_notifications), getString(R.string.activity_tabTitle_updates))

        override fun getCount(): Int {
            return pageCount
        }

        override fun getItem(position: Int): Fragment? {

            return when (position) {

                NotifUpdateFragment.TYPE_NOTIFICATION ->
                    NotifUpdateFragment.newInstance(NotifUpdateFragment.TYPE_NOTIFICATION)

                NotifUpdateFragment.TYPE_UPDATE ->
                    NotifUpdateFragment.newInstance(NotifUpdateFragment.TYPE_UPDATE)

                else ->
                    null
            }
        }

        override fun getPageTitle(position: Int): CharSequence {
            return tabTitles[position]
        }
    }
}
