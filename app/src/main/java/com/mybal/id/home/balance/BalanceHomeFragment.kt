package com.mybal.id.home.balance

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.TELEPHONY_SERVICE
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mybal.id.BuildConfig
import com.mybal.id.R
import com.mybal.id.db.Device
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.db.Provider
import com.mybal.id.network.RestClient
import com.mybal.id.util.Constants
import com.mybal.id.util.SessionManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BalanceHomeFragment : Fragment() {

    private var provider1 = Provider()
    private var provider2 = Provider()

    companion object {

        // Intentionally uses 0 and 1 to comply with the hardware's SIM slot index
        // Don't ever change, please
        const val FIRST_SLOT = 0
        const val SECOND_SLOT = 1

        fun newInstance(): BalanceHomeFragment {
            return BalanceHomeFragment()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_balance_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        retrieveSimInfos()
        profileDevice()
        SessionManager.checkTokenValidity(context!!)

        if (BuildConfig.BUILD_TYPE == "uiDev")
            childFragmentManager.beginTransaction()
                    .add(R.id.cardSlot1, BalanceCardFragment.newInstance(FIRST_SLOT, provider1.id))
                    .commit()
        else
            childFragmentManager.beginTransaction()
                    .add(R.id.cardSlot1, BalanceCardFragment.newInstance(FIRST_SLOT, provider1.id))
                    .add(R.id.cardSlot2, BalanceCardFragment.newInstance(SECOND_SLOT, provider2.id))
                    .commit()
    }


    // https://developer.android.com/about/versions/android-5.1.html#multisim
    @SuppressLint("NewApi")
    private fun retrieveSimInfos() {

        // For Android 5.1 (API level 22) and above, there's an official dual-SIM API
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {

            val infos = SubscriptionManager.from(activity).activeSubscriptionInfoList
            val providerIdPref = activity!!
                    .getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE)

            if (infos != null)
                if (infos.size > 0) { // If there's at least one SIM
                    provider1 = Provider(infos[0])
                    providerIdPref.edit().putInt(Constants.PROVIDER_ID_SLOT_1, provider1.id).apply()

                    if (infos.size > 1) { // If there's at least two SIMs
                        provider2 = Provider(infos[1])
                        providerIdPref.edit().putInt(Constants.PROVIDER_ID_SLOT_2, provider2.id).apply()
                    }
                }
        }

        // For Android KitKat and below, the official API only detects the first SIM card slot
        else {
            val tel = activity!!.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
            val networkOperator = tel.networkOperator
            if (networkOperator != null) {

                val mcc = Integer.parseInt(networkOperator.substring(0, 3))
                val mnc = Integer.parseInt(networkOperator.substring(3))

                MyBalDatabase.getInstance(activity).providerDao().getProviderByMccMnc(mcc, mnc)
            }
        }
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    private fun profileDevice() {

        // If this is the first run of this particular installation
        if (SessionManager.getAuthToken(activity!!) == "") {

            val telephonyManager = activity!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            val imsi = telephonyManager.subscriberId
            val imei = telephonyManager.deviceId
            val simSerial = telephonyManager.simSerialNumber

            val androidId: String = "" + android.provider.Settings.Secure.getString(activity!!.contentResolver, android.provider.Settings.Secure.ANDROID_ID)
            RestClient.getClient(activity)
                    .registerDeviceSilently(imei, "", androidId, provider1.id, provider2.id, imsi, simSerial)
                    .enqueue(object : Callback<Device> {
                        override fun onResponse(call: Call<Device>, response: Response<Device>) {
                            if (response.isSuccessful) {
                                response.body()?.let {
                                    Log.i("###", "newly registered token: " + it.tokenAuth);
                                    SessionManager.storeAuthToken(activity!!, it.tokenAuth)
                                }
                            } else {
                                Log.i("###", "token registration failed: ");
                            }
                        }

                        override fun onFailure(call: Call<Device>, t: Throwable) {
                            Log.i("###", "Fail: " + t.localizedMessage)
                        }
                    })
        }
    }
}