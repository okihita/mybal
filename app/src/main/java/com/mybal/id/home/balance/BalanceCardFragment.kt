package com.mybal.id.home.balance

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.animation.DynamicAnimation
import android.support.animation.SpringAnimation
import android.support.animation.SpringForce
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.telecom.TelecomManager
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mybal.id.BuildConfig
import com.mybal.id.R
import com.mybal.id.db.*
import com.mybal.id.network.RestClient
import com.mybal.id.settings.ChangeSimTypeDialogFragment
import com.mybal.id.settings.SettingsActivity
import com.mybal.id.settings.SimSettingsFragment
import com.mybal.id.util.Constants
import com.mybal.id.util.Magic
import com.mybal.id.util.MyBalApp
import com.mybal.id.util.RxBus
import io.reactivex.Observer
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_balance_card.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat
import java.text.NumberFormat

class BalanceCardFragment : Fragment() {

    // For informations inside the card
    private val maxPulldownDistance = 100f

    private var simSlot = 0
    private var providerId = 0

    private var pulsaBarOrigin = 0f
    private var pulsaBarTouchOrigin: Float = 0.toFloat()
    private var donutOrigin = 0f
    private var donutTouchOrigin: Float = 0.toFloat()
    private var lastTouchAction: Int = 0

    private var hasUssdReceived = false
    private var ussdBalanceTypeCalled = 0
    private var shownUssdBalanceType = 0

    companion object {

        private val ARGS_KEY_PROVIDER_ID = "com.mybal.id.provider_id"
        private val ARGS_KEY_CARD_SLOT = "com.mybal.id.card_slot"

        fun newInstance(cardSlot: Int, providerId: Int): BalanceCardFragment {
            val fragment = BalanceCardFragment()
            val args = Bundle()
            args.putInt(ARGS_KEY_PROVIDER_ID, providerId)
            args.putInt(ARGS_KEY_CARD_SLOT, cardSlot)
            fragment.arguments = args
            return fragment
        }
    }

    private val ussdSmsMessageObserver = object : Observer<RxBus.Payload> {

        override fun onSubscribe(@NonNull d: Disposable) {}

        override fun onNext(@NonNull payload: RxBus.Payload) {

            when (payload.requestCode) {

                RxBus.FLAG_REQUEST_BALANCE_INCOMING_USSD ->

                    if (!hasUssdReceived) {

                        hasUssdReceived = true

                        if (payload.stringPayload.toLowerCase().contains("connection problem")) {

                            Toast.makeText(activity, "Failed to retrieve message. Please try again.", Toast.LENGTH_LONG).show()

                        } else {

                            sendUssdMessageToServer(payload.stringPayload, providerId)
                            val parsedProviderMessage = parseProviderMessage(payload.stringPayload)

                            // Popup screen for internal use
                            @Suppress("ConstantConditionIf")
                            if (BuildConfig.BUILD_TYPE == "teamRelease" || BuildConfig.BUILD_TYPE == "debug") {
                                Magic.showPopup(activity, parsedProviderMessage,
                                        payload.stringPayload, providerId, ussdBalanceTypeCalled)
                            }

                            // Save the processed USSD into a SIM History item
                            Magic.saveIntoSimUsage(activity, simSlot, payload.stringPayload, parsedProviderMessage)

                            when (ussdBalanceTypeCalled) {

                                Message.BALANCE_TYPE_PULSA_MAIN,
                                Message.BALANCE_TYPE_PULSA_PROMOTION ->
                                    refreshPulsaBalanceDisplay()

                                else ->
                                    refreshDonutBalanceDisplay()
                            }
                        }
                    }

                RxBus.FLAG_REQUEST_BALANCE_INCOMING_SMS ->
                    refreshDonutBalanceDisplay()

                else -> {
                }
            }
        }

        override fun onError(@NonNull e: Throwable) {}

        override fun onComplete() {}
    }

    private fun sendUssdMessageToServer(messageContent: String, providerId: Int) {

        val myBalApi = RestClient.getClient(context)

        val message = Message()
        message.textUserId = Message.DEFAULT_UNKNOWN_USER_ID
        message.textRecieveType = Message.RECEIVE_TYPE_USSD
        message.textContent = messageContent
        message.textSender = Message.USSD_SENDER_ID
        message.textUssd = MyBalDatabase.getInstance(activity).ussdCodeDao().getUssdCode(providerId, ussdBalanceTypeCalled).code
        message.textProviderId = providerId

        myBalApi.postMessage(message).enqueue(object : Callback<Message> {
            override fun onResponse(call: Call<Message>, response: Response<Message>) {
                Log.d("###", "onResponse() called with: call = [$call], response = [$response]")
            }

            override fun onFailure(call: Call<Message>, t: Throwable) {
                Log.d("###", "onFailure() called with: call = [$call], t = [$t]")
            }
        })
    }

    private fun refreshPulsaBalanceDisplay() {

        if (isAdded) {
            val localDecimalFormat = NumberFormat.getInstance(
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) activity!!.resources.configuration.locale
                    else context!!.resources.configuration.locales.get(0))

            val pulsaSimUsage = MyBalDatabase.getInstance(activity)
                    .simUsageDao()
                    .loadLatestBalance(simSlot, SimUsage.INQUIRY_BALANCE_PULSA)

            // Show upper-part pulsa
            currencyTV.text = pulsaSimUsage?.getUnitName(activity) ?: "Rp"

            // If the pulsa is more than Rp 99, don't show the decimal calculation
            val animator =
                    if (pulsaSimUsage?.amount?.toInt() ?: 0 > 99)
                        ValueAnimator.ofInt(0, pulsaSimUsage?.amount?.toInt() ?: 0)
                    else
                        ValueAnimator.ofFloat(0f, pulsaSimUsage?.amount?.toFloat() ?: 0f)

            animator.duration = 500
            animator.addUpdateListener { animation ->
                if (cashBalanceTV != null)
                    cashBalanceTV.text = localDecimalFormat.format(animation.animatedValue)
            }
            animator.start()
        }
    }

    private fun refreshDonutBalanceDisplay() {

        // This check must be done because there are cases where the refresh is called
        // when an SMS comes and the fragment may not be visible
        if (isAdded) {

            val simUsageDao = MyBalDatabase.getInstance(activity).simUsageDao()
            val dataSimUsage = simUsageDao.loadLatestBalance(simSlot, SimUsage.INQUIRY_BALANCE_DATA)
            val smsSimUsage = simUsageDao.loadLatestBalance(simSlot, SimUsage.INQUIRY_BALANCE_SMS)
            val callSimUsage = simUsageDao.loadLatestBalance(simSlot, SimUsage.INQUIRY_BALANCE_CALL)

            // https://stackoverflow.com/a/39639103
            @Suppress("DEPRECATION")
            val localNumberFormat = NumberFormat.getInstance(
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) context!!.resources.configuration.locale
                    else context!!.resources.configuration.locales.get(0))

            val localDecimalFormat = localNumberFormat as DecimalFormat
            localDecimalFormat.applyPattern("###,###.##")

            // Show right-side of the card
            if (dataSimUsage != null && dataSimUsage.noPack) {
                dataBalanceTV.text = getString(R.string.balanceCard_noPack)
            } else {

                // If the data balance amount is more than 999, don't show the decimal calculation
                var dataBalanceText =
                        if (dataSimUsage?.amount?.toInt() ?: 0 > 999) {
                            localDecimalFormat.applyPattern("###,###")
                            localDecimalFormat.format(dataSimUsage?.amount ?: 0)
                        } else {
                            localDecimalFormat.applyPattern("###.##")
                            localDecimalFormat.format(dataSimUsage?.amount ?: 0)
                        }

                dataBalanceText += " "
                dataBalanceText += dataSimUsage?.getUnitName(context) ?: "MB"
                dataBalanceTV.text = dataBalanceText
            }

            if (smsSimUsage != null && smsSimUsage.noPack) {
                smsBalanceTV.text = getString(R.string.balanceCard_noPack)
            } else {
                var smsBalanceText = smsSimUsage?.amount?.toInt()?.toString() ?: "0"
                smsBalanceText += " "
                smsBalanceText += smsSimUsage?.getUnitName(context) ?: "SMS"
                smsBalanceTV.text = smsBalanceText
            }

            if (callSimUsage != null && callSimUsage.noPack) {
                callBalanceTV.text = getString(R.string.balanceCard_noPack)
            } else {
                var callBalanceText = callSimUsage?.amount?.toString() ?: "0"
                callBalanceText += " "
                callBalanceText += callSimUsage?.getUnitName(context) ?: getString(R.string.simUsage_unit_minutes)
                callBalanceTV.text = callBalanceText
            }


            // Show left-side of the card
            var donutLabel = ""
            var donutAmount = ""
            var donutUnit = ""
            var donutExpiry = ""

            when (shownUssdBalanceType) {

                Message.BALANCE_TYPE_DATA_GENERIC -> {
                    donutLabel = getString(R.string.balanceCheck_dataQuota)
                    if (dataSimUsage != null && dataSimUsage.noPack) {
                        donutAmount = "No Pack"
                    } else {
                        donutAmount =
                                if (dataSimUsage?.amount == null) getString(R.string.balanceCard_pullDown)
                                else localDecimalFormat.format(dataSimUsage.amount)
                        donutUnit = dataSimUsage?.getUnitName(context) ?: ""
                        donutExpiry = if (dataSimUsage == null) "" else Magic.expiryCountdown(context, dataSimUsage)
                    }
                }

                Message.BALANCE_TYPE_SMS -> {
                    donutLabel = getString(R.string.balanceCheck_smsQuota)
                    if (smsSimUsage != null && smsSimUsage.noPack) {
                        donutAmount = "No Pack"
                    } else {
                        donutAmount = smsSimUsage?.amount?.toInt()?.toString() ?: getString(R.string.balanceCard_pullDown)
                        donutUnit = smsSimUsage?.getUnitName(context) ?: ""
                        donutExpiry = if (smsSimUsage == null) "" else Magic.expiryCountdown(context, smsSimUsage)
                    }
                }

                Message.BALANCE_TYPE_CALL -> {
                    donutLabel = getString(R.string.balanceCheck_callQuota)
                    if (callSimUsage != null && callSimUsage.noPack) {
                        donutAmount = "No Pack"
                    } else {
                        donutAmount = callSimUsage?.amount?.toInt()?.toString() ?: getString(R.string.balanceCard_pullDown)
                        donutUnit = callSimUsage?.getUnitName(context) ?: ""
                        donutExpiry = if (callSimUsage == null) "" else Magic.expiryCountdown(context, callSimUsage)
                    }
                }
            }

            donutBalanceLabelTV.text = donutLabel
            donutBalanceAmountTV.text = donutAmount
            donutBalanceUnitTV.text = donutUnit
            donutBalanceExpiryTV.text = donutExpiry


            // Recolor the texts
            recolorTexts(shownUssdBalanceType)
        }
    }

    private fun parseProviderMessage(message: String): Map<String, String> {
        val msgRegexes = MyBalDatabase.getInstance(context)
                .messageRegexDao().getLivePatternsForProvider(providerId)

        return Magic.parseMessagePattern(message, msgRegexes)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_balance_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Global variables
        simSlot = arguments!!.getInt(ARGS_KEY_CARD_SLOT)
        providerId = arguments!!.getInt(ARGS_KEY_PROVIDER_ID)

        // Setup screen contents
        displayBasicSimInfo()
        displayPhoneNumber()
        setupTouchInteraction()

        shownUssdBalanceType = Message.BALANCE_TYPE_DATA_GENERIC
        refreshPulsaBalanceDisplay()
        refreshDonutBalanceDisplay()

        // Setup click listeners
        dataBalanceContainerLL.setOnClickListener {
            shownUssdBalanceType = Message.BALANCE_TYPE_DATA_GENERIC
            recolorTexts(Message.BALANCE_TYPE_DATA_GENERIC)
            refreshDonutBalanceDisplay()
        }
        smsBalanceContainerLL.setOnClickListener {
            shownUssdBalanceType = Message.BALANCE_TYPE_SMS
            recolorTexts(Message.BALANCE_TYPE_SMS)
            refreshDonutBalanceDisplay()
        }
        callBalanceContainerLL.setOnClickListener {
            shownUssdBalanceType = Message.BALANCE_TYPE_CALL
            recolorTexts(Message.BALANCE_TYPE_CALL)
            refreshDonutBalanceDisplay()
        }
    }

    override fun onStart() {
        super.onStart()

        displayBasicSimInfo()
        displayProviderInfo()
    }

    private fun displayPhoneNumber() {
        val phoneNumber = activity!!
                .getSharedPreferences(SimSettingsFragment.KEY_PREF_PHONE_NUMBER, Context.MODE_PRIVATE)
                .getString(if (simSlot == BalanceHomeFragment.FIRST_SLOT) Constants.PHONE_NUMBER_SLOT_1
                else Constants.PHONE_NUMBER_SLOT_2, "")

        phoneNumberTV.text = Magic.insertPeriodically(phoneNumber, " ", 4)
    }

    private fun recolorTexts(balanceType: Int) {

        // Reset all TV color
        val textViews = listOf(dataBalanceTV, dataExpiryTV, smsBalanceTV, smsExpiryTV, callBalanceTV, callExpiryTV)
        textViews.forEach { it.setTextColor(ContextCompat.getColor(activity!!, R.color.balanceCardTextColor)) }

        // Reset all icon color
        dataTypeIconIV.setImageResource(R.drawable.ic_card_data)
        smsTypeIconIV.setImageResource(R.drawable.ic_card_sms)
        callTypeIconIV.setImageResource(R.drawable.ic_card_call)

        // Paint only the selected part
        val activeTextColor = ContextCompat.getColor(activity!!, R.color.colorPrimary)
        when (balanceType) {
            Message.BALANCE_TYPE_DATA_GENERIC -> {
                dataTypeIconIV.setImageResource(R.drawable.ic_card_data_active)
                dataBalanceTV.setTextColor(activeTextColor)
                dataExpiryTV.setTextColor(activeTextColor)
            }

            Message.BALANCE_TYPE_SMS -> {
                smsTypeIconIV.setImageResource(R.drawable.ic_card_sms_active)
                smsBalanceTV.setTextColor(activeTextColor)
                smsExpiryTV.setTextColor(activeTextColor)
            }

            Message.BALANCE_TYPE_CALL -> {
                callTypeIconIV.setImageResource(R.drawable.ic_card_call_active)
                callBalanceTV.setTextColor(activeTextColor)
                callExpiryTV.setTextColor(activeTextColor)
            }
        }
    }

    private fun displayBasicSimInfo() {

        val simType = activity!!
                .getSharedPreferences(SimSettingsFragment.KEY_PREF_SIM_TYPE, Context.MODE_PRIVATE)
                .getInt(if (simSlot == BalanceHomeFragment.FIRST_SLOT) Constants.SIM_TYPE_SLOT_1 else Constants.SIM_TYPE_SLOT_2,
                        ChangeSimTypeDialogFragment.GSM_PREPAID)

        // If provider is undetected or the card is not a prepaid, show "Unsupported"
        if (providerId == 0 ||
                simType != ChangeSimTypeDialogFragment.GSM_PREPAID) {
            simNotSupportedTV.visibility = View.VISIBLE
        } else {
            simNotSupportedTV.visibility = View.GONE
        }
    }

    private fun setupTouchInteraction() {

        @Suppress("ConstantConditionIf")
        if (BuildConfig.BUILD_TYPE == "uiDev") {
            balanceDebug.visibility = View.VISIBLE
        }

        val pulsaBarSpringAnim = SpringAnimation(pulsaBarLL, DynamicAnimation.TRANSLATION_Y)
        val donutSpringAnim = SpringAnimation(pulldownDonutFL, DynamicAnimation.TRANSLATION_Y)

        val force = SpringForce(0f)
        force.dampingRatio = SpringForce.DAMPING_RATIO_LOW_BOUNCY
        force.stiffness = SpringForce.STIFFNESS_MEDIUM
        pulsaBarSpringAnim.spring = force
        donutSpringAnim.spring = force

        pulsaBarOrigin = pulsaBarLL.y
        pulsaBarLL.setOnTouchListener({ _, event ->

            when (event.actionMasked) {

                MotionEvent.ACTION_DOWN -> {
                    pulsaBarTouchOrigin = pulsaBarLL.y - event.rawY
                    lastTouchAction = MotionEvent.ACTION_DOWN
                }

                MotionEvent.ACTION_MOVE -> {
                    if (event.rawY + pulsaBarTouchOrigin > pulsaBarOrigin)
                        if (pulsaBarLL.translationY in 0f..maxPulldownDistance)
                            pulsaBarLL.y = event.rawY + pulsaBarTouchOrigin

                    lastTouchAction = MotionEvent.ACTION_MOVE
                }

                MotionEvent.ACTION_UP -> {
                    if (pulsaBarLL.y - pulsaBarOrigin >= maxPulldownDistance)
                        dialBalanceCheck(simSlot, Message.BALANCE_TYPE_PULSA_MAIN)

                    pulsaBarSpringAnim.start()
                }
            }

            true
        })

        donutOrigin = pulldownDonutFL.y
        pulldownDonutFL.setOnTouchListener({ _, event ->

            when (event.actionMasked) {

                MotionEvent.ACTION_DOWN -> {
                    donutTouchOrigin = pulldownDonutFL.y - event.rawY
                    lastTouchAction = MotionEvent.ACTION_DOWN
                }

                MotionEvent.ACTION_MOVE -> {
                    if (event.rawY + donutTouchOrigin > donutOrigin)
                        if (pulldownDonutFL.translationY in 0f..maxPulldownDistance)
                            pulldownDonutFL.y = event.rawY + donutTouchOrigin

                    lastTouchAction = MotionEvent.ACTION_MOVE

                    textView1.text = "FL y: " + pulldownDonutFL.y
                    textView2.text = "Donut Origin: " + donutOrigin
                    textView3.text = "Max Distance: " + maxPulldownDistance
                    textView4.text = "Translation: " + (pulldownDonutFL.y - donutOrigin)
                    textView5.text = "Y translate: " + pulldownDonutFL.translationY
                }

                MotionEvent.ACTION_UP -> {
                    if (pulldownDonutFL.y - donutOrigin >= maxPulldownDistance) {
                        dialBalanceCheck(simSlot, shownUssdBalanceType)
                    }

                    donutSpringAnim.start()
                }
            }

            true
        })
    }

    /**
     * If the card is XL or Axis, and user hasn't explicitly defined which card it is,
     * display this View. Remember: ID 3 is XL and 4 is Axis, defined when user has synced
     * the offline database, see Stetho for more details.
     *
     * If user has already defined the custom selection, show it.
     */
    private fun displayProviderInfo() {

        val xlAxisPref = activity!!.getSharedPreferences(SimSettingsFragment.KEY_PREF_XL_AXIS, Context.MODE_PRIVATE)
        val xlOrAxis = xlAxisPref.getInt(if (simSlot == 0) Constants.XL_AXIS_SLOT_1 else Constants.XL_AXIS_SLOT_2, 0)

        when (providerId) {

            3, 4 ->
                if (xlOrAxis == 0) { // If the XL or Axis preference is not found, prompt choice
                    xlAxisLL.visibility = View.VISIBLE
                    donutBalanceLL.visibility = View.GONE
                    pulsaBarLL.visibility = View.GONE

                    keepXlButton.setOnClickListener({
                        xlAxisPref.edit().putInt(if (simSlot == 0) Constants.XL_AXIS_SLOT_1 else Constants.XL_AXIS_SLOT_2, 3).apply()
                        onStart()
                    })
                    switchAxisButton.setOnClickListener({
                        // Because the General Settings tab is 0, we increment the index by 1
                        startActivity(Intent(activity, SettingsActivity::class.java)
                                .putExtra(SettingsActivity.KEY_ARG_LANDING, simSlot + 1))
                    })

                } else { // If the XL or Axis preference is found
                    providerId = xlOrAxis
                    xlAxisLL.visibility = View.GONE
                    donutBalanceLL.visibility = View.VISIBLE
                    pulsaBarLL.visibility = View.VISIBLE
                }

        // For other providers
            else -> {
                xlAxisLL.visibility = View.GONE
                donutBalanceLL.visibility = View.VISIBLE
                pulsaBarLL.visibility = View.VISIBLE
            }
        }

        providerLogoIV.setImageResource(Provider.getProviderLogo(providerId)!!)
    }

    /**
     * Searches the ussd_code table defined in [UssdCode] for an USSD sequence for a particular
     * request, then dials the USSD sequence using a particular SIM slot.
     * // https://stackoverflow.com/a/38605194
     *
     * @param cardSlot    The card slot used to dial the USSD sequence/number
     * @param ussdBalanceType The balance type to be checked, according to UssdCode
     */
    private fun dialBalanceCheck(cardSlot: Int, ussdBalanceType: Int) {

        @Suppress("ConstantConditionIf")
        if (BuildConfig.BUILD_TYPE == "uiDev") {
            Toast.makeText(context, "Dialing " + ussdBalanceType, 100).show()
            return
        }

        // Check permission
        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(activity, "Call permission is not granted.", Toast.LENGTH_SHORT).show()
            return
        }

        // First, check whether the user has overridden the USSD from the settings page.
        // If user has set a custom for this sim slot and dial type, use the custom USSD instead.
        // If user hasn't set a custom USSD call, go check the database.

        val customUssdCode = activity!!
                .getSharedPreferences(SimSettingsFragment.KEY_PREF_USSD_OVERRIDE, Context.MODE_PRIVATE)
                .getString(Magic.getCustomUssdPrefKey(cardSlot, ussdBalanceType), "")

        // If custom USSD code is defined, use it
        if (customUssdCode != null && customUssdCode != "") {

            dialUssd(ussdBalanceType, customUssdCode)

        }

        // Else use the default
        else {

            val defaultUssdCode: UssdCode? = MyBalDatabase.getInstance(activity)
                    .ussdCodeDao().getUssdCode(providerId, ussdBalanceType)

            // Determine which provider is going to call
            if (defaultUssdCode == null) {
                AlertDialog.Builder(activity)
                        .setTitle("No USSD data")
                        .setMessage("This app needs to download a list of USSD sequences to be able to check your balance. "
                                + "Please make sure your phone is connected to the internet and press Retry.")
                        .setCancelable(true)
                        .setNeutralButton("Retry") { dialog, _ ->
                            (activity!!.application as MyBalApp).updateOfflineDb()
                            dialog.dismiss()
                        }
                        .show()
            } else {

                dialUssd(ussdBalanceType, defaultUssdCode.code)

            }
        }
    }

    @SuppressLint("MissingPermission", "NewApi")
    private fun dialUssd(ussdBalanceType: Int, ussdCode: String) {

        // Setup listener to retrieve the reply from USSD call
        hasUssdReceived = false
        ussdBalanceTypeCalled = ussdBalanceType
        RxBus.getInstance().observables.subscribe(ussdSmsMessageObserver)

        // Prepare the calling intent
        val ussdCallIntent = Intent(Intent.ACTION_CALL).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        ussdCallIntent.putExtra("com.android.phone.force.slot", true)
        ussdCallIntent.data = Uri.parse("tel:" + Uri.encode(ussdCode))
        ussdCallIntent.putExtra("Cdma_Supp", true)

        // Force use the second SIM slot, depending on the Android OS version
        // https://stackoverflow.com/a/44298513
        Constants.simSlotNames
                .forEach { ussdCallIntent.putExtra(it, simSlot) }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val telecomManager = activity!!.getSystemService(Context.TELECOM_SERVICE) as TelecomManager
            val phoneAccountHandleList = telecomManager.callCapablePhoneAccounts
            if (phoneAccountHandleList != null && phoneAccountHandleList.size > 0)
                ussdCallIntent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandleList[simSlot])
        }

        // Activate one-time listener in MyBalAccessibilityService
        RxBus.getInstance().publish(RxBus.Payload(RxBus.FLAG_REQUEST_BALANCE_START_LISTENING, true))
        startActivity(ussdCallIntent)
    }
}