package com.mybal.id.home

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Toast
import com.mybal.id.R
import com.mybal.id.network.RestClient
import com.mybal.id.siminfo.TelephonyInfo
import kotlinx.android.synthetic.main.widget_rate_us.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateUsDialogFragment : DialogFragment() {

    companion object {
        fun newInstance(): RateUsDialogFragment {
            return RateUsDialogFragment()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = View.inflate(context, R.layout.widget_rate_us, null)
        return AlertDialog.Builder(activity).setView(view).show()
    }

    override fun onStart() {
        super.onStart()
        dialog.ratingBar.setOnRatingBarChangeListener { _, rating, _ ->
            if (rating < 3) displaySuggestionBox()
            else hideSuggestionBox()
        }

        dialog.gotoPlayStoreButton.setOnClickListener({
            val appPackageName = dialog.ownerActivity.packageName
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)))
            }
        })
    }

    private fun displaySuggestionBox() {
        dialog.gotoPlayStoreButton.visibility = View.GONE
        dialog.suggestionContainerLL.visibility = View.VISIBLE
        dialog.submitButton.setOnClickListener { reportMessage() }
        dialog.cancelButton.setOnClickListener { dismiss() }
    }

    private fun hideSuggestionBox() {
        dialog.gotoPlayStoreButton.visibility = View.VISIBLE
        dialog.suggestionContainerLL.visibility = View.GONE
    }

    private fun reportMessage() {
        RestClient.getClient(context).postAppRating(
                TelephonyInfo.getInstance(dialog.ownerActivity).imsiSIM1.toString(),
                dialog.ratingBar.rating.toInt(),
                dialog.suggestionET.text.toString())
                .enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        Toast.makeText(activity, "Your message has been reported. Thank you.", Toast.LENGTH_SHORT).show()
                        dismiss()
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        Toast.makeText(activity, "Message failed to send. Please try again.", Toast.LENGTH_SHORT).show()
                    }
                })
    }
}
