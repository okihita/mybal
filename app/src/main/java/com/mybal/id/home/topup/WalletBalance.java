package com.mybal.id.home.topup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletBalance {

    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("wallet_balance")
    @Expose
    public int walletBalance;
    @SerializedName("reward_total")
    @Expose
    public int rewardTotal;
    @SerializedName("n_reward")
    @Expose
    public int nReward;
    @SerializedName("n_follower")
    @Expose
    public int nFollower;

}
