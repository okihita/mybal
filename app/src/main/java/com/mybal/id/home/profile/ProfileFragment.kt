package com.mybal.id.home.profile

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.accountkit.Account
import com.facebook.accountkit.AccountKit
import com.facebook.accountkit.AccountKitCallback
import com.facebook.accountkit.AccountKitError
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mybal.id.R
import com.mybal.id.helpsupport.HelpSupportActivity
import com.mybal.id.home.LoginActivity
import com.mybal.id.invite.InviteActivity
import com.mybal.id.settings.SettingsActivity
import com.mybal.id.simhistory.SimHistoryActivity
import com.mybal.id.splash.SplashActivity
import com.mybal.id.util.Constants
import com.mybal.id.util.Magic
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.*

class ProfileFragment : Fragment() {

    // Menu item index
    private val simUsage = 0
    private val helpSupport = 1
    private val settings = 2
    private val language = 3
    private val invite = 4
    private val about = 5

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onResume() {
        super.onResume()
        if (isAdded) loadProfile()
        loadMenus()
        nsv.fullScroll(View.FOCUS_UP)
        loadLogout()
    }

    private fun loadProfile() {

        Glide.with(activity)
                .load(R.drawable.img_user_icon)
                .apply(RequestOptions.circleCropTransform())
                .into(avatarIV)

        AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
            override fun onSuccess(account: Account) {
                if (isVisible) {
                    loginRegisterButton.visibility = View.GONE
                    containerLL.visibility = View.VISIBLE
                    usernameTV.text = account.phoneNumber.toString()
                }
            }

            override fun onError(accountKitError: AccountKitError) {
                if (isVisible) {
                    containerLL.visibility = View.GONE
                    loginRegisterButton.visibility = View.VISIBLE
                }
            }
        })

        loginRegisterButton.setOnClickListener {
            startActivity(Intent(activity, LoginActivity::class.java))
        }
    }

    private fun loadMenus() {

        val flagResId: Int
        val lang = Locale.getDefault().language
        flagResId = if (lang.equals(Constants.LOCALE_CODE_INDONESIA, ignoreCase = true))
            R.drawable.img_flag_id
        else
            R.drawable.img_flag_gb

        val profileMenus = ArrayList<ProfileMenu>()
        profileMenus.add(ProfileMenu(R.drawable.menu_icon_sim_usage_24, getString(R.string.profile_menu_simUsage)))
        profileMenus.add(ProfileMenu(R.drawable.menu_icon_support_24, getString(R.string.profile_menu_helpSupport)))
        profileMenus.add(ProfileMenu(R.drawable.menu_icon_settings_24, getString(R.string.profile_menu_settings)))
        profileMenus.add(ProfileMenu(flagResId, getString(R.string.profile_menu_language)))
        profileMenus.add(ProfileMenu(R.drawable.menu_icon_invite_24, getString(R.string.profile_menu_invite)))
        profileMenus.add(ProfileMenu(R.drawable.img_logo_mybal, getString(R.string.profile_menu_about)))

        val itemAdapter: ItemAdapter<IItem<*, *>> = ItemAdapter()
        val fastAdapter = FastAdapter.with<IItem<*, *>, ItemAdapter<IItem<*, *>>>(itemAdapter)

        profileMenusRV.layoutManager = LinearLayoutManager(activity)
        profileMenusRV.setHasFixedSize(true)
        profileMenusRV.adapter = fastAdapter

        itemAdapter.add(profileMenus as List<IItem<*, *>>?)
        fastAdapter.withOnClickListener({ _, _, _, position ->

            when (position) {

                simUsage ->
                    startActivity(Intent(activity, SimHistoryActivity::class.java))

                helpSupport ->
                    startActivity(Intent(activity, HelpSupportActivity::class.java))

                settings ->
                    startActivity(Intent(activity, SettingsActivity::class.java))

                language ->
                    when (lang) {

                        Constants.LOCALE_CODE_INDONESIA ->
                            Magic.changeLanguage(activity, Constants.LOCALE_CODE_ENGLISH)

                        Constants.LOCALE_CODE_ENGLISH ->
                            Magic.changeLanguage(activity, Constants.LOCALE_CODE_INDONESIA)
                    }

                invite ->
                    startActivity(Intent(activity, InviteActivity::class.java))

                about ->
                    startActivity(Intent(activity, AboutUsActivity::class.java))

                else -> {
                }
            }

            true
        })
    }

    private fun loadLogout() {
        AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
            override fun onSuccess(account: Account) {
                if (isVisible) {
                    signoutContainerLL.visibility = View.VISIBLE
                }
            }

            override fun onError(accountKitError: AccountKitError) {
                if (isVisible) {
                    signoutContainerLL.visibility = View.GONE
                }
            }
        })

        signoutContainerLL.setOnClickListener({
            AccountKit.logOut()
            activity!!.finish()
            startActivity(Intent(activity, SplashActivity::class.java))
        })
    }
}