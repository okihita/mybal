package com.mybal.id.home.topup

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mikepenz.fastadapter.items.AbstractItem
import com.mybal.id.R
import kotlinx.android.synthetic.main.item_bank_channel.view.*

open class BankChannelItem(
        val bankCode: String
) : AbstractItem<BankChannelItem, BankChannelItem.ViewHolder>() {

    var isChosen = false

    companion object {
        const val BANK_CODE_MANDIRI = "BMRI"
        const val BANK_CODE_BCA = "CENA"
        const val BANK_CODE_BNI = "BNIN"
        const val BANK_CODE_KEB_HANA = "HNBN"
        const val BANK_CODE_PERMATA = "BBBA"
        const val BANK_CODE_BII_MAYBANK = "IBBK"
        const val BANK_CODE_CIMB = "BNIA"
        const val BANK_CODE_BRI = "BRIN"
        const val BANK_CODE_DANAMON = "BDIN"
    }

    override fun getViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }

    override fun getType(): Int {
        return R.id.fastadapter_bankchannel_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_bank_channel
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)

        holder.logo.setImageResource(when (bankCode) {
            BANK_CODE_MANDIRI -> R.drawable.img_logo_mandiri
            BANK_CODE_BCA -> R.drawable.img_logo_bca
            BANK_CODE_BNI -> R.drawable.img_logo_bni
            BANK_CODE_KEB_HANA -> R.drawable.img_logo_keb_hana
            BANK_CODE_PERMATA -> R.drawable.img_logo_permata_bank
            BANK_CODE_BII_MAYBANK -> R.drawable.img_logo_maybank
            BANK_CODE_CIMB -> R.drawable.img_logo_cimb_niaga
            BANK_CODE_BRI -> R.drawable.img_logo_bri
            BANK_CODE_DANAMON -> R.drawable.img_logo_danamon
            else -> 0
        })

        holder.name.text = when (bankCode) {
            BANK_CODE_MANDIRI -> "Mandiri"
            BANK_CODE_BCA -> "BCA"
            BANK_CODE_BNI -> "BNI"
            BANK_CODE_KEB_HANA -> "KEB HANA"
            BANK_CODE_PERMATA -> "Permata"
            BANK_CODE_BII_MAYBANK -> "BII Maybank"
            BANK_CODE_CIMB -> "CIMB"
            BANK_CODE_BRI -> "BRI"
            BANK_CODE_DANAMON -> "Danamon"
            else -> "invalid"
        }

        holder.container.setBackgroundColor(ContextCompat.getColor(holder.container.context,
                if (isChosen) R.color.colorAccent else R.color.solidWhite))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var container: LinearLayout = itemView.containerLL
        var logo: ImageView = itemView.logoIV
        var name: TextView = itemView.nameTV
    }
}
