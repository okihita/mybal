package com.mybal.id.home.profile

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.mikepenz.fastadapter.items.AbstractItem
import com.mybal.id.R
import kotlinx.android.synthetic.main.item_profile_menu.view.*


class ProfileMenu(private val imageRes: Int, private val title: String) : AbstractItem<ProfileMenu, ProfileMenu.ViewHolder>() {

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override fun getType(): Int {
        return R.id.fastadapter_profilemenu_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_profile_menu
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        holder.iconIV.setImageResource(imageRes)
        holder.titleTV.text = title
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iconIV: ImageView = itemView.profileMenu_IV_icon
        var titleTV: TextView = itemView.profileMenu_TV_title
    }
}
