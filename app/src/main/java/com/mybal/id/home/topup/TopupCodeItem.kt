package com.mybal.id.home.topup

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.mikepenz.fastadapter.items.AbstractItem
import com.mybal.id.R
import com.mybal.id.db.TopupCode
import kotlinx.android.synthetic.main.item_topup_code.view.*

open class TopupCodeItem(val topupCode: TopupCode) : AbstractItem<TopupCodeItem, TopupCodeItem.ViewHolder>() {

    override fun getViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }

    override fun getType(): Int {
        return R.id.fastadapter_topupcode_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_topup_code
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        holder.amountTV.text = topupCode.amount.toString()
        holder.priceTV.text = topupCode.price.toString()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var amountTV: TextView = itemView.amountTV
        var priceTV: TextView = itemView.priceTV
    }
}
