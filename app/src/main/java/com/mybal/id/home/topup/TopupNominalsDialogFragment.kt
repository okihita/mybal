package com.mybal.id.home.topup

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mybal.id.R
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.db.Provider
import kotlinx.android.synthetic.main.widget_topup_nominals.view.*

class TopupNominalsDialogFragment : DialogFragment() {

    companion object {

        private const val ARGS_KEY_PROVIDER_ID = "com.mybal.id.provider_id"

        fun newInstance(providerId: Int): TopupNominalsDialogFragment {

            val args = Bundle()
            args.putInt(ARGS_KEY_PROVIDER_ID, providerId)

            val fragment = TopupNominalsDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = View.inflate(context, R.layout.widget_topup_nominals, null)

        val provider = Provider(arguments!!.getInt(ARGS_KEY_PROVIDER_ID))
        view.providerNameTV.text = provider.name
        view.providerLogoTV.setImageResource(Provider.getProviderLogo(provider.id))

        val topupCodes = MyBalDatabase.getInstance(context).topupCodeDao().loadByProvider(provider.id)

        val itemAdapter: ItemAdapter<IItem<*, *>> = ItemAdapter()
        val fastAdapter = FastAdapter.with<IItem<*, *>, ItemAdapter<IItem<*, *>>>(itemAdapter)

        view.topupNominalsRV.layoutManager = LinearLayoutManager(activity)
        view.topupNominalsRV.setHasFixedSize(true)
        view.topupNominalsRV.adapter = fastAdapter

        topupCodes.forEach { itemAdapter.add(TopupCodeItem(it)) }

        fastAdapter.withOnClickListener { _, _, item, _ ->
            (parentFragment as PulsaTopupFragment).showTopupCodeItem(item as TopupCodeItem)
            dismiss()
            true
        }

        return AlertDialog.Builder(activity).setView(view).show()
    }
}
