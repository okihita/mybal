package com.mybal.id.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.facebook.accountkit.*
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import com.mybal.id.db.User
import com.mybal.id.network.RestClient
import com.mybal.id.siminfo.TelephonyInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    companion object {
        const val FACEBOOK_LOGIN_REQUEST_CODE = 99
    }

    private val myBalApi = RestClient.getClient(this)

    private var telephonyInfo: TelephonyInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        telephonyInfo = TelephonyInfo.getInstance(this)

        checkIfLoggedIn()
    }

    private fun checkIfLoggedIn() {
        val accessToken = AccountKit.getCurrentAccessToken()
        if (accessToken != null) { // If a user session still exists in the device
            finish()
        } else {
            facebookKitLogin()
        }
    }


    private fun facebookKitLogin() {
        val intent = Intent(this, AccountKitActivity::class.java)

        val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN)

        configurationBuilder.setReceiveSMS(true)
        configurationBuilder.setSMSWhitelist(arrayOf("ID"))

        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build())
        startActivityForResult(intent, FACEBOOK_LOGIN_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == FACEBOOK_LOGIN_REQUEST_CODE) {

            val loginResult = data.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY)
            val toastMessage: String

            when {

                loginResult.error != null -> {
                    toastMessage = loginResult.error!!.errorType.message
                    Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show()
                }

                loginResult.wasCancelled() -> {
                    toastMessage = "Login Cancelled"
                    Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show()
                    finish()
                }

                else -> // If success i.e. phone number ownership is verified
                    AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
                        override fun onSuccess(account: Account) {
                            promptLogin(account.phoneNumber.toString(), telephonyInfo!!.imsiSIM1.toString())
                        }

                        override fun onError(accountKitError: AccountKitError) {
                            Toast.makeText(this@LoginActivity, "Please close the app and try again.", Toast.LENGTH_SHORT).show()
                        }
                    })
            }
        }
    }

    /**
     * After phone number ownership is verified, the app checks whether the current number is
     * already exist in the list of active user.
     *
     * @param phoneNumber
     * @param imei
     */
    private fun promptLogin(phoneNumber: String, imei: String) {

        myBalApi.loginRefreshTokenUser(phoneNumber, imei).enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful) {
                    Toast.makeText(this@LoginActivity, "Welcome!", Toast.LENGTH_SHORT).show()
                } else if (response.code() == 404) { // Kalau object user not found, maka registerin
                    bindPhoneNumber(phoneNumber, imei)
                }

                val homeIntent = Intent(this@LoginActivity, HomeActivity::class.java)
                homeIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(homeIntent)

                finish()
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "Network error. Please close the app and try again.", Toast.LENGTH_SHORT).show()
            }
        })
    }

    // Remember that user ID is first bound to the device, and only then the user can choose to
    // bind her/his account.
    private fun bindPhoneNumber(phoneNumber: String, imei: String) {
        myBalApi.register(phoneNumber, imei).enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful) {
                    Toast.makeText(this@LoginActivity, "Welcome!", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "Network error. Please close the app and try again.", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
