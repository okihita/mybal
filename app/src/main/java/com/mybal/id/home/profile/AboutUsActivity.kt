package com.mybal.id.home.profile

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem

import com.mybal.id.BuildConfig
import com.mybal.id.R

import kotlinx.android.synthetic.main.activity_about_us.*
import kotlinx.android.synthetic.main.widget_toolbar.*

class AboutUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        setupToolbar()
        versionTV.text = "MyBal App v" + BuildConfig.VERSION_NAME
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setTitle(R.string.aboutUs_title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
