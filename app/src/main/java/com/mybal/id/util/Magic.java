package com.mybal.id.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mybal.id.R;
import com.mybal.id.db.Message;
import com.mybal.id.db.MessageRegex;
import com.mybal.id.db.MyBalDatabase;
import com.mybal.id.db.SimUsage;
import com.mybal.id.home.balance.BalanceHomeFragment;
import com.mybal.id.network.RestClient;
import com.mybal.id.siminfo.TelephonyInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.CLIPBOARD_SERVICE;

public class Magic {

    public static final String KEY_FOUND_MESSAGE_ID = "FOUND_MESSAGE_ID";
    public static final String KEY_FOUND_REGEX_ID = "FOUND_REGEX_ID";
    private static final String KEY_ADDITIONAL_REGEX = "ADDITIONAL_REGEX";

    /**
     * Please note that both dates must be ISO8601-compliant else it won't calculate.
     *
     * @param seenAt     The SimUsage#createdAt
     * @param expiryDate The SimUsage#expiry
     * @return Remaining time in months or weeks or days or hours or minutes or seconds
     */
    private static String expiryCountdown(Context context, String seenAt, String expiryDate) {

        if (expiryDate.equals(SimUsage.EXPIRY_DATE_UNKNOWN) || expiryDate.equals("")) {

            return "n/a";

        } else try {

            long diff = Constants.INSTANCE.getDateStorageFormat().parse(expiryDate).getTime()
                    - Constants.INSTANCE.getDateStorageFormat().parse(seenAt).getTime();

            if (diff > 1000 * 60 * 60 * 24) { // If more than one day
                return "" + ((int) (diff / (1000 * 60 * 60 * 24))) + " " + context.getResources().getString(R.string.simUsage_unit_days);
            } else if (diff > 1000 * 60 * 60) { // If more than one hour
                return "" + ((int) (diff / (1000 * 60 * 60))) + " " + context.getResources().getString(R.string.simUsage_unit_hours);
            } else if (diff > 1000 * 60) { // If more than one minute
                return "" + ((int) (diff / (1000 * 60))) + " " + context.getResources().getString(R.string.simUsage_unit_minutes);
            } else if (diff > 1000) {
                return "" + ((int) (diff / 1000)) + " " + context.getResources().getString(R.string.simUsage_unit_seconds);
            } else { // If it's a negative
                return "0 seconds (already expired)";
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return context.getString(R.string.simUsage_expiry_wrongDate);
        }
    }

    // Convenience method
    public static String expiryCountdown(Context context, SimUsage simUsage) {
        return expiryCountdown(context, simUsage.createdAt, simUsage.expiry);
    }

    // Careful. Indonesia uses "in" and not "id". https://github.com/championswimmer/android-locales
    public static void changeLanguage(Activity activity, String lang) {
        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        activity.getResources().updateConfiguration(config, activity.getResources().getDisplayMetrics());
        activity.recreate();
    }

    public static Map<String, String> parseMessagePattern(String rawMessage, List<MessageRegex> messageRegexes) {

        // Removes the new line, according to the agreeement with the backend engineer
        rawMessage = rawMessage
                .replace("\n", " ")
                .replace("\r", " ")
                .replace("  ", " ");

        Map<String, String> patternedElements = new HashMap<>();

        // Iterate through all rows of database for that particular provider
        boolean isWholePatternMatched = false;
        for (MessageRegex messageRegex : messageRegexes) {

            if (!isWholePatternMatched) {
                try {

                    Matcher fullPatternMatcher = Pattern.compile(messageRegex.getRegex()).matcher(rawMessage);

                    // http://tutorials.jenkov.com/java-regex/matcher.html
                    if (fullPatternMatcher.lookingAt()) {

                        patternedElements.put(KEY_FOUND_MESSAGE_ID, String.valueOf(messageRegex.getMessageId().id));
                        patternedElements.put(KEY_FOUND_REGEX_ID, String.valueOf(messageRegex.getId()));
                        patternedElements.put(KEY_ADDITIONAL_REGEX, messageRegex.getDateRegex());

                        isWholePatternMatched = true;

                        // Jika pola ditemukan, iterasi messageRegex.code satu per satu
                        List<String> labels = Arrays.asList(messageRegex.getPattern().split("@@"));
                        List<String> groupedParts = new ArrayList<>();
                        for (int i = 1; i <= fullPatternMatcher.groupCount(); i++) {
                            groupedParts.add(fullPatternMatcher.group(i));
                        }

                        // Start putting matches into the Map<String, String>
                        double additionalMainDataBalance = 0;
                        double sumMainDataBalance = 0;

                        String label;
                        String content;

                        for (int i = 0; i < labels.size(); i++) {

                            label = labels.get(i);
                            // If it's only one label, like "IGNORED PATTERN" or "ALERT BY SMS",
                            // save the raw message instead of the grouped parts of the message
                            content = groupedParts.size() == 0 ? rawMessage : groupedParts.get(i);


                            // Check all already-stored patterns
                            // Here's the algorithm to sum all data balances
                            switch (label) {

                                case Constants.MAIN_DATA_MRP_MAIN_BALANCE:
                                    if (patternedElements.containsKey(Constants.MAIN_DATA_MRP_MAIN_BALANCE)) {
                                        additionalMainDataBalance = Double.parseDouble(content);
                                    } else {
                                        patternedElements.put(Constants.MAIN_DATA_MRP_MAIN_BALANCE, content);
                                    }
                                    break;


                                case Constants.DATA_UNIT:
                                    if (additionalMainDataBalance != 0) {

                                        double originalMainDataBalance = Double.parseDouble(patternedElements.get(Constants.MAIN_DATA_MRP_MAIN_BALANCE));
                                        String originalMainDataUnit = patternedElements.get(Constants.DATA_UNIT);

                                        // Convert it all to kB
                                        if (originalMainDataUnit.equalsIgnoreCase("mb")) {
                                            originalMainDataBalance = originalMainDataBalance * 1000;
                                        } else if (originalMainDataUnit.equalsIgnoreCase("gb")) {
                                            originalMainDataBalance = originalMainDataBalance * 1000000;
                                        }

                                        // Add them
                                        if (content.equalsIgnoreCase("mb")) {
                                            sumMainDataBalance = originalMainDataBalance + (additionalMainDataBalance * 1000);
                                        } else if (content.equalsIgnoreCase("gb")) {
                                            sumMainDataBalance = originalMainDataBalance + (additionalMainDataBalance * 1000000);
                                        }

                                        // Assign the correct unit
                                        if (sumMainDataBalance / 10000000 >= 1) { // If it's above 10 GB, store in GB format
                                            patternedElements.put(Constants.MAIN_DATA_MRP_MAIN_BALANCE, String.valueOf(sumMainDataBalance / 1000000));
                                            patternedElements.put(Constants.DATA_UNIT, "GB");
                                        } else if (sumMainDataBalance / 10000 >= 1) { // If it's above 10 MB, store in MB format
                                            patternedElements.put(Constants.MAIN_DATA_MRP_MAIN_BALANCE, String.valueOf(sumMainDataBalance / 1000));
                                            patternedElements.put(Constants.DATA_UNIT, "MB");
                                        } else {
                                            patternedElements.put(Constants.MAIN_DATA_MRP_MAIN_BALANCE, String.valueOf(sumMainDataBalance));
                                            patternedElements.put(Constants.DATA_UNIT, "KB");
                                        }

                                        additionalMainDataBalance = 0;
                                        sumMainDataBalance = 0;
                                    }

                                default:
                                    patternedElements.put(label, content);
                                    break;
                            }
                        }
                    }
                } catch (Exception ignored) {
                    Log.e("###", "parseMessagePattern: " + ignored.getMessage());
                }
            }
        }
        return patternedElements;
    }

    /**
     * Note that this storage mechanism depends on what kind the Message package type is returned.
     */
    public static void saveIntoSimUsage(Context context, int callerSlot, String originalMessage, Map<String, String> parsedMessage) {

        if (parsedMessage.get(KEY_FOUND_REGEX_ID) == null) {

            Log.d("###", "saveIntoSimUsage: Match not found. Message not saved.");

        } else {

            // Determine the transaction type
            int messageRegexId = Integer.valueOf(parsedMessage.get(KEY_FOUND_REGEX_ID));
            MessageRegex messageRegex = MyBalDatabase.getInstance(context)
                    .messageRegexDao().getMessageRegex(messageRegexId);

            int transactionType = SimUsage.UNNECESSARY_MESSAGE;

            int eventType = messageRegex.getMessageId().eventType;
            int packageType = Integer.parseInt(messageRegex.getMessageId().textPackType);

            boolean isExist = false;

            switch (eventType) {
                case Message.EVENT_TYPE_INQUIRY:
                    switch (packageType) {
                        case Message.BALANCE_TYPE_PULSA_MAIN:
                        case Message.BALANCE_TYPE_PULSA_PROMOTION:
                            transactionType = SimUsage.INQUIRY_BALANCE_PULSA;
                            break;

                        case Message.BALANCE_TYPE_DATA_GENERIC:
                        case Message.BALANCE_TYPE_DATA_2G:
                        case Message.BALANCE_TYPE_DATA_3G:
                        case Message.BALANCE_TYPE_DATA_4G:
                            transactionType = SimUsage.INQUIRY_BALANCE_DATA;
                            break;

                        case Message.BALANCE_TYPE_SMS:
                            transactionType = SimUsage.INQUIRY_BALANCE_SMS;
                            break;

                        case Message.BALANCE_TYPE_CALL:
                            transactionType = SimUsage.INQUIRY_BALANCE_CALL;
                            break;

                        default:
                            break;
                    }
                    break;

                case Message.EVENT_TYPE_PURCHASE:
                    switch (packageType) {
                        case Message.BALANCE_TYPE_PULSA_MAIN:
                        case Message.BALANCE_TYPE_PULSA_PROMOTION:
                            transactionType = SimUsage.PURCHASE_PULSA_PACK;
                            break;

                        case Message.BALANCE_TYPE_DATA_GENERIC:
                        case Message.BALANCE_TYPE_DATA_2G:
                        case Message.BALANCE_TYPE_DATA_3G:
                        case Message.BALANCE_TYPE_DATA_4G:
                            transactionType = SimUsage.PURCHASE_DATA_PACK;
                            break;

                        case Message.BALANCE_TYPE_SMS:
                            transactionType = SimUsage.PURCHASE_SMS_PACK;
                            break;

                        case Message.BALANCE_TYPE_CALL:
                            transactionType = SimUsage.PURCHASE_CALL_PACK;
                            break;

                        default:
                            break;
                    }
            }

            switch (transactionType) {

                case SimUsage.INQUIRY_BALANCE_PULSA:
                case SimUsage.INQUIRY_BALANCE_DATA:
                case SimUsage.INQUIRY_BALANCE_SMS:
                case SimUsage.INQUIRY_BALANCE_CALL:

                case SimUsage.PURCHASE_PULSA_PACK:
                case SimUsage.PURCHASE_DATA_PACK:
                case SimUsage.PURCHASE_SMS_PACK:
                case SimUsage.PURCHASE_CALL_PACK:

                    if (parsedMessage.containsKey(Constants.ALERT_BY_SMS)) break;

                    // Create the object to save
                    SimUsage simUsage = new SimUsage();

                    // If an exact match already exist right before, overwrite the message
                    SimUsage previousSimUsage = MyBalDatabase.getInstance(context).simUsageDao().loadLatestBalance(callerSlot, transactionType);
                    if (previousSimUsage != null) {
                        if (previousSimUsage.originalMessage.equals(originalMessage)) {
                            simUsage.id = previousSimUsage.id;
                            isExist = true;
                        }
                    }

                    simUsage.slot = callerSlot;
                    simUsage.originalMessage = originalMessage;
                    if (parsedMessage.get(KEY_FOUND_REGEX_ID) != null)
                        simUsage.regexId = Integer.valueOf(parsedMessage.get(KEY_FOUND_REGEX_ID));

                    simUsage.transactionType = transactionType;
                    simUsage.createdAt = Constants.INSTANCE.getDateStorageFormat().format(new Date());


                    // If it's no pack, skip
                    if (parsedMessage.containsKey(Constants.NO_PACK)) {
                        simUsage.noPack = true;
                    }

                    // Fetch the amount and unit according to the transaction type
                    simUsage = integrateAmountAndUnitInto(simUsage, transactionType, parsedMessage);

                    // Fetch the expiry date and integrate it
                    simUsage = integrateExpiryDateInto(simUsage, transactionType, parsedMessage);

                    // Save the SimUsage object, along with its label-text relationship in another table
                    long rowId = MyBalDatabase.getInstance(context).simUsageDao().insertSingleSimUsage(simUsage);

                    RestClient.getClient(context).postSimBalanceDetail(
                            (int) simUsage.amount,
                            simUsage.unit,
                            simUsage.expiry,
                            simUsage.regexId,
                            simUsage.slot,
                            originalMessage,
                            TelephonyInfo.Companion.getInstance(context).getImsiSIM1()
                    ).enqueue(new Callback<SimUsage>() {
                        @Override
                        public void onResponse(@NonNull Call<SimUsage> call, @NonNull Response<SimUsage> response) {
                            Log.d("###", "onResponse() called with: call = [" + call + "], response = [" + response + "]");
                        }

                        @Override
                        public void onFailure(@NonNull Call<SimUsage> call, @NonNull Throwable t) {
                            Log.d("###", "onFailure() called with: call = [" + call + "], t = [" + t + "]");
                        }
                    });

                    // If the SimUsage itself isn't already saved, save the label-text arrays
                    if (!isExist) {
                        List<SimUsage.LabelText> labelTexts = new ArrayList<>();
                        for (Map.Entry<String, String> entry : parsedMessage.entrySet())
                            labelTexts.add(new SimUsage.LabelText(rowId, entry.getKey(), entry.getValue()));

                        MyBalDatabase.getInstance(context).simUsageDao().insertLabelTexts(labelTexts);
                    }
                    break;

                default:
                    break;
            }
        }
    }

    private static SimUsage integrateExpiryDateInto(SimUsage simUsage, int transactionType, Map<String, String> parsedMessage) {

        if (parsedMessage.get(Magic.KEY_ADDITIONAL_REGEX) == null) {

            simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;

        } else {

            switch (transactionType) {

                case SimUsage.INQUIRY_BALANCE_PULSA:
                case SimUsage.PURCHASE_PULSA_PACK:

                    // It's a JsonObject in the String form, so parse it and find the first regex
                    try {

                        JSONObject jsonObject = new JSONObject(parsedMessage.get(Magic.KEY_ADDITIONAL_REGEX));
                        String datePattern = jsonObject.getString(Constants.MAIN_BALANCE_EXPIRY);
                        DateFormat expiryDateFormat = new SimpleDateFormat(datePattern, Locale.getDefault());

                        try {
                            simUsage.expiry = Constants.INSTANCE.getDateStorageFormat()
                                    .format(expiryDateFormat.parse(parsedMessage.get(Constants.MAIN_BALANCE_EXPIRY)));

                        } catch (ParseException pe) {
                            simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                        }
                    } catch (JSONException jsone) {
                        simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                    }

                    break;


                case SimUsage.INQUIRY_BALANCE_DATA:
                case SimUsage.PURCHASE_DATA_PACK:

                    try {

                        JSONObject jsonObject = new JSONObject(parsedMessage.get(Magic.KEY_ADDITIONAL_REGEX));
                        String datePattern = jsonObject.getString(Constants.MAIN_DATA_EXPIRY_DATE);

                        // If date is unknown (doesn't contain DAY or MONTH), set it to today
                        if (!datePattern.contains("dd") || !datePattern.contains("MM")) {

                            // today
                            Calendar defaultExpiryDate = Calendar.getInstance();

                            // reset hour, minutes, seconds and millis
                            defaultExpiryDate.set(Calendar.HOUR_OF_DAY, 23);
                            defaultExpiryDate.set(Calendar.MINUTE, 59);
                            defaultExpiryDate.set(Calendar.SECOND, 59);
                            defaultExpiryDate.set(Calendar.MILLISECOND, 0);

                            simUsage.expiry = Constants.INSTANCE.getDateStorageFormat()
                                    .format(defaultExpiryDate.getTime());
                            break;

                        } else {

                            DateFormat expiryDateFormat = new SimpleDateFormat(datePattern, Locale.getDefault());

                            try {
                                simUsage.expiry = Constants.INSTANCE.getDateStorageFormat()
                                        .format(expiryDateFormat.parse(parsedMessage.get(Constants.MAIN_DATA_EXPIRY_DATE)));

                            } catch (ParseException e) {
                                simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                            }
                        }

                    } catch (JSONException e) {
                        simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                    }

                    break;


                case SimUsage.INQUIRY_BALANCE_SMS:
                case SimUsage.PURCHASE_SMS_PACK:

                    try {

                        JSONObject jsonObject = new JSONObject(parsedMessage.get(Magic.KEY_ADDITIONAL_REGEX));
                        String datePattern = jsonObject.getString(Constants.SMS_EXPIRY_DATE);
                        DateFormat expiryDateFormat = new SimpleDateFormat(datePattern, Locale.getDefault());

                        try {
                            simUsage.expiry = Constants.INSTANCE.getDateStorageFormat()
                                    .format(expiryDateFormat.parse(parsedMessage.get(Constants.SMS_EXPIRY_DATE)));
                        } catch (ParseException e) {
                            simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                        }

                    } catch (JSONException e) {
                        simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                    }

                    break;


                case SimUsage.INQUIRY_BALANCE_CALL:
                case SimUsage.PURCHASE_CALL_PACK:

                    try {

                        JSONObject jsonObject = new JSONObject(parsedMessage.get(Magic.KEY_ADDITIONAL_REGEX));
                        String datePattern = jsonObject.getString(Constants.CALL_LOCAL_EXPIRY_DATE);
                        DateFormat expiryDateFormat = new SimpleDateFormat(datePattern, Locale.getDefault());

                        try {
                            simUsage.expiry = Constants.INSTANCE.getDateStorageFormat()
                                    .format(expiryDateFormat.parse(parsedMessage.get(Constants.CALL_LOCAL_EXPIRY_DATE)));
                        } catch (ParseException e) {
                            simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                        }

                    } catch (JSONException e) {
                        simUsage.expiry = SimUsage.EXPIRY_DATE_UNKNOWN;
                    }

                    break;
            }
        }

        return simUsage;
    }

    private static SimUsage integrateAmountAndUnitInto(SimUsage simUsage, int transactionType, Map<String, String> parsedMessage) {

        switch (transactionType) {

            case SimUsage.INQUIRY_BALANCE_PULSA:
            case SimUsage.PURCHASE_PULSA_PACK:

                // Balance
                if (parsedMessage.get(Constants.MAIN_BALANCE_AMOUNT_CURRENT) == null)
                    simUsage.amount = 0.0;
                else try {
                    simUsage.amount = Double.valueOf(parsedMessage.get(Constants.MAIN_BALANCE_AMOUNT_CURRENT));
                } catch (Exception e) {
                    simUsage.amount = 0.0;
                }

                // Unit
                if (parsedMessage.get(Constants.CURRENCY) != null)
                    switch (parsedMessage.get(Constants.CURRENCY).toLowerCase()) {
                        case "idr":
                        case "rp":
                        case "rupiah":
                            simUsage.unit = SimUsage.UNIT_CURRENCY_IDR;
                            break;
                    }
                else simUsage.unit = SimUsage.UNIT_CURRENCY_IDR;

                break;


            case SimUsage.INQUIRY_BALANCE_DATA:
            case SimUsage.PURCHASE_DATA_PACK:

                // Balance
                if (parsedMessage.get(Constants.MAIN_DATA_MRP_MAIN_BALANCE) != null)
                    simUsage.amount = Double.valueOf(parsedMessage.get(Constants.MAIN_DATA_MRP_MAIN_BALANCE));
                else simUsage.amount = 0.0;


                // Unit
                if (parsedMessage.get(Constants.DATA_UNIT) != null) {

                    switch (parsedMessage.get(Constants.DATA_UNIT).toLowerCase()) {

                        case "b":
                        case "byte":
                            simUsage.unit = SimUsage.UNIT_DATA_BYTE;
                            break;

                        case "kb":
                        case "kilobyte":
                            simUsage.unit = SimUsage.UNIT_DATA_KILOBYTE;
                            break;

                        case "mb":
                        case "megabyte":
                            simUsage.unit = SimUsage.UNIT_DATA_MEGABYTE;
                            break;

                        case "gb":
                        case "gigabyte":
                            simUsage.unit = SimUsage.UNIT_DATA_GIGABYTE;
                            break;

                        default:
                            break;
                    }

                } else {
                    simUsage.unit = SimUsage.UNIT_UNKNOWN;
                }

                break;

            case SimUsage.INQUIRY_BALANCE_SMS:
            case SimUsage.PURCHASE_SMS_PACK:

                // Amount
                if (parsedMessage.get(Constants.SMS_COUNT_CURRENT) != null)
                    simUsage.amount = Double.valueOf(parsedMessage.get(Constants.SMS_COUNT_CURRENT));
                else
                    simUsage.amount = 0.0;

                // Unit
                simUsage.unit = SimUsage.UNIT_MESSAGE_SMS_COUNT;


            case SimUsage.INQUIRY_BALANCE_CALL:
            case SimUsage.PURCHASE_CALL_PACK:

                // Amount
                if (parsedMessage.get(Constants.CALL_LOCAL_CURRENT) != null)
                    simUsage.amount = Double.valueOf(parsedMessage.get(Constants.CALL_LOCAL_CURRENT));
                else
                    simUsage.amount = 0.0;

                // Unit
                simUsage.unit = SimUsage.UNIT_CALL_MINUTE;
                break;

            default:
                break;
        }

        return simUsage;
    }

    public static void showPopup(Activity activity, Map<String, String> patternedElements, String rawMessage, int providerId, int balanceTypeRequested) {

        StringBuilder patternedContent = new StringBuilder();
        String ussdCode = MyBalDatabase.getInstance(activity).ussdCodeDao().getUssdCode(providerId, balanceTypeRequested).code;

        patternedContent.append("FOUND PATTERNS").append("\n");
        patternedContent.append("USSD: ").append(ussdCode);
        if (patternedElements.get(Magic.KEY_FOUND_MESSAGE_ID) != null && patternedElements.get(Magic.KEY_FOUND_REGEX_ID) != null) {
            patternedContent
                    .append(", Msg: ").append(patternedElements.get(Magic.KEY_FOUND_MESSAGE_ID))
                    .append(", Reg: ").append(patternedElements.get(Magic.KEY_FOUND_REGEX_ID));
        }
        patternedContent.append("\n");

        for (Map.Entry<String, String> entry : patternedElements.entrySet()) {
            if (!entry.getKey().equalsIgnoreCase(Magic.KEY_FOUND_MESSAGE_ID) && !entry.getKey().equalsIgnoreCase(Magic.KEY_FOUND_REGEX_ID))
                patternedContent.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }

        List<MessageRegex> messageRegexes = MyBalDatabase.getInstance(activity).messageRegexDao().getLivePatternsForProvider(providerId);

        patternedContent.append("\n\n").append("RAW MESSAGE").append("\n").append(rawMessage);

        patternedContent.append("\n\n").append("LIVE PATTERNS").append("\n");
        for (MessageRegex messageRegex : messageRegexes) {
            patternedContent
                    .append("Msg ID: ").append(messageRegex.getMessageId().id).append(" | ")
                    .append("Reg ID: ").append(messageRegex.getId()).append("\n")
                    .append(messageRegex.getRegex()).append("\n")
                    .append("--------------").append("\n");
        }

        new AlertDialog.Builder(activity)
                .setTitle("Pattern-Value Pairs")
                .setMessage(patternedContent.toString())
                .setNeutralButton("Close", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("Copy Message", (dialogInterface, i) -> {
                    ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label", rawMessage);
                    if (clipboard != null) {
                        clipboard.setPrimaryClip(clip);
                    }
                })
                .show();
    }

    /**
     * @param simSlot     Either 0 for the first slot, or 1 for the second slot.
     * @param balanceType The balance type from Message object. As per 29 Nov, it's either
     *                    BALANCE_TYPE_PULSA_MAIN,
     *                    BALANCE_TYPE_DATA_GENERIC,
     *                    BALANCE_TYPE_SMS, or
     *                    BALANCE_TYPE_CALL
     * @return The custom preference key
     */
    public static String getCustomUssdPrefKey(int simSlot, int balanceType) {
        switch (simSlot) {

            case BalanceHomeFragment.FIRST_SLOT:
                switch (balanceType) {
                    case Message.BALANCE_TYPE_PULSA_MAIN:
                        return Constants.USSD_NUMBER_PULSA_OP1;
                    case Message.BALANCE_TYPE_DATA_GENERIC:
                        return Constants.USSD_NUMBER_DATA_OP1;
                    case Message.BALANCE_TYPE_SMS:
                        return Constants.USSD_NUMBER_SMS_OP1;
                    case Message.BALANCE_TYPE_CALL:
                        return Constants.USSD_NUMBER_CALL_OP1;
                    default:
                        return "";
                }

            case BalanceHomeFragment.SECOND_SLOT:
                switch (balanceType) {
                    case Message.BALANCE_TYPE_PULSA_MAIN:
                        return Constants.USSD_NUMBER_PULSA_OP2;
                    case Message.BALANCE_TYPE_DATA_GENERIC:
                        return Constants.USSD_NUMBER_DATA_OP2;
                    case Message.BALANCE_TYPE_SMS:
                        return Constants.USSD_NUMBER_SMS_OP2;
                    case Message.BALANCE_TYPE_CALL:
                        return Constants.USSD_NUMBER_CALL_OP2;
                    default:
                        return "";
                }

            default:
                return "";
        }
    }

    // Adds a space after every 4 characters, for easier number reading
    // https://stackoverflow.com/a/537190
    public static String insertPeriodically(String text, String insert, int period) {
        StringBuilder builder =
                new StringBuilder(text.length() + insert.length() * (text.length() / period) + 1);

        int index = 0;
        String prefix = "";
        while (index < text.length()) {
            // Don't put the insert in the very first iteration.
            // This is easier than appending it *after* each substring
            builder.append(prefix);
            prefix = insert;
            builder.append(text.substring(index, Math.min(index + period, text.length())));
            index += period;
        }
        return builder.toString();
    }

}
