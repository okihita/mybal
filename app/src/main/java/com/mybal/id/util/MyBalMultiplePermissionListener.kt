package com.mybal.id.util

import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mybal.id.splash.SplashActivity

// Code from Dexter library's example
class MyBalMultiplePermissionListener(private val activity: SplashActivity) : MultiplePermissionsListener {

    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
        report.grantedPermissionResponses
                .forEach { activity.afterPermissionGranted(it.permissionName) }
        report.deniedPermissionResponses
                .forEach { activity.afterPermissionDenied(it.permissionName, it.isPermanentlyDenied) }
    }

    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
        activity.showPermissionRationale()
    }
}