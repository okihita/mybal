package com.mybal.id.util

import android.util.Log

import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequestErrorListener

class MyBalErrorListener : PermissionRequestErrorListener {
    override fun onError(error: DexterError) {
        Log.e("Dexter", "There was an error: " + error.toString())
    }
}
