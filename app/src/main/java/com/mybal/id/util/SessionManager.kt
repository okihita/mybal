package com.mybal.id.util

import android.content.Context
import android.util.Log
import com.mybal.id.db.Device
import com.mybal.id.network.RestClient
import com.mybal.id.network.TokenStatus
import com.mybal.id.siminfo.TelephonyInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO there was a big change for the auth flow on mid-December.
// Many things need to be fixed since we have multiple-login flow.
class SessionManager {

    companion object {

        fun getAuthToken(context: Context): String {

            val authPref = context.getSharedPreferences(Constants.PREF_KEY_TOKEN_AUTH, Context.MODE_PRIVATE)!!

            return if (authPref.contains(Constants.PREF_KEY_TOKEN_AUTH)) {
                authPref.getString(Constants.PREF_KEY_TOKEN_AUTH, "")
            } else {
                ""
            }
        }

        fun storeAuthToken(context: Context, token: String) {
            context.getSharedPreferences(Constants.PREF_KEY_TOKEN_AUTH, Context.MODE_PRIVATE)!!
                    .edit()
                    .putString(Constants.PREF_KEY_TOKEN_AUTH, token)
                    .apply()
        }

        fun checkTokenValidity(context: Context) {

            val token = getAuthToken(context)

            if (token != "") {
                RestClient.getClient(context).tokenAuthCheck(token)
                        .enqueue(object : Callback<TokenStatus> {
                            override fun onResponse(call: Call<TokenStatus>?, response: Response<TokenStatus>?) {
                                if (response!!.isSuccessful) {
                                    if (response.body()!!.tokenStatus == "valid") {
                                        Log.i("###", "token valid until: " + response.body()!!.tokenExpTime)
                                    } else {
                                        Log.i("###", "invalid token. Refreshing...: ")

                                        RestClient.getClient(context)
                                                .loginRefreshTokenDevice(TelephonyInfo.getInstance(context).imsiSIM1.toString())
                                                .enqueue(object : Callback<Device> {
                                                    override fun onResponse(call: Call<Device>?, response: Response<Device>) {
                                                        if (response.isSuccessful) {
                                                            response.body()?.let { SessionManager.storeAuthToken(context, it.tokenAuth) }
                                                            Log.i("###", "new token acquired: ")
                                                        } else {
                                                            Log.i("###", "response outside 200: ")
                                                        }
                                                    }

                                                    override fun onFailure(call: Call<Device>?, t: Throwable?) {
                                                        Log.i("###", "token refresh failed: ")
                                                    }
                                                })
                                    }
                                }
                            }

                            override fun onFailure(call: Call<TokenStatus>?, t: Throwable?) {
                                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                            }

                        })
            } else {
                Log.i("###", "Session: You have no token")
            }

        }
    }
}