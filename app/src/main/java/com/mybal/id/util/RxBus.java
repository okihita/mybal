package com.mybal.id.util;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * This class is used as an event bus for smooth asynchronous communication between activities and
 * services. Beware of steep learning curve. RxJava is hard to learn at first. Brace yourself!
 */
public class RxBus {

    // Constants for the in-app event bus
    public static final int FLAG_CLOSE_ACCESSIBILITY_FLOATING_GUIDE = 101; // User already follows the instruction

    public static final int FLAG_REQUEST_BALANCE_START_LISTENING = 201; // Ask the service to listen
    public static final int FLAG_REQUEST_BALANCE_INCOMING_USSD = 202; // Send the USSD content to Balance Screen
    public static final int FLAG_REQUEST_BALANCE_INCOMING_SMS = 203; // Send the signal that an SMS has received to Balance Screen

    public static final int FLAG_OUTGOING_USSD_CALL_NUMBER = 301; // User makes a phone call for USSD
    public static final int FLAG_OUTGOING_USSD_CALL_SIM_SLOT = 302; // Which slot makes the call


    private static RxBus instance;

    // https://blog.mindorks.com/understanding-rxjava-subject-publish-replay-behavior-and-async-subject-224d663d452f
    private final PublishSubject<Payload> subject = PublishSubject.create();

    public static RxBus getInstance() {
        if (instance == null) instance = new RxBus();
        return instance;
    }

    /**
     * Broadcast a message with a Payload object that contains a flag and its value.
     *
     * @param payload A specific payload object
     */
    public void publish(Payload payload) {
        subject.onNext(payload);
    }

    public Observable<Payload> getObservables() {
        return subject;
    }

    public static class Payload {

        public int requestCode = 0;
        public boolean boolPayload = false;
        public int intPayload = 0;
        public String stringPayload = "";

        public Payload(int requestCode, boolean boolPayload) {
            this.requestCode = requestCode;
            this.boolPayload = boolPayload;
        }

        public Payload(int requestCode, int intPayload) {
            this.requestCode = requestCode;
            this.intPayload = intPayload;
        }

        public Payload(int requestCode, String stringPayload) {
            this.requestCode = requestCode;
            this.stringPayload = stringPayload;
        }
    }
}
