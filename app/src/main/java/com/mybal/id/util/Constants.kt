package com.mybal.id.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object Constants {

    const val DEVELOPMENT_URL = "https://developer.mybalapp.com/api/"

    // https://stackoverflow.com/a/38605194
    val simSlotNames = arrayOf(
            "extra_asus_dial_use_dualsim",
            "com.android.phone.extra.slot",
            "slot",
            "simslot",
            "sim_slot",
            "subscription",
            "Subscription",
            "phone",
            "com.android.phone.DialingMode",
            "simSlot",
            "slot_id",
            "simId",
            "simnum",
            "phone_type",
            "slotId",
            "slotIdx"
    )


    // HTTP RESPONSE CODES
    const val HTTP_RESPONSE_FORBIDDEN = 403


    // LIST OF BALANCES
    // This matches results from api/regex_options
    const val MAIN_BALANCE_MRP = "P_MAIN_BALANCE_MRP"
    const val MAIN_BALANCE_AMOUNT_BENEFIT = "P_MAIN_BALANCE_AMOUNT_BENEFIT"
    const val MAIN_BALANCE_AMOUNT_CURRENT = "P_MAIN_BALANCE_AMOUNT_CURRENT"
    const val CURRENCY = "P_CURRENCY"
    const val MAIN_BALANCE_EXPIRY = "P_MAIN_BALANCE_EXP_DATE"
    const val SIM_EXPIRY = "P_SIM_EXP_DATE"
    const val SMS_COUNT_BENEFIT = "P_SMS_COUNT_BENEFIT"
    const val SMS_COUNT_CURRENT = "P_SMS_COUNT_CURRENT"
    const val PURCHASE_DATE = "P_PURCHASE_DATE"
    const val SMS_EXPIRY_DATE = "P_SMS_EXP_DATE"
    const val MAIN_DATA_MRP_MAIN_BALANCE = "P_MAIN_DATA_MRP_MAIN_BALANCE"
    const val MAIN_DATA_MRP = "P_MAIN_DATA_MRP"
    const val DATA_UNIT = "P_DATA_UNIT"
    const val CALL_LOCAL_BENEFIT = "P_CALL_LOCAL_BENEFIT"
    const val CALL_LOCAL_CURRENT = "P_CALL_LOCAL_CURRENT"
    const val CALL_LOCAL_EXPIRY_DATE = "P_CALL_LOCAL_EXP_DATE"
    const val CALL_LOCAL_MRP_MAIN_BALANCE = "P_CALL_LOCAL_MRP_MAIN_BALANCE"
    const val CALL_UNIT = "P_CALL_UNIT"
    const val MAIN_DATA_EXPIRY_DATE = "P_MAIN_DATA_EXP_DATE"
    const val CALL_LOCAL_MRP = "P_CALL_LOCAL_MRP"
    const val HOURS_DATA_EXPIRY_DATE = "P_HRS_DATA_EXP_DATE"
    const val DATA_3G_MRP_MAIN_BALANCE = "P_3G_DATA_MRP_MAIN_BALANCE"
    const val DATA_3G_EXP_DATE = "P_3G_DATA_EXP_DATE"
    const val DATA_4G_MRP = "P_4G_DATA_MRP"
    const val DATA_4G_MRP_MAIN_BALANCE = "P_4G_DATA_MRP_MAIN_BALANCE"
    const val DATA_2G_MRP = "P_2G_DATA_MRP"
    const val DATA_2G_MRP_MAIN_BALANCE = "P_2G_DATA_MRP_MAIN_BALANCE"
    const val DATA_2G_EXP_DATE = "P_2G_DATA_EXP_DATE"
    const val DATA_3G_MRP = "P_3G_DATA_MRP"
    const val APP_DATA_EXP_DATE = "P_APP_DATA_EXP_DATE"
    const val APP_DATA_MRP = "P_APP_DATA_MRP"
    const val DATA_4G_EXP_DATE = "P_4G_DATA_EXP_DATE"
    const val APP_DATA_NAME = "P_APP_DATA_NAME"


    // LIST OF CUSTOM USSD NUMBER
    const val USSD_NUMBER_PULSA_OP1 = "pulsaop1"
    const val USSD_NUMBER_DATA_OP1 = "dataop1"
    const val USSD_NUMBER_SMS_OP1 = "smsop1"
    const val USSD_NUMBER_CALL_OP1 = "callop1"
    const val USSD_NUMBER_PULSA_OP2 = "pulsaop2"
    const val USSD_NUMBER_DATA_OP2 = "dataop2"
    const val USSD_NUMBER_SMS_OP2 = "smsop2"
    const val USSD_NUMBER_CALL_OP2 = "callop2"


    // LIST OF UNPROCESSED LABELS
    const val ALERT_BY_SMS = "__ALERT_BY_SMS__PATTERN__"
    const val NO_PACK = "__NO_PACK__PATTERN__"


    // PREFERENCES' KEY
    const val PREF_KEY_TOKEN_AUTH = "com.mybal.id.auth"
    const val PREF_KEY_IS_FIRST_ACCESSIBILITY_CONNECT = "com.mybal.id.IS_FIRST_ACCESSIBILITY_CONNECT"
    const val PREF_KEY_TOUR_SKIP = "com.mybal.id.PREFERENCE_KEY_TOUR_SKIP"
    const val PREF_KEY_PROVIDER_ID = "com.mybal.id.PREFERENCE_KEY_OPERATOR_ID"
    const val PREF_KEY_AGREED_TOS = "com.mybal.id.PREF_KEY_AGREED_TO_TOS"
    const val PROVIDER_ID_SLOT_1 = "com.mybal.id.provider_id_slot_1"
    const val PROVIDER_ID_SLOT_2 = "com.mybal.id.provider_id_slot_2"
    const val PHONE_NUMBER_SLOT_1 = "com.mybal.id.phone_number_slot_1"
    const val PHONE_NUMBER_SLOT_2 = "com.mybal.id.phone_number_slot_2"
    const val SIM_TYPE_SLOT_1 = "com.mybal.id.sim_type_slot_1"
    const val SIM_TYPE_SLOT_2 = "com.mybal.id.sim_type_slot_2"
    const val XL_AXIS_SLOT_1 = "com.mybal.id.xl_axis_slot_1"
    const val XL_AXIS_SLOT_2 = "com.mybal.id.xl_axis_slot_2"


    // LANGUAGE SELECTION KEYS
    const val LOCALE_CODE_INDONESIA = "in"
    const val LOCALE_CODE_ENGLISH = "en"


    // DATE FORMATTING
    private const val DATE_FORMAT_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    val humanDateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
    val dateStorageFormat: DateFormat = SimpleDateFormat(Constants.DATE_FORMAT_ISO_8601, Locale.getDefault())


    // PORTAL MENUS' NAMES
    val PORTAL_SPECIAL_OFFERS = "Special Offers"
    val PORTAL_BUY_DATA = "Buy Data"
    val PORTAL_BUY_CALL = "Buy Call"
    val PORTAL_PORTAL_MENU = "Portal"
    val PORTAL_SHARE_TALK_TIME = "Share Talk Time"

}