package com.mybal.id.util

import android.app.Application
import android.content.Intent
import com.facebook.stetho.Stetho
import com.mybal.id.BuildConfig
import com.mybal.id.accessibility.MyBalAccessibilityService
import com.mybal.id.db.*
import com.mybal.id.network.MyBalApi
import com.mybal.id.network.RestClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyBalApp : Application() {

    private lateinit var myBalApi: MyBalApi
    private lateinit var db: MyBalDatabase

    override fun onCreate() {
        super.onCreate()

        // Enables Chrome debugging only when the app is in debug variant
        if (BuildConfig.DEBUG) Stetho.initializeWithDefaults(this)

        // Starts listening to accessibility (USSD, SMS) events
        startService(Intent(this, MyBalAccessibilityService::class.java))

        // Initiate database instance
        db = MyBalDatabase.getInstance(this)

        myBalApi = RestClient.getClient(this)
    }

    fun updateOfflineDb() {
        updateProviders()
        updateCards()
        updateMessageRegexes()
        updateUssdCodes()
        updateProviderSenders()
        updatePortalMenus()
        updateRegexOptions()
        updateCardNumberPrefixes()
        updateTopupCodes()
    }

    private fun updateTopupCodes() {
        myBalApi.topupCodes.enqueue(object : Callback<List<TopupCode>> {
            override fun onResponse(call: Call<List<TopupCode>>?, response: Response<List<TopupCode>>) {
                if (response.isSuccessful)
                    db.topupCodeDao().insertTopupCodes(response.body())
            }

            override fun onFailure(call: Call<List<TopupCode>>?, t: Throwable?) {}
        })
    }

    private fun updateCardNumberPrefixes() {
        myBalApi.cardNumberPrefixes.enqueue(object : Callback<List<CardNumberPrefix>> {
            override fun onResponse(call: Call<List<CardNumberPrefix>>?, response: Response<List<CardNumberPrefix>>) {
                if (response.isSuccessful)
                    db.cardNumberPrefixDao().insertCardNumberPrefix(response.body())
            }

            override fun onFailure(call: Call<List<CardNumberPrefix>>?, t: Throwable?) {}
        })
    }

    private fun updatePortalMenus() {
        myBalApi.portalMenus.enqueue(object : Callback<List<PortalMenu>> {
            override fun onResponse(call: Call<List<PortalMenu>>, response: Response<List<PortalMenu>>) {
                if (response.isSuccessful)
                    db.portalMenuDao().insertPortalMenus(response.body())
            }

            override fun onFailure(call: Call<List<PortalMenu>>, t: Throwable) {}
        })
    }

    private fun updateRegexOptions() {
        myBalApi.regexOptions.enqueue(object : Callback<List<RegexOption>> {
            override fun onResponse(call: Call<List<RegexOption>>, response: Response<List<RegexOption>>) {
                if (response.isSuccessful)
                    db.regexOptionDao().insertRegexOptions(response.body())
            }

            override fun onFailure(call: Call<List<RegexOption>>, t: Throwable) {}
        })
    }

    private fun updateProviderSenders() {
        myBalApi.providerSenders.enqueue(object : Callback<List<ProviderSender>> {
            override fun onResponse(call: Call<List<ProviderSender>>, response: Response<List<ProviderSender>>) {
                if (response.isSuccessful)
                    db.providerSenderDao().insertProviderSenders(response.body())
            }

            override fun onFailure(call: Call<List<ProviderSender>>, t: Throwable) {}
        })
    }

    private fun updateProviders() {
        myBalApi.providers.enqueue(object : Callback<List<Provider>> {
            override fun onResponse(call: Call<List<Provider>>, response: Response<List<Provider>>) {
                if (response.isSuccessful)
                    db.providerDao().insertProvider(response.body())
            }

            override fun onFailure(call: Call<List<Provider>>, t: Throwable) {}
        })
    }

    private fun updateCards() {
        myBalApi.cards.enqueue(object : Callback<List<Card>> {
            override fun onResponse(call: Call<List<Card>>, response: Response<List<Card>>) {
                if (response.isSuccessful)
                    db.cardDao().insertCards(response.body())
            }

            override fun onFailure(call: Call<List<Card>>, t: Throwable) {}
        })
    }

    private fun updateMessageRegexes() {
        myBalApi.messageRegexes.enqueue(object : Callback<List<MessageRegex>> {
            override fun onResponse(call: Call<List<MessageRegex>>, response: Response<List<MessageRegex>>) {
                if (response.isSuccessful)
                    db.messageRegexDao().insertMessageRegexes(response.body())
            }

            override fun onFailure(call: Call<List<MessageRegex>>, t: Throwable) {}
        })
    }

    private fun updateUssdCodes() {
        myBalApi.ussdCodes.enqueue(object : Callback<List<UssdCode>> {
            override fun onResponse(call: Call<List<UssdCode>>, response: Response<List<UssdCode>>) {
                if (response.isSuccessful)
                    db.ussdCodeDao().insertUssdCodes(response.body())
            }

            override fun onFailure(call: Call<List<UssdCode>>, t: Throwable) {}
        })
    }
}
