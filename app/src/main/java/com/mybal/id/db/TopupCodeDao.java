package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface TopupCodeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTopupCodes(List<TopupCode> topupCodes);

    // Select for certain provider
    @Query("SELECT * FROM topup_code WHERE providerId = :providerId")
    List<TopupCode> loadByProvider(int providerId);
}
