package com.mybal.id.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "ussd_code")
public class UssdCode {

    // This corresponds to the api/message_pack_types

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public int id;

    @ColumnInfo(name = "cards_id")
    @SerializedName("cards_id")
    @Expose
    public int card;

    @ColumnInfo(name = "ussd_functions_id")
    @SerializedName("ussd_functions_id")
    public int function; // Should be "balance type". Stupid semantic by WGS.

    @SerializedName("ussd_code")
    @Expose
    public String code;
}
