package com.mybal.id.db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("token_auth")
    @Expose
    public String tokenAuth;

    @SerializedName("is_blocked")
    @Expose
    public boolean isBlocked;
}
