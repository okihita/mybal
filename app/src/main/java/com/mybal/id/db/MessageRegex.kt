package com.mybal.id.db

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

// The annotations here are combinations of ones from Retrofit and Room.
// Master both libraries before visiting this class.
// Understanding this class also requires you to master Regular Expression and its String representation.
@Entity(tableName = "message_regex")
class MessageRegex {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("regex")
    @Expose
    var regex: String? = null

    @SerializedName("additional_regex")
    @Expose
    var dateRegex: String? = null

    // https://developer.android.com/reference/android/arch/persistence/room/Embedded.html
    @Embedded
    @SerializedName("message_id")
    @Expose
    var messageId: Message? = null

    @SerializedName("pattern")
    @Expose
    var pattern: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null
}