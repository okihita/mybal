package com.mybal.id.db;

import android.arch.persistence.room.ColumnInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Message {

    // Why the user gets the message
    public static final int EVENT_TYPE_INQUIRY = 1;
    public static final int EVENT_TYPE_PURCHASE = 2;
    public static final int EVENT_TYPE_SPENDING = 3;
    public static final int EVENT_TYPE_EXPIRED = 4;
    public static final int EVENT_TYPE_WARNING = 5;
    public static final int EVENT_TYPE_DISMISS = 6;
    public static final int EVENT_TYPE_NOT_PROCESSED = 7;
    public static final int EVENT_TYPE_BILL = 8;

    // How the user gets the message
    public static final int RECEIVE_TYPE_USSD = 1;
    public static final int RECEIVE_TYPE_SMS = 2;

    // What the message contains
    public static final int BALANCE_TYPE_PULSA_MAIN = 2;
    public static final int BALANCE_TYPE_PULSA_PROMOTION = 3;
    public static final int BALANCE_TYPE_DATA_2G = 4;
    public static final int BALANCE_TYPE_DATA_3G = 5;
    public static final int BALANCE_TYPE_DATA_4G = 6;
    public static final int BALANCE_TYPE_DATA_GENERIC = 7;
    public static final int BALANCE_TYPE_CALL = 8;
    public static final int BALANCE_TYPE_SMS = 9;
    public static final int BALANCE_TYPE_PHONE_NUMBER = 10;
    public static final int BALANCE_TYPE_MANY = 11;
    public static final int BALANCE_TYPE_POSTPAID = 12;
    public static final int BALANCE_TYPE_NONE = 13;
    public static final int BALANCE_TYPE_UNKNOWN = 14;
    public static final int BALANCE_TYPE_RETRY = 15;

    // Default values
    public static final int DEFAULT_UNKNOWN_USER_ID = 0;
    public static final String USSD_SENDER_ID = "-";

    @ColumnInfo(name = "embedded_message_id")
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("text_recieve_type")
    @Expose
    public int textRecieveType;
    // 1 for USSD, 2 for SMS

    @SerializedName("message")
    @Expose
    public String message;
    // Ignore as per 9 Nov

    @SerializedName("text_content")
    @Expose
    public String textContent;

    @SerializedName("text_ussd")
    @Expose
    public String textUssd;
    // The USSD sequence used to summon this message

    @SerializedName("text_sender")
    @Expose
    public String textSender;
    // If it's from SMS, get the sender ID, e.g. "TELKOMSEL"

    /**
     * Can be INQUIRY or PURCHASE as per 24 November
     */
    @SerializedName("text_event_type")
    @Expose
    public int eventType;

    /**
     * See UssdCode.java for this.
     */
    @SerializedName("text_pack_type")
    @Expose
    public String textPackType;

    @SerializedName("text_user_id")
    @Expose
    public int textUserId;

    @SerializedName("text_provider_id")
    @Expose
    public int textProviderId;
}
