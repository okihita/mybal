package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PortalMenuDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPortalMenus(List<PortalMenu> portalMenus);

    @Query("SELECT * FROM portal_menu WHERE providerId = :providerId AND name = :name")
    PortalMenu getPortalMenuForProviderFunction(int providerId, String name);
}
