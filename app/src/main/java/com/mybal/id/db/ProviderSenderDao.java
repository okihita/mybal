package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ProviderSenderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProviderSenders(List<ProviderSender> providerSenders);

    @Query("SELECT * FROM provider_sender WHERE providerId = :providerId")
    List<ProviderSender> getSendersByProvider(int providerId);
}
