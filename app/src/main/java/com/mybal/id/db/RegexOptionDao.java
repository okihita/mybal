package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import java.util.List;

@Dao
public interface RegexOptionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRegexOptions(List<RegexOption> options);
}
