package com.mybal.id.db;

import android.annotation.TargetApi;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Build;
import android.telephony.SubscriptionInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mybal.id.R;

@Entity(tableName = "provider")
public class Provider {

    private static final int MCC_INDONESIA = 510;

    private static final int MNC_TELKOMSEL = 10;
    private static final int MNC_INDOSAT_OOREDO = 1;
    private static final int MNC_XL = 11;
    private static final int MNC_AXIS = 11;
    private static final int MNC_SMARTFREN = 9;
    private static final int MNC_THREE = 89;

    private static final int PROVIDER_ID_UNKNOWN = 0;
    private static final int PROVIDER_ID_TELKOMSEL = 1;
    private static final int PROVIDER_ID_INDOSAT = 2;
    private static final int PROVIDER_ID_XL = 3;
    private static final int PROVIDER_ID_AXIS = 4;
    private static final int PROVIDER_ID_THREE = 5;
    private static final int PROVIDER_ID_SMARTFREN = 6;

    @SerializedName("id")
    @PrimaryKey
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("mcc")
    @Expose
    public int mcc;
    @SerializedName("mnc")
    @Expose
    public int mnc;

    @SerializedName("country_code")
    @ColumnInfo(name = "country_code")
    @Expose
    public int countryCode;
    // In case user is roaming and using MCC-MNC different than its own
    // Without "+" sign. Indonesia = 62, Malaysia = 60.

    public Provider() {
        id = 0;
        name = "";
        mcc = 0;
        mnc = 0;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    public Provider(SubscriptionInfo info) {

        switch (info.getCarrierName().toString().toLowerCase()) {

            case "telkomsel":
            case "t-sel":
            case "tsel":
            case "telkom":
                id = PROVIDER_ID_TELKOMSEL;
                break;

            case "indosat":
            case "indosat ooredoo":
            case "indosatooredoo":
            case "im3":
            case "mentari":
                id = PROVIDER_ID_INDOSAT;
                break;

            case "xl":
            case "xl 4g lte":
            case "xl axiata":
            case "xl lte":
            case "xl 4g":
                id = PROVIDER_ID_XL;
                break;

            case "3":
            case "three":
            case "tri":
            case "thre":
                id = PROVIDER_ID_THREE;
                break;

            case "axis":
                id = PROVIDER_ID_AXIS;
                break;

            case "smartfren":
                id = PROVIDER_ID_SMARTFREN;
                break;

            default:
                id = PROVIDER_ID_UNKNOWN;
                break;
        }

        // Modifying the Mobile Country Code
        switch (id) {
            case PROVIDER_ID_TELKOMSEL:
            case PROVIDER_ID_INDOSAT:
            case PROVIDER_ID_XL:
            case PROVIDER_ID_THREE:
            case PROVIDER_ID_AXIS:
            case PROVIDER_ID_SMARTFREN:
                mcc = MCC_INDONESIA;
                break;
            default:
                mcc = 0;
        }

        // Modifying Network Code
        switch (id) {
            case PROVIDER_ID_TELKOMSEL:
                mnc = MNC_TELKOMSEL;
                break;
            case PROVIDER_ID_INDOSAT:
                mnc = MNC_INDOSAT_OOREDO;
                break;
            case PROVIDER_ID_XL:
                mnc = MNC_XL;
                break;
            case PROVIDER_ID_THREE:
                mnc = MNC_THREE;
                break;
            case PROVIDER_ID_AXIS:
                mnc = MNC_AXIS;
                break;
            case PROVIDER_ID_SMARTFREN:
                mnc = MNC_SMARTFREN;
                break;
            default:
                mnc = 0;
                break;
        }
    }

    public Provider(int providerId) {

        id = providerId;

        // Modifying the Mobile Country Code
        switch (providerId) {
            case PROVIDER_ID_TELKOMSEL:
            case PROVIDER_ID_INDOSAT:
            case PROVIDER_ID_XL:
            case PROVIDER_ID_THREE:
            case PROVIDER_ID_AXIS:
            case PROVIDER_ID_SMARTFREN:
                mcc = MCC_INDONESIA;
                break;
            default:
                mcc = 0;
        }

        // Modifying Network Code
        switch (providerId) {
            case PROVIDER_ID_TELKOMSEL:
                mnc = MNC_TELKOMSEL;
                break;
            case PROVIDER_ID_INDOSAT:
                mnc = MNC_INDOSAT_OOREDO;
                break;
            case PROVIDER_ID_XL:
                mnc = MNC_XL;
                break;
            case PROVIDER_ID_THREE:
                mnc = MNC_THREE;
                break;
            case PROVIDER_ID_AXIS:
                mnc = MNC_AXIS;
                break;
            case PROVIDER_ID_SMARTFREN:
                mnc = MNC_SMARTFREN;
                break;
            default:
                mnc = 0;
                break;
        }

        // Provider name
        switch (providerId) {
            case PROVIDER_ID_TELKOMSEL:
                name = "TELKOMSEL";
                break;
            case PROVIDER_ID_INDOSAT:
                name = "INDOSATOOREDOO";
                break;
            case PROVIDER_ID_XL:
                name = "XL AXIATA";
                break;
            case PROVIDER_ID_THREE:
                name = "THREE";
                break;
            case PROVIDER_ID_AXIS:
                name = "AXIS";
                break;
            case PROVIDER_ID_SMARTFREN:
                name = "SMARTFREN";
                break;
            default:
                name = "UNKNOWN";
                break;
        }
    }

    public static Integer getProviderLogo(int providerId) {
        switch (providerId) {
            case PROVIDER_ID_TELKOMSEL:
                return R.drawable.img_provider_telkomsel;
            case PROVIDER_ID_INDOSAT:
                return R.drawable.img_card_im3;
            case PROVIDER_ID_XL:
                return R.drawable.img_provider_xl;
            case PROVIDER_ID_AXIS:
                return R.drawable.img_provider_axis;
            case PROVIDER_ID_THREE:
                return R.drawable.img_provider_3;
            case PROVIDER_ID_SMARTFREN:
                return 0;
            default:
                return 0;
        }
    }
}
