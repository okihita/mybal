package com.mybal.id.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "provider_sender")
public class ProviderSender {

    @PrimaryKey
    public int id;

    @SerializedName("providers_id")
    public int providerId;

    @SerializedName("category")
    public String category;

    @SerializedName("code_number")
    public String codeNumber;
}