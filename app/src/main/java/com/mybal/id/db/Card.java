package com.mybal.id.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "card")
public class Card {

    @PrimaryKey
    @Expose
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "providers_id")
    @Expose
    @SerializedName("providers_id")
    public int providerId;

    @Expose
    @SerializedName("name")
    public String name;
}
