package com.mybal.id.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

// https://medium.com/@ajaysaini.official/building-database-with-room-persistence-library-ecf7d0b8f3e9
@Database(
        entities = {
                Provider.class,
                RegexOption.class,
                MessageRegex.class,
                UssdCode.class,
                Card.class,
                SimUsage.class,
                SimUsage.LabelText.class,
                ProviderSender.class,
                PortalMenu.class,
                CardNumberPrefix.class,
                TopupCode.class
        },
        version = 34,
        exportSchema = false)
public abstract class MyBalDatabase extends RoomDatabase {

    private static MyBalDatabase instance;

    public static MyBalDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .databaseBuilder(context.getApplicationContext(), MyBalDatabase.class, "mybaldb")
                    .fallbackToDestructiveMigration() // Re-create tables if there's any schema change
                    .allowMainThreadQueries() // To avoid async
                    .build();
        }

        return instance;
    }

    public abstract ProviderDao providerDao();

    public abstract CardDao cardDao();

    public abstract MessageRegexDao messageRegexDao();

    public abstract UssdCodeDao ussdCodeDao();

    public abstract SimUsageDao simUsageDao();

    public abstract ProviderSenderDao providerSenderDao();

    public abstract PortalMenuDao portalMenuDao();

    public abstract RegexOptionDao regexOptionDao();

    public abstract CardNumberPrefixDao cardNumberPrefixDao();

    public abstract TopupCodeDao topupCodeDao();
}
