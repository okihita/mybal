package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface SimUsageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertSingleSimUsage(SimUsage simUsage);

    // For the first tab, "All"
    @Query("SELECT * FROM sim_usage ORDER BY createdAt DESC")
    List<SimUsage> loadAllLatestFirst();

    @Query("SELECT * FROM sim_usage WHERE transactionType IN(:transactionTypes) ORDER BY createdAt DESC")
    List<SimUsage> loadByTransactionTypes(int... transactionTypes);

    @Query("SELECT * FROM sim_usage WHERE slot = :slot AND transactionType = :type ORDER BY createdAt DESC LIMIT 1")
    SimUsage loadLatestBalance(int slot, int type);

    @Delete
    void deleteSingleSimUsage(SimUsage simUsage);

    @Query("DELETE FROM sim_usage")
    void nukeSimUsageTable();

    @Query("DELETE FROM sim_usage_label_text")
    void nukeSimUsageLabelTextTable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLabelTexts(List<SimUsage.LabelText> labelTexts);

    @Query("SELECT * FROM sim_usage_label_text WHERE simUsageId = :simUsageId")
    List<SimUsage.LabelText> getLabelTextsForSimUsage(long simUsageId);
}
