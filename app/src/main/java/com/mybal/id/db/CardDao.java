package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCards(List<Card> cards);

    @Query("SELECT providers_id FROM card WHERE id = :cardId LIMIT 1")
    int getProviderFromCard(int cardId);
}
