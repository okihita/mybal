package com.mybal.id.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;

import com.mybal.id.R;
import com.mybal.id.home.balance.BalanceHomeFragment;

import java.util.List;

// In the database, the name is "sim_balance_detail".
@Entity(tableName = "sim_usage")
public class SimUsage {

    // TODO: 26/11/17 Split event type (inquiry, purchase) from pack type (balance pulsa, data, sms, call, main, bonus)
    // See Message.java for further descriptions

    // Transaction types done by the SIM card itself
    public static final int INQUIRY_BALANCE_PULSA = 1001;
    public static final int INQUIRY_BALANCE_DATA = 1002;
    public static final int INQUIRY_BALANCE_SMS = 1003;
    public static final int INQUIRY_BALANCE_CALL = 1004;

    public static final int PURCHASE_PULSA_PACK = 2001;
    public static final int PURCHASE_DATA_PACK = 2002;
    public static final int PURCHASE_SMS_PACK = 2003;
    public static final int PURCHASE_CALL_PACK = 2004;

    public static final int UNNECESSARY_MESSAGE = 9666;

    public static final int UNIT_UNKNOWN = 1000;

    public static final int UNIT_CURRENCY_IDR = 1001;
    public static final int UNIT_DATA_BYTE = 2001;
    public static final int UNIT_DATA_KILOBYTE = 2002;
    public static final int UNIT_DATA_MEGABYTE = 2003;
    public static final int UNIT_DATA_GIGABYTE = 2004;
    public static final int UNIT_MESSAGE_SMS_COUNT = 3002;
    public static final int UNIT_CALL_MINUTE = 4002;
    public static final String EXPIRY_DATE_UNKNOWN = "n/a";
    private static final int UNIT_CURRENCY_MYR = 1002;
    private static final int UNIT_MESSAGE_CHARACTERS = 3001;
    private static final int UNIT_CALL_SECOND = 4001;
    private static final int UNIT_CALL_HOUR = 4003;
    @PrimaryKey(autoGenerate = true)
    public long id;

    // To follow the device convention in BalanceCheck, set 0 (slot 1) by default
    public int slot = BalanceHomeFragment.FIRST_SLOT;

    // There's a possibibility that the regex will be deleted later,
    // so we must also store the pattern, capture groups, and labels.
    // (And the `additional_regex` column too.)
    public String originalMessage; // Raw message received by the device
    public int regexId; // Regex used to pattern this message

    @Ignore
    public List<LabelText> labelTexts;

    // As per 15 Nov 2017, there are 6 kinds of transactions:
    // See SimUsageItem for further information
    // 1. INQUIRY, as in checking the balance
    // 2. PURCHASE, as in the recharge information
    public int transactionType; // default to 0

    public String createdAt = ""; // ISO 8601 of the time this message is received

    // These three are convenience variables for INQUIRY and PURCHASE types
    public double amount; // default to 0.0
    public int unit = UNIT_UNKNOWN; // Tricky. Be careful about KB/MB/GB and IDR/MYR/currencies

    // ISO-8601 format of the expiry of the package, must be one point in the future
    // because the Magic.expiryCountdown() will convert it
    public String expiry = "";

    // A special field denoting if the Data, SMS, or Call isn't available
    public boolean noPack = false;

    /**
     * @param context For, you know, internationalization since .xml resources can only be
     *                accessed through Android contexts.
     */
    public String getUnitName(Context context) {

        switch (unit) {

            case UNIT_UNKNOWN:
                return "unit";

            case UNIT_CURRENCY_IDR:
                return "Rp";
            case UNIT_CURRENCY_MYR:
                return "MYR";

            case UNIT_DATA_BYTE:
                return "B";
            case UNIT_DATA_KILOBYTE:
                return "kB";
            case UNIT_DATA_MEGABYTE:
                return "MB";
            case UNIT_DATA_GIGABYTE:
                return "GB";

            case UNIT_MESSAGE_CHARACTERS:
                return "char";
            case UNIT_MESSAGE_SMS_COUNT:
                return "SMS";

            case UNIT_CALL_SECOND:
                return context.getString(R.string.simUsage_unit_seconds);
            case UNIT_CALL_MINUTE:
                return context.getString(R.string.simUsage_unit_minutes);
            case UNIT_CALL_HOUR:
                return context.getString(R.string.simUsage_unit_hours);

            default:
                return "?";
        }
    }

    @Entity(tableName = "sim_usage_label_text")
    public static class LabelText {

        public final long simUsageId;
        public final String label;
        public final String text;
        @PrimaryKey(autoGenerate = true)
        public long id;

        public LabelText(long simUsageId, String label, String text) {
            this.simUsageId = simUsageId;
            this.label = label;
            this.text = text;
        }
    }
}