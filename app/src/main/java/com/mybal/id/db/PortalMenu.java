package com.mybal.id.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "portal_menu")
public class PortalMenu {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("provider_id")
    @Expose
    public int providerId;
    @SerializedName("card_id")
    @Expose
    public int cardId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("ussd_code")
    @Expose
    public String ussdCode;
    @SerializedName("status")
    @Expose
    public String status;
}