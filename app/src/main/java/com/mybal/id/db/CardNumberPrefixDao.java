package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CardNumberPrefixDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCardNumberPrefix(List<CardNumberPrefix> cardNumberPrefixes);

    @Query("SELECT cards_id FROM card_number_prefix WHERE prefix_code = :prefix LIMIT 1")
    int getCardId(String prefix);
}
