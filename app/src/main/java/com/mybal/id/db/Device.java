package com.mybal.id.db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Device {

    @SerializedName("id")
    @Expose
    public long id;
    @SerializedName("imei")
    @Expose
    public long imei;
    @SerializedName("device_registered")
    @Expose
    public String deviceRegistered;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("action_logged")
    @Expose
    public String actionLogged;
    @SerializedName("token_auth")
    @Expose
    public String tokenAuth;
}
