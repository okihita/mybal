package com.mybal.id.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "topup_code")
class TopupCode {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Long = 0

    @SerializedName("opr_code")
    @Expose
    var oprCode: String? = null

    @SerializedName("providers_id")
    @Expose
    var providerId: Int = 0

    @SerializedName("amount")
    @Expose
    var amount: Int = 0

    @SerializedName("price")
    @Expose
    var price: Int = 0

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("merchant_id")
    @Expose
    var merchantId: Int = 0
}
