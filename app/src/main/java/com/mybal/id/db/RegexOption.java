package com.mybal.id.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// Don't complain. Most of these are leftovers from the ugly v1.0 app design.
@Entity(tableName = "regex_option")
public class RegexOption {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("ussd_functions_id")
    @Expose
    public int ussdFunctionId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("display_name")
    @Expose
    public String displayName;
}
