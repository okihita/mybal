package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UssdCodeDao {

    /**
     * Retrieve the single USSD sequence to check balance.
     *
     * @param providerId  Provider ID
     * @param balanceType What kind of balance is being called. See UssdCode class for further info.
     */
    @Query("SELECT ussd_code.* FROM ussd_code " +
            "JOIN card ON ussd_code.cards_id = card.id " +
            "JOIN provider ON card.providers_id = provider.id " +
            "WHERE provider.id = :providerId " +
            "AND ussd_functions_id = :balanceType " +
            "LIMIT 1")
    UssdCode getUssdCode(int providerId, int balanceType);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUssdCodes(List<UssdCode> ussdCodes);
}