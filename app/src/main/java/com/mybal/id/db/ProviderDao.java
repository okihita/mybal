package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ProviderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProvider(List<Provider> providers);

    @Query("SELECT * FROM provider WHERE mcc = :mcc AND mnc = :mnc LIMIT 1")
    Provider getProviderByMccMnc(int mcc, int mnc);

    @Query("SELECT * FROM provider WHERE id = :id LIMIT 1")
    Provider getProviderById(int id);
}
