package com.mybal.id.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "card_number_prefix")
public class CardNumberPrefix {

    @PrimaryKey
    @Expose
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "prefix_code")
    @Expose
    @SerializedName("prefix_code")
    public String prefixCode;

    @ColumnInfo(name = "cards_id")
    @Expose
    @SerializedName("cards_id")
    public int cardId;
}
