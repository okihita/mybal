package com.mybal.id.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MessageRegexDao {

    @Query("SELECT * FROM message_regex WHERE textProviderId = :providerId AND status LIKE 'live'")
    List<MessageRegex> getLivePatternsForProvider(int providerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMessageRegexes(List<MessageRegex> messageRegexes);

    @Query("SELECT * FROM message_regex WHERE id = :messageRegexId")
    MessageRegex getMessageRegex(int messageRegexId);
}
