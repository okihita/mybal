package com.mybal.id.network

import com.mybal.id.db.*
import com.mybal.id.home.topup.TopupRequestResponse
import com.mybal.id.home.topup.WalletBalance
import com.mybal.id.home.topup.WalletHistoryItem
import retrofit2.Call
import retrofit2.http.*

interface MyBalApi {


    /***** ACCOUNT *****/

    // A user is silently registered when the app first installs
    // See also Constants.PREF_KEY_HAS_BEEN_PROFILED
    @FormUrlEncoded
    @POST("devices")
    fun registerDeviceSilently(
            @Field("imei") imei: String,
            @Field("google_ads_id") googleAdsId: String,
            @Field("android_id") androidId: String, // https://stackoverflow.com/a/2785493
            @Field("first_sim_provider") firstProvider: Int,
            @Field("second_sim_provider") secondProvider: Int,
            @Field("sim_subscriber_id") subid: String,
            @Field("sim_serial") serial: String
    ): Call<Device>

    @FormUrlEncoded
    @POST("devices/login")
    fun loginRefreshTokenDevice(
            @Field("imei") imei: String
    ): Call<Device>

    @FormUrlEncoded
    @POST("users/register")
    fun register(
            @Field("imei") imei: String,
            @Field("phone_number") phoneNumber: String
    ): Call<User>

    @FormUrlEncoded
    @POST("users/login")
    fun loginRefreshTokenUser(
            @Field("imei") imei: String,
            @Field("phone_number") phoneNumber: String // Starts with "62"
    ): Call<User>

    @FormUrlEncoded
    @POST("users/token_auth_check")
    fun tokenAuthCheck(
            @Field("token_auth") token: String
    ): Call<TokenStatus>

    @FormUrlEncoded
    @GET("payment_accounts/get_user_wallet_balance")
    fun getWalletBalance(): Call<WalletBalance>

    @FormUrlEncoded
    @GET("account_histories/myhistory")
    fun getWalletHistory(): Call<List<WalletHistoryItem>>


    /***** OFFLINE DATABASE *****/

    @get:GET("providers")
    val providers: Call<List<Provider>>

    @get:GET("cards")
    val cards: Call<List<Card>>

    // USSD Code table is hardcoded

    @get:GET("ussd_codes")
    val ussdCodes: Call<List<UssdCode>>

    @get:GET("provider_senders")
    val providerSenders: Call<List<ProviderSender>>

    @get:GET("message_regexes")
    val messageRegexes: Call<List<MessageRegex>>

    @get:GET("regex_options")
    val regexOptions: Call<List<RegexOption>>

    @get:GET("portal_menus")
    val portalMenus: Call<List<PortalMenu>>

    // Pack Type is hardcoded

    @get:GET("prefix_codes")
    val cardNumberPrefixes: Call<List<CardNumberPrefix>>

    @get:GET("topup_codes")
    val topupCodes: Call<List<TopupCode>>


    /***** MESSAGE PATTERNING *****/

    @POST("messages/gather_new_message")
    fun postMessage(@Body message: Message): Call<Message>

    @FormUrlEncoded
    @POST("sim_balance_details")
    fun postSimBalanceDetail(
            @Field("amount") amount: Int,
            @Field("unit") unit: Int,
            @Field("expiry") expiry: String,
            @Field("regex_id") regexId: Int,
            @Field("sim_slot") simSlot: Int,
            @Field("raw_message") rawMessage: String,
            @Field("imei") imei: String
    ): Call<SimUsage>

    @FormUrlEncoded
    @POST("message_reporteds")
    fun postReportMessage(
            @Field("message_id") messageId: Int,
            @Field("message_regexes_id") messageRegexId: Int,
            @Field("imei") imei: String,
            @Field("note") note: String
    ): Call<Void>

    @FormUrlEncoded
    @POST("app_rates")
    fun postAppRating(
            @Field("imei") imei: String,
            @Field("rating") rating: Int,
            @Field("content") content: String
    ): Call<Void>

    @FormUrlEncoded
    @POST("complaint_and_helps")
    fun postComplaintSuggestions(
            @Field("imei") imei: String,
            @Field("content") content: String
    ): Call<Void>


    /***** TOPUP AND RECHARGE *****/
    @GET("topup_codes?search=providers_id:{providerId}")
    fun topupCodes(@Path("providerId") providerId: Int): Call<List<Provider>>

    @FormUrlEncoded
    @POST("topup/execute")
    fun requestTopup(
            @Field("phone_number") phoneNumber: String, // Starts with 081xxx
            @Field("topup_code") topupCode: String,
            @Field("payment_type") paymentType: String,
            @Field("bank_code") bankCode: String
    ): Call<TopupRequestResponse>
}
