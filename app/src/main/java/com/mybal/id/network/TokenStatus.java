package com.mybal.id.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenStatus {
    @SerializedName("token_status")
    @Expose
    public String tokenStatus;
    @SerializedName("token_exp_time")
    @Expose
    public String tokenExpTime;
}
