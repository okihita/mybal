package com.mybal.id.network;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.mybal.id.BuildConfig;
import com.mybal.id.util.Constants;
import com.mybal.id.util.SessionManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {


    public static MyBalApi getClient(Context context) {

        String authToken = SessionManager.Companion.getAuthToken(context);

        Interceptor interceptor;

        interceptor = chain -> {

            Request.Builder requestBuilder = chain.request().newBuilder()
                    .header("x-api-key", "99jkahjkfdjkd^&$@*#_=)hjk&5234ghjfd763Khjsfd()*(^&2%u83hjsfd123");

            if (!authToken.equals(""))
                requestBuilder.header("Authorization", authToken);

            return chain.proceed(requestBuilder.build());
        };

        // https://futurestud.io/tutorials/retrofit-add-custom-request-header
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        if (BuildConfig.BUILD_TYPE.equals("debug"))
            clientBuilder.addNetworkInterceptor(new StethoInterceptor());

        OkHttpClient client = clientBuilder.addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.DEVELOPMENT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(MyBalApi.class);
    }
}
