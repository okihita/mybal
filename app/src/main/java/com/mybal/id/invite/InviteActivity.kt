package com.mybal.id.invite

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.mybal.id.R
import kotlinx.android.synthetic.main.activity_invite.*
import kotlinx.android.synthetic.main.widget_toolbar.*

class InviteActivity : AppCompatActivity() {

    private val shareText = "Check out this cool app! https://play.google.com/store/apps/details?id=com.mybal.id"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite)

        setupToolbar()

        twitterCV.setOnClickListener({
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/intent/tweet?text=" + shareText))
            startActivity(browserIntent)
        })

        facebookCV.setOnClickListener({
            val content = ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.mybal.id"))
                    .build()
            ShareDialog.show(this, content)
        })

        lineCV.setOnClickListener({
            val lineIntent = Intent()
            lineIntent.action = Intent.ACTION_VIEW
            lineIntent.data = Uri.parse("line://msg/text/" + shareText)
            try {
                startActivity(lineIntent)
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "LINE has not been installed.", Toast.LENGTH_SHORT).show()
            }
        })

        whatsAppCV.setOnClickListener({
            val whatsappIntent = Intent(Intent.ACTION_SEND)
            whatsappIntent.type = "text/plain"
            whatsappIntent.`package` = "com.whatsapp"
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, shareText)
            try {
                startActivity(whatsappIntent)
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show()
            }
        })

        smsCV.setOnClickListener({
            val smsIntent = Intent(android.content.Intent.ACTION_VIEW)
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra("sms_body", shareText)
            startActivity(smsIntent)
        })
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Invite"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
