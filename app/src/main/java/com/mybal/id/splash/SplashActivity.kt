package com.mybal.id.splash

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.facebook.accountkit.AccountKit
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import com.mybal.id.BuildConfig
import com.mybal.id.R
import com.mybal.id.home.HomeActivity
import com.mybal.id.tour.TourActivity
import com.mybal.id.util.*
import com.scottyab.rootbeer.RootBeer
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*

class SplashActivity : AppCompatActivity() {

    // Flags whether to skip the Splash Screen and/or Tour Screen
    private var hasAgreedToTos = false
    private var hasSeenTour = false

    // Dexter's library variables
    private var allPermissionsListener: MultiplePermissionsListener? = null
    private var errorListener: PermissionRequestErrorListener? = null

    // Permission
    private var isReadPhoneStatePermitted = false
    private var isCallPermitted = false
    private var isReadSmsPermitted = false
    private var isReadOutgoingCallPermitted = false

    private var permissionDeniedDialog: AlertDialog? = null

    companion object {
        private val AGREED_TO_TOS = "com.mybal.id.pref_key_agreed_to_tos"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        hasAgreedToTos = getSharedPreferences(Constants.PREF_KEY_AGREED_TOS, Context.MODE_PRIVATE).getBoolean(AGREED_TO_TOS, false)
        hasSeenTour = getSharedPreferences(Constants.PREF_KEY_TOUR_SKIP, Context.MODE_PRIVATE).getBoolean(TourActivity.HAS_SEEN_TOUR, false)

        createPermissionListeners()
        setupLanguageSelection()
        setupTermsOfService()
    }

    override fun onStart() {
        super.onStart()

        (application as MyBalApp).updateOfflineDb() // Updates the offline database
        blockIfDeviceRooted() // User can't go further it the device is rooted

        // Display ToS and language selection screen
        if (hasAgreedToTos) checkIfAllPermitted()
        else nextButton.setOnClickListener({
            if (!ToSCheckBox.isChecked) {
                Toast.makeText(this, getString(R.string.splash_mustAgree), Toast.LENGTH_LONG).show()
            } else {
                getSharedPreferences(Constants.PREF_KEY_AGREED_TOS, Context.MODE_PRIVATE).edit().putBoolean(AGREED_TO_TOS, true).apply()
                checkIfAllPermitted()
            }
        })
    }

    private fun blockIfDeviceRooted() {
        val rootBeer = RootBeer(this)
        if (rootBeer.isRootedWithoutBusyBoxCheck) {
            AlertDialog.Builder(this)
                    .setTitle("Rooted Device")
                    .setMessage("Your device is rooted, sorry.")
                    .setCancelable(false)
                    .show()
        }
    }

    private fun setupLanguageSelection() {

        val deviceLocaleLang = Locale.getDefault().language
        val languages = listOf(
                "English", // index 0
                "Bahasa Indonesia" // index 1
        )

        val languagesAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
        languagesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner.adapter = languagesAdapter
        spinner.setSelection(if (deviceLocaleLang == Constants.LOCALE_CODE_ENGLISH) 0 else 1)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {

                // If the language is not already English, but the "English" option is selected,
                // change the device locale to English
                    0 -> if (deviceLocaleLang != Constants.LOCALE_CODE_ENGLISH)
                        Magic.changeLanguage(this@SplashActivity, Constants.LOCALE_CODE_ENGLISH)

                    1 -> if (deviceLocaleLang != Constants.LOCALE_CODE_INDONESIA)
                        Magic.changeLanguage(this@SplashActivity, Constants.LOCALE_CODE_INDONESIA)

                    else -> {
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun setupTermsOfService() {

        val agreeWith = getString(R.string.splash_i_agree_with_the) + " "
        val tos = getString(R.string.splash_tos)
        val and = " " + getString(R.string.splash_and) + " "
        val privacyPolicy = getString(R.string.splash_pp)

        val spannable = SpannableStringBuilder(agreeWith)

        // First clickable link: Terms of Services
        spannable.append(tos)
        spannable.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                val intent = Intent(this@SplashActivity, ToSActivity::class.java)
                intent.putExtra(ToSActivity.TOS_INTENT, ToSActivity.TYPE_TERMS_OF_SERVICES)
                startActivity(intent)
            }
        }, spannable.length - tos.length, spannable.length, 0)

        spannable.append(and)

        // Second clickable link: Privacy Policy
        spannable.append(privacyPolicy)
        spannable.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                val intent = Intent(this@SplashActivity, ToSActivity::class.java)
                intent.putExtra(ToSActivity.TOS_INTENT, ToSActivity.TYPE_PRIVACY_POLICY)
                startActivity(intent)
            }
        }, spannable.length - privacyPolicy.length, spannable.length, 0)
        tosPpTV.movementMethod = LinkMovementMethod.getInstance()
        tosPpTV.setText(spannable, TextView.BufferType.SPANNABLE)
    }

    private fun createPermissionListeners() {
        val listener = MyBalMultiplePermissionListener(this)

        allPermissionsListener = CompositeMultiplePermissionsListener(listener,
                SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(findViewById(android.R.id.content),
                        R.string.permission_all_denied_feedback)
                        .withDuration(30000) // 30 seconds snackbar
                        .withOpenSettingsButton("Settings")
                        .build())

        errorListener = MyBalErrorListener()
    }

    private fun checkIfAllPermitted() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.PROCESS_OUTGOING_CALLS
                )
                .withListener(allPermissionsListener)
                .withErrorListener(errorListener)
                .check()
    }

    fun afterPermissionGranted(permission: String) {
        when (permission) {
            Manifest.permission.READ_PHONE_STATE -> isReadPhoneStatePermitted = true
            Manifest.permission.CALL_PHONE -> isCallPermitted = true
            Manifest.permission.READ_SMS -> isReadSmsPermitted = true
            Manifest.permission.PROCESS_OUTGOING_CALLS -> isReadOutgoingCallPermitted = true
            else -> {
            }
        }

        if (isReadPhoneStatePermitted
                && isCallPermitted
                && isReadSmsPermitted
                && isReadOutgoingCallPermitted) {

            logUserIn()
        }
    }

    fun afterPermissionDenied(permission: String, isPermanentlyDenied: Boolean) {

        // If a dialog already shows or one is already created, don't show anything
        if ((permissionDeniedDialog != null && permissionDeniedDialog?.isShowing!!)
                || permissionDeniedDialog != null) {

        } else {

            val permissionName = when (permission) {
                Manifest.permission.READ_PHONE_STATE -> "read phone state"
                Manifest.permission.CALL_PHONE -> "phone call"
                Manifest.permission.READ_SMS -> "SMS reading"
                Manifest.permission.PROCESS_OUTGOING_CALLS -> "outgoing calls"
                else -> ""
            }

            permissionDeniedDialog = AlertDialog.Builder(this)
                    .setTitle("Permission Denied")
                    .setMessage("You can't use this app since you denied the " + permissionName + " permission" +
                            (if (isPermanentlyDenied) " permanently" else "") + ". "
                            + "Go to the Permission Settings to permit this app.")
                    .setPositiveButton("Go to Permission Settings") { _, _ ->
                        startActivity(Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + BuildConfig.APPLICATION_ID)))
                    }
                    .show()
        }
    }

    private fun logUserIn() {

        val accessToken = AccountKit.getCurrentAccessToken()

        // If user already has a session, go to Home
        if (accessToken != null) {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }

        // If the user already has seen the tour before, go to Home
        else {
            if (hasSeenTour) {
                startActivity(Intent(this, HomeActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, TourActivity::class.java))
                finish()
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun showPermissionRationale() {
    }
}
