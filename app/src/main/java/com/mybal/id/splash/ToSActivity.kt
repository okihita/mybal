package com.mybal.id.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mybal.id.R
import kotlinx.android.synthetic.main.activity_tos.*

class ToSActivity : AppCompatActivity() {

    companion object {
        val TOS_INTENT = "com.mybal.id.tos_intent"
        val TYPE_TERMS_OF_SERVICES = 1
        val TYPE_PRIVACY_POLICY = 2
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tos)

        val type = intent.getIntExtra(TOS_INTENT, 0)
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(
                if (type == TYPE_PRIVACY_POLICY) "http://mybal.asia/privacy-policy/"
                else "http://mybal.asia/terms-conditions/"
        )
    }
}
