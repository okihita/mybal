package com.mybal.id.accessibility

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.WindowManager.LayoutParams

import com.mybal.id.R
import com.mybal.id.util.RxBus

import io.reactivex.Observer
import io.reactivex.disposables.Disposable

/**
 * Called after {@link AccessibilityGuideActivity} to show another floating guide screen.
 */
class FindMyBalService : Service() {

    private var windowManager: WindowManager? = null
    private var floatingView: View? = null

    private val stopFloatingObserver = object : Observer<RxBus.Payload> {
        override fun onSubscribe(d: Disposable) {}

        override fun onNext(payload: RxBus.Payload) {
            when (payload.requestCode) {
            // If there's a command to stop, finish the view
                RxBus.FLAG_CLOSE_ACCESSIBILITY_FLOATING_GUIDE ->
                    if (payload.boolPayload)
                        onDestroy()
                else -> {
                }
            }
        }

        override fun onError(e: Throwable) {}

        override fun onComplete() {}
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @SuppressLint("InflateParams")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        RxBus.getInstance().observables.subscribe(stopFloatingObserver)

        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        floatingView = inflater.inflate(R.layout.widget_find_mybal, null)

        // Add the view to the window.
        // https://stackoverflow.com/a/36101111
        @Suppress("DEPRECATION")
        val params = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.TYPE_TOAST,
                LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)

        // Specify the view position
        params.gravity = Gravity.CENTER

        // Add the view to the window
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            if (windowManager != null && intent != null)
                Handler().postDelayed({ windowManager!!.addView(floatingView, params) }, 1000)
        }

        // For Oreo and above...
        else {
            // ..do nothing
        }

        // Still remove the view if it's more than 30 seconds
        Handler().postDelayed({ if (intent != null) onDestroy() }, 30000)

        return super.onStartCommand(intent, flags, startId)
    }

    private fun removeFloatingView() {
        if (floatingView != null && floatingView!!.isAttachedToWindow)
            windowManager!!.removeView(floatingView)
    }

    override fun onDestroy() {
        super.onDestroy()
        removeFloatingView()
    }
}