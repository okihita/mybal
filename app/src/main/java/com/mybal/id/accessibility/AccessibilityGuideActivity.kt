package com.mybal.id.accessibility

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import com.mybal.id.R
import com.mybal.id.util.Constants
import java.util.*

/**
 * A dimmed-screen guide that shows when user is prompted to turn on Accessibility Access for MyBal.
 */
class AccessibilityGuideActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.widget_accessibility_guide)

        val accessibilityGuideImgRes =
                if (Locale.getDefault().language == Constants.LOCALE_CODE_INDONESIA)
                    R.drawable.img_accessibility_indonesian
                else
                    R.drawable.img_accessibility_english

        (findViewById<View>(R.id.accessibilityGuideIV) as ImageView)
                .setImageResource(accessibilityGuideImgRes)

        findViewById<View>(R.id.accessibilityGuideIV).setOnClickListener({
            applicationContext.startService(Intent(applicationContext, FindMyBalService::class.java))
            finish()
        })
    }
}
