package com.mybal.id.accessibility

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.AccessibilityServiceInfo
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Toast
import com.mybal.id.BuildConfig
import com.mybal.id.db.Message
import com.mybal.id.home.HomeActivity
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.network.RestClient
import com.mybal.id.util.Constants
import com.mybal.id.util.RxBus
import io.reactivex.Observer
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

// The class to read incoming USSD messages and SMS
// https://developer.android.com/training/accessibility/service.html
class MyBalAccessibilityService : AccessibilityService() {

    // A flag to indicate whether it's MyBal app who initiated the USSD call.
    // This avoids listening to unnecessary USSD calls from other apps or user's manual action.
    private var isRequestedFromApp = false

    // Variables when the USSD itself is called from outside the app
    private var outsideAppUssdSequence: String? = null

    private var outsideAppCallerSimSlot: Int = 0

    companion object {

        private val CLASS_NAME_APP_ALERT_DIALOG = "android.app.AlertDialog"

        // https://stackoverflow.com/a/40568194
        fun isServiceEnabled(context: Context): Boolean {

            val expectedComponentName = ComponentName(context, MyBalAccessibilityService::class.java.canonicalName)

            val enabledServicesSetting = Settings.Secure.getString(context.contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES) ?: return false

            val colonSplitter = TextUtils.SimpleStringSplitter(':')

            colonSplitter.setString(enabledServicesSetting)

            while (colonSplitter.hasNext()) {
                val componentNameString = colonSplitter.next()
                val enabledService = ComponentName.unflattenFromString(componentNameString)
                if (enabledService != null && enabledService == expectedComponentName)
                    return true
            }

            return false
        }
    }

    // To avoid reading irrelevant USSD popups called from non-MyBal apps (and accidentally closing
    // them!) we need to setup a one-time listener that observes whether any USSD popup is being
    // requested.
    private val readUssdRequestObserver = object : Observer<RxBus.Payload> {
        override fun onSubscribe(@NonNull d: Disposable) {}

        override fun onNext(@NonNull payload: RxBus.Payload) {

            when (payload.requestCode) {

                RxBus.FLAG_REQUEST_BALANCE_START_LISTENING ->
                    isRequestedFromApp = payload.boolPayload


                RxBus.FLAG_OUTGOING_USSD_CALL_SIM_SLOT ->
                    if (!isRequestedFromApp)
                        outsideAppCallerSimSlot = payload.intPayload

                RxBus.FLAG_OUTGOING_USSD_CALL_NUMBER ->
                    if (!isRequestedFromApp) {
                        outsideAppUssdSequence = payload.stringPayload
                    }

                else -> {
                }
            }
        }

        override fun onError(@NonNull e: Throwable) {}

        override fun onComplete() {}
    }

    // This is called after user grants permission in Settings > Accessibility
    // 18 DEC: Problem in Oppo: this method is not called. Probably Android SDK bug.
    override fun onServiceConnected() {

        super.onServiceConnected()

        RxBus.getInstance().observables.subscribe(readUssdRequestObserver)

        // Set what kind device events we want to read
        val info = AccessibilityServiceInfo()
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED or
                AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED or
                AccessibilityEvent.TYPE_VIEW_CLICKED // Fire when a new window shows
        info.packageNames = arrayOf("com.android.phone") // Get all packages
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC
        serviceInfo = info

        // Close the floating view
        RxBus.getInstance()
                .publish(RxBus.Payload(RxBus.FLAG_CLOSE_ACCESSIBILITY_FLOATING_GUIDE, true))

        gotoHomeScreen()
    }

    /**
     * When the phone restarts, the onServiceConnected is called again. This may result in the
     * Home Screen shows up every device restart. By implementing a special Shared Preference,
     * we will avoid that.
     */
    private fun gotoHomeScreen() {
        val isFirstConnect = getSharedPreferences(Constants.PREF_KEY_IS_FIRST_ACCESSIBILITY_CONNECT, Context.MODE_PRIVATE)
                .getBoolean(Constants.PREF_KEY_IS_FIRST_ACCESSIBILITY_CONNECT, true)

        if (isFirstConnect) {

            // Close the Settings screen, go to Home, change the flag
            val homeIntent = Intent(this, HomeActivity::class.java)
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(homeIntent)

            getSharedPreferences(Constants.PREF_KEY_IS_FIRST_ACCESSIBILITY_CONNECT, Context.MODE_PRIVATE)
                    .edit()
                    .putBoolean(Constants.PREF_KEY_IS_FIRST_ACCESSIBILITY_CONNECT, false)
                    .apply()
        }
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {

        val eventType = event.eventType

        when (eventType) {

        // If there's a new pop-up window...
            AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED -> {

                if (event.className.toString() == CLASS_NAME_APP_ALERT_DIALOG) {

                    val ussdMessageContent: String = extractTextContent(event.text)

                    if (isRequestedFromApp) {

                        // ..read the raw USSD message and send it to the
                        // BalanceHomeFragment to be processed.
                        RxBus.getInstance().publish(RxBus.Payload(
                                RxBus.FLAG_REQUEST_BALANCE_INCOMING_USSD,
                                ussdMessageContent))

                        // Change the flag
                        isRequestedFromApp = false

                        dismissPopup(event)

                    } else {

                        // If it's not called from inside the app, keep sending the Message,
                        // but don't delegate it to the BalanceHomeFragment class

                        val message = Message()
                        message.textUserId = Message.DEFAULT_UNKNOWN_USER_ID
                        message.textRecieveType = Message.RECEIVE_TYPE_USSD
                        message.textContent = ussdMessageContent
                        message.textSender = Message.USSD_SENDER_ID
                        message.textUssd = outsideAppUssdSequence

                        val providerPref = getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE)
                        val providerIdSlot1 = providerPref.getInt(Constants.PROVIDER_ID_SLOT_1, 0)
                        val providerIdSlot2 = providerPref.getInt(Constants.PROVIDER_ID_SLOT_2, 0)
                        message.textProviderId = if (outsideAppCallerSimSlot == BalanceHomeFragment.FIRST_SLOT) providerIdSlot1 else providerIdSlot2

                        RestClient.getClient(this).postMessage(message).enqueue(object : Callback<Message> {
                            override fun onResponse(call: Call<Message>, response: Response<Message>) {
                                Log.d("###", "onResponse: " + response.toString())
                            }

                            override fun onFailure(call: Call<Message>, t: Throwable) {
                                Log.d("###", "onFailure: " + t.message)
                            }
                        })

                        // Reset the information
                        outsideAppCallerSimSlot = 0
                        outsideAppUssdSequence = ""
                    }
                }
            }

            else -> {
            }
        }
    }

    // Some device manufacturers add extra "title" in the USSD Alert Dialog
    // If that happens, get the dialogTextGroup[1] instead of dialogTextGroup[0]
    private fun extractTextContent(dialogTextGroup: List<CharSequence>): String {

        val deviceModel = android.os.Build.MODEL
        val deviceManufacturer = android.os.Build.MANUFACTURER

        if (BuildConfig.BUILD_TYPE == "teamRelease") {
            Toast.makeText(this,
                    "extractTextContent: model: $deviceModel, brand: $deviceManufacturer",
                    Toast.LENGTH_SHORT).show()
        }

        return dialogTextGroup[0].toString()
    }

    // Dismiss the popup by programmatically pressing Back
    private fun dismissPopup(event: AccessibilityEvent) {

        // Plan A: Try to press hardware Back button
        performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)

        // Plan B: call the system to close the dialog
        val closeDialog = Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)
        sendBroadcast(closeDialog)

        // Plan C: Attempt to close it by pressing its Buttons
        val eventNodeInfo = event.source
        val clickableNodeInfos = ArrayList<AccessibilityNodeInfo>()

        val possibleCancelTexts = listOf(
                "BATALKAN",
                "TUTUP",
                "KELUAR",
                "BATAL",
                "CANCEL",
                "CLOSE",
                "GOT IT" // From Oppo
        )

        possibleCancelTexts.forEach {
            if (eventNodeInfo != null) // Find the "close" button in the popup
                clickableNodeInfos.addAll(eventNodeInfo.findAccessibilityNodeInfosByText(it))
        }

        clickableNodeInfos.forEach { it.performAction(AccessibilityNodeInfo.ACTION_CLICK) }
    }

    override fun onInterrupt() {
    }
}