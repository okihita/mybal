package com.mybal.id.accessibility

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.mybal.id.util.Constants
import com.mybal.id.util.RxBus

/**
 * A class that listens when the user makes a call.
 */
class DialBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != null)
            if (intent.action == Intent.ACTION_NEW_OUTGOING_CALL) {
                val number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER)

                if (number.startsWith("*") && number.endsWith("#")) {

                    RxBus.getInstance().publish(RxBus.Payload(
                            RxBus.FLAG_OUTGOING_USSD_CALL_NUMBER,
                            number
                    ))

                    var whichSIM = 0 // Security fallback to SIM 1
                    if (intent.extras != null)
                        Constants.simSlotNames
                                .filter { intent.extras!!.containsKey(it) }
                                .forEach { whichSIM = intent.extras!!.getInt(it) }

                    RxBus.getInstance().publish(RxBus.Payload(
                            RxBus.FLAG_OUTGOING_USSD_CALL_SIM_SLOT,
                            whichSIM
                    ))
                }
            }
    }
}
