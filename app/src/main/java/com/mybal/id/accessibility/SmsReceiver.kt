package com.mybal.id.accessibility

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.util.Log
import com.mybal.id.db.Message
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.db.Provider
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.network.RestClient
import com.mybal.id.util.Constants
import com.mybal.id.util.Magic
import com.mybal.id.util.RxBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("DEPRECATION")
class SmsReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        val extras = intent.extras

        if (extras != null) {

            // Fetch the extra informations from the incoming SMS
            val smsextras = extras.get("pdus") as Array<*>

            smsextras.forEach {

                val smsMessage = SmsMessage.createFromPdu(it as ByteArray)

                // Try to find which SIM slot received the SMS
                var whichSIM = 0
                Constants.simSlotNames
                        .filter { extras.containsKey(it) }
                        .forEach { whichSIM = extras.getInt(it) }

                val firstProviderId = context.getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE).getInt(Constants.PROVIDER_ID_SLOT_1, 0)
                val secondProviderId = context.getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE).getInt(Constants.PROVIDER_ID_SLOT_2, 0)
                val providerId = Provider(if (whichSIM == BalanceHomeFragment.FIRST_SLOT) firstProviderId else secondProviderId).id

                val db = MyBalDatabase.getInstance(context)
                val officialProviderSenders = db.providerSenderDao().getSendersByProvider(providerId)

                officialProviderSenders.forEach {

                    val senderId = it.codeNumber

                    if (senderId == smsMessage.originatingAddress ||
                            senderId == smsMessage.displayOriginatingAddress) {

                        val smsBody = smsMessage.messageBody

                        // Save the SMS received as a SIM Usage, if it meets the condition
                        val messageRegexes = db.messageRegexDao().getLivePatternsForProvider(providerId)
                        Magic.saveIntoSimUsage(context, whichSIM, smsBody, Magic.parseMessagePattern(smsBody, messageRegexes))
                        RxBus.getInstance().publish(RxBus.Payload(RxBus.FLAG_REQUEST_BALANCE_INCOMING_SMS, true))

                        // Send it to server
                        sendMessageToServer(context, senderId, smsBody, providerId)
                    }
                }
            }
        }
    }

    private fun sendMessageToServer(context: Context, senderId: String, messageContent: String, providerId: Int) {

        val message = Message()
        message.textUserId = 0
        message.textRecieveType = Message.RECEIVE_TYPE_SMS
        message.textContent = messageContent
        message.textSender = senderId
        message.textUssd = "-" // According to agreement w/ backend programmer
        message.textProviderId = providerId

        RestClient.getClient(context).postMessage(message).enqueue(object : Callback<Message> {
            override fun onResponse(call: Call<Message>, response: Response<Message>) {
                Log.d("###", "onResponse: " + response.toString())
            }

            override fun onFailure(call: Call<Message>, t: Throwable) {
                Log.d("###", "onFailure: " + t.message)
            }
        })
    }
}
