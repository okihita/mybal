package com.mybal.id.tour

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mybal.id.R

import kotlinx.android.synthetic.main.fragment_tour.*

class TourFragment : Fragment() {

    companion object {

        private val ILLUSTRATION_DRAWABLE_ID = "com.mybal.id.tour_illustration_image"
        private val HEADER_TEXT = "com.mybal.id.tour_header_text"
        private val DESCRIPTION_TEXT = "com.mybal.id.tour_description_text"

        fun newInstance(drawableId: Int, headerText: String, descriptionText: String): TourFragment {

            val args = Bundle()
            args.putInt(ILLUSTRATION_DRAWABLE_ID, drawableId)
            args.putString(HEADER_TEXT, headerText)
            args.putString(DESCRIPTION_TEXT, descriptionText)

            val fragment = TourFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tour, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments
        illustrationIV.setImageDrawable(ContextCompat.getDrawable(activity!!, args!!.getInt(ILLUSTRATION_DRAWABLE_ID)))
        headerTV.text = args.getString(HEADER_TEXT)
        descriptionTV.text = args.getString(DESCRIPTION_TEXT)
    }
}
