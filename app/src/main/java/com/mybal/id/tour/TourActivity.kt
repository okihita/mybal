package com.mybal.id.tour

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat

import com.github.paolorotolo.appintro.AppIntro
import com.mybal.id.R
import com.mybal.id.home.HomeActivity
import com.mybal.id.util.Constants

// https://android-arsenal.com/details/1/1939
class TourActivity : AppIntro() {

    companion object {
        val HAS_SEEN_TOUR = "com.mybal.id.pref_key_has_seen_tour"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        addSlide(TourFragment.newInstance(R.drawable.img_tour_balance_check, getString(R.string.tour_header_balance_check), getString(R.string.tour_description_balance_check)))
        addSlide(TourFragment.newInstance(R.drawable.img_tour_dual_sim, getString(R.string.tour_header_dual_sim), getString(R.string.tour_description_dual_sim)))
        addSlide(TourFragment.newInstance(R.drawable.img_tour_recharge, getString(R.string.tour_header_recharge), getString(R.string.tour_description_recharge)))
    }

    private fun finishTour() {
        getSharedPreferences(Constants.PREF_KEY_TOUR_SKIP, Context.MODE_PRIVATE).edit()
                .putBoolean(HAS_SEEN_TOUR, true).apply()
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        finishTour()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        finishTour()
    }
}
