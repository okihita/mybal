package com.mybal.id.settings

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Toast

import com.mybal.id.R
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.util.Constants
import kotlinx.android.synthetic.main.widget_change_phone_number_dialog.*

class ChangePhoneNumberFragment : DialogFragment() {

    private var phoneNumberPref: SharedPreferences? = null
    private var simSlot: Int = 0

    companion object {

        fun newInstance(simSlot: Int): ChangePhoneNumberFragment {

            val args = Bundle()
            args.putInt(SimSettingsFragment.KEY_SETTINGS_SIM_SLOT, simSlot) // 0 or 1

            val fragment = ChangePhoneNumberFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        phoneNumberPref = activity!!.getSharedPreferences(SimSettingsFragment.KEY_PREF_PHONE_NUMBER, Context.MODE_PRIVATE)
        simSlot = arguments!!.getInt(SimSettingsFragment.KEY_SETTINGS_SIM_SLOT)

        val dialogView = View.inflate(context, R.layout.widget_change_phone_number_dialog, null)
        return AlertDialog.Builder(activity).setView(dialogView).create()
    }

    override fun onStart() {
        super.onStart()

        val phoneNumber = phoneNumberPref!!.getString(
                if (simSlot == BalanceHomeFragment.FIRST_SLOT) Constants.PHONE_NUMBER_SLOT_1
                else Constants.PHONE_NUMBER_SLOT_2, ""
        )
        dialog.numberET.setText(phoneNumber)

        dialog.closeButton.setOnClickListener { dialog.dismiss() }
        dialog.okButton.setOnClickListener {
            val newUssd = dialog.numberET.text.toString()
            if (newUssd == "") {
                updateCustomPhoneNumber("")
                Toast.makeText(activity, "Phone number removed", Toast.LENGTH_SHORT).show()
            } else {
                updateCustomPhoneNumber(newUssd)
                Toast.makeText(activity, "Phone number updated", Toast.LENGTH_SHORT).show()
            }
        }

    }

    /**
     * Updates this slot's custom phone number by storing it on the SharedPreference
     *
     * @param newPhoneNumber As it says
     */
    private fun updateCustomPhoneNumber(newPhoneNumber: String) {
        phoneNumberPref!!.edit().putString(
                if (simSlot == BalanceHomeFragment.FIRST_SLOT)
                    Constants.PHONE_NUMBER_SLOT_1
                else
                    Constants.PHONE_NUMBER_SLOT_2,
                newPhoneNumber).apply()
        parentFragment!!.onResume() // Refresh the SimSettingsFragment
        dismiss()
    }
}
