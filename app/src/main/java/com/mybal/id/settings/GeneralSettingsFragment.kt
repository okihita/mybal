package com.mybal.id.settings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.mybal.id.R
import com.mybal.id.util.MyBalApp

import kotlinx.android.synthetic.main.fragment_general_settings.*

class GeneralSettingsFragment : Fragment() {

    companion object {
        fun newInstance(): GeneralSettingsFragment {
            return GeneralSettingsFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_general_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appEngineLL.setOnClickListener({
            (activity!!.application as MyBalApp).updateOfflineDb()
            Handler().postDelayed({ Toast.makeText(activity, "Message engine refreshed", Toast.LENGTH_SHORT).show() },
                    2000)
        })

        appVersionLL.setOnClickListener({
            val appPackageName = "com.mybal.id"
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
            } catch (e: Exception) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)))
            }
        })
    }
}
