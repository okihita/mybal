package com.mybal.id.settings

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Toast
import com.mybal.id.R
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.util.Constants
import kotlinx.android.synthetic.main.widget_change_xlaxis_dialog.*

class ChangeXlAxisFragment : DialogFragment() {

    private var xlAxisPref: SharedPreferences? = null
    private var simSlot: Int = 0

    companion object {

        private val PROVIDER_XL = 3
        private val PROVIDER_AXIS = 4

        fun newInstance(simSlot: Int): ChangeXlAxisFragment {

            val args = Bundle()
            // 0 (first slot) or 1 (second slot)
            args.putInt(SimSettingsFragment.KEY_SETTINGS_SIM_SLOT, simSlot)

            val fragment = ChangeXlAxisFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogView = View.inflate(context, R.layout.widget_change_xlaxis_dialog, null)
        return AlertDialog.Builder(activity).setView(dialogView).create()
    }

    override fun onStart() {
        super.onStart()

        xlAxisPref = dialog.ownerActivity.getSharedPreferences(SimSettingsFragment.KEY_PREF_XL_AXIS, Context.MODE_PRIVATE)
        simSlot = arguments?.getInt(SimSettingsFragment.KEY_SETTINGS_SIM_SLOT) ?: 0

        val xlOrAxis = xlAxisPref?.getInt(
                if (simSlot == BalanceHomeFragment.FIRST_SLOT) Constants.XL_AXIS_SLOT_1
                else Constants.XL_AXIS_SLOT_2, PROVIDER_XL) ?: PROVIDER_XL

        // By default, 0 means XL
        if (xlOrAxis == PROVIDER_XL) dialog.xlRB.isChecked = true
        else dialog.axisRB.isChecked = true

        dialog.closeButton.setOnClickListener { dialog.dismiss() }
        dialog.okButton.setOnClickListener {
            if (dialog.xlRB.isChecked) {
                updateSimType(PROVIDER_XL)
                Toast.makeText(activity, "Provider changed to XL", Toast.LENGTH_SHORT).show()
            } else if (dialog.axisRB.isChecked) {
                updateSimType(PROVIDER_AXIS)
                Toast.makeText(activity, "Provider changed to Axis", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun updateSimType(xlOrAxis: Int) {
        xlAxisPref!!.edit().putInt(
                if (simSlot == BalanceHomeFragment.FIRST_SLOT)
                    Constants.XL_AXIS_SLOT_1 else Constants.XL_AXIS_SLOT_2,
                xlOrAxis).apply()
        parentFragment!!.onResume()
        dismiss()
    }
}
