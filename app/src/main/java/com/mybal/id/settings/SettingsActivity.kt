package com.mybal.id.settings

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem

import com.mybal.id.R
import com.mybal.id.home.balance.BalanceHomeFragment
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.widget_toolbar.*

class SettingsActivity : AppCompatActivity() {

    companion object {
        const val KEY_ARG_LANDING = "com.mybal.id.settings_landing_fragment"
        const val TAB_SETTINGS_GENERAL = 0
        const val TAB_SETTINGS_SIM_ONE = 1
        const val TAB_SETTINGS_SIM_TWO = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        setupToolbar()
        setupTabs()

        if (intent.hasExtra(KEY_ARG_LANDING))
            tabLayout.getTabAt(intent.getIntExtra(KEY_ARG_LANDING, 0))?.select()
        else
            changeFragment(GeneralSettingsFragment.newInstance())
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setTitle(R.string.settings_title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupTabs() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    TAB_SETTINGS_GENERAL ->
                        changeFragment(GeneralSettingsFragment.newInstance())
                    TAB_SETTINGS_SIM_ONE ->
                        changeFragment(SimSettingsFragment.newInstance(BalanceHomeFragment.FIRST_SLOT))
                    TAB_SETTINGS_SIM_TWO ->
                        changeFragment(SimSettingsFragment.newInstance(BalanceHomeFragment.SECOND_SLOT))
                    else -> {
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun changeFragment(fragment: Fragment) {
        val fm = supportFragmentManager
        fm.beginTransaction().replace(R.id.fragmentContainerFL, fragment).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }
}
