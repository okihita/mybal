package com.mybal.id.settings

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Toast

import com.mybal.id.R
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.util.Constants
import kotlinx.android.synthetic.main.widget_change_simtype_dialog.*

class ChangeSimTypeDialogFragment : DialogFragment() {

    private var simTypePref: SharedPreferences? = null
    private var simSlot: Int = 0

    companion object {

        const val GSM_PREPAID = 0
        private const val GSM_POSTPAID = 1

        fun newInstance(simSlot: Int): ChangeSimTypeDialogFragment {

            val args = Bundle()
            // 0 (first slot) or 1 (second slot)
            args.putInt(SimSettingsFragment.KEY_SETTINGS_SIM_SLOT, simSlot)

            val fragment = ChangeSimTypeDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        simTypePref = activity!!.getSharedPreferences(SimSettingsFragment.KEY_PREF_SIM_TYPE, Context.MODE_PRIVATE)
        simSlot = arguments!!.getInt(SimSettingsFragment.KEY_SETTINGS_SIM_SLOT)

        val view = View.inflate(context, R.layout.widget_change_simtype_dialog, null)
        return AlertDialog.Builder(activity).setView(view).create()
    }

    override fun onStart() {
        super.onStart()
        val simType = simTypePref!!.getInt(if (simSlot == BalanceHomeFragment.FIRST_SLOT)
            Constants.SIM_TYPE_SLOT_1
        else
            Constants.SIM_TYPE_SLOT_2, 0)

        if (simType == 0) dialog.prepaidRB.isChecked = true
        else dialog.postpaidRB.isChecked = true

        dialog.closeButton.setOnClickListener { dismiss() }
        dialog.okButton.setOnClickListener {
            if (dialog.prepaidRB.isChecked) {
                updateSimType(GSM_PREPAID)
                Toast.makeText(activity, "SIM type changed to prepaid", Toast.LENGTH_SHORT).show()
            } else {
                updateSimType(GSM_POSTPAID)
                Toast.makeText(activity, "SIM type changed to postpaid", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun updateSimType(simType: Int) {
        simTypePref!!.edit().putInt(
                when (simSlot) {
                    BalanceHomeFragment.FIRST_SLOT -> Constants.SIM_TYPE_SLOT_1
                    else -> Constants.SIM_TYPE_SLOT_2
                },
                simType).apply()
        parentFragment!!.onResume()
        dismiss()
    }
}
