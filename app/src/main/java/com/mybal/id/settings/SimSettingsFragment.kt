package com.mybal.id.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.mybal.id.R
import com.mybal.id.db.Message
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.util.Constants
import com.mybal.id.util.Magic

import kotlinx.android.synthetic.main.fragment_sim_settings.*


class SimSettingsFragment : Fragment() {

    private var customUssdPref: SharedPreferences? = null
    private var simTypePref: SharedPreferences? = null
    private var phoneNumberPref: SharedPreferences? = null

    private var simSlot: Int = 0
    private var providerId: Int = 0

    companion object {

        const val KEY_PREF_USSD_OVERRIDE = "com.mybal.id.PREF_USSD_OVERRIDE"
        const val KEY_PREF_PHONE_NUMBER = "com.mybal.id.PREF_PHONE_NUMBER"
        const val KEY_PREF_SIM_TYPE = "com.mybal.id.PREF_SIM_TYPE"
        const val KEY_PREF_XL_AXIS = "com.mybal.id.PREF_XL_AXIS"

        const val KEY_SETTINGS_SIM_SLOT = "com.mybal.id.sim_slot"

        fun newInstance(simSlot: Int): SimSettingsFragment {

            val args = Bundle()
            args.putInt(KEY_SETTINGS_SIM_SLOT, simSlot)

            val fragment = SimSettingsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sim_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        customUssdPref = activity!!.getSharedPreferences(KEY_PREF_USSD_OVERRIDE, Context.MODE_PRIVATE)
        simTypePref = activity!!.getSharedPreferences(KEY_PREF_SIM_TYPE, Context.MODE_PRIVATE)
        phoneNumberPref = activity!!.getSharedPreferences(KEY_PREF_PHONE_NUMBER, Context.MODE_PRIVATE)

        simSlot = arguments!!.getInt(KEY_SETTINGS_SIM_SLOT)

        val providerIdSlot1 = activity!!.getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE).getInt(Constants.PROVIDER_ID_SLOT_1, 0)
        val providerIdSlot2 = activity!!.getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE).getInt(Constants.PROVIDER_ID_SLOT_2, 0)

        providerId = if (simSlot == BalanceHomeFragment.FIRST_SLOT) providerIdSlot1 else providerIdSlot2


        // Setup button interactions
        phoneNumberLL.setOnClickListener({
            ChangePhoneNumberFragment.newInstance(simSlot)
                    .show(childFragmentManager, "phoneNumber")
        })

        providerLL.setOnClickListener({
            when (providerId) {
            // 3 is for XL and 4 is for Axis.
            // See /api/providers endpoint. This is stored through the Provider model in Room database.
                3, 4 -> {
                    ChangeXlAxisFragment.newInstance(simSlot).show(childFragmentManager, "xlAxis")
                }
                else -> {
                    Toast.makeText(activity, getString(R.string.simSettings_onlyIfXlAxis), Toast.LENGTH_SHORT).show()
                }
            }
        })

        simTypeLL.setOnClickListener({
            ChangeSimTypeDialogFragment.newInstance(simSlot)
                    .show(childFragmentManager, "simType")
        })

        changePulsaUssdLL.setOnClickListener({
            ChangeUssdDialogFragment.newInstance(simSlot, Message.BALANCE_TYPE_PULSA_MAIN)
                    .show(childFragmentManager, "pulsaUssd")
        })

        changeDataUssdLL.setOnClickListener({
            ChangeUssdDialogFragment.newInstance(simSlot, Message.BALANCE_TYPE_DATA_GENERIC)
                    .show(childFragmentManager, "dataUssd")
        })

        changeSmsUssdLL.setOnClickListener({
            ChangeUssdDialogFragment.newInstance(simSlot, Message.BALANCE_TYPE_SMS)
                    .show(childFragmentManager, "smsUssd")
        })

        changeCallUssdLL.setOnClickListener({
            ChangeUssdDialogFragment.newInstance(simSlot, Message.BALANCE_TYPE_CALL)
                    .show(childFragmentManager, "callUssd")
        })
    }

    override fun onResume() {
        super.onResume()

        if (providerId == 0) {

            // Hide the SIM details
            optionsContainerLL.visibility = View.GONE
            simNotAvailableTV.visibility = View.VISIBLE

        } else {

            // Show the SIM details
            optionsContainerLL.visibility = View.VISIBLE
            simNotAvailableTV.visibility = View.GONE

            changeIfXlAxis()

            // If phone number exists, show it
            val phoneNumber = phoneNumberPref?.getString(
                    if (simSlot == BalanceHomeFragment.FIRST_SLOT) Constants.PHONE_NUMBER_SLOT_1
                    else Constants.PHONE_NUMBER_SLOT_2,
                    "")
            if (phoneNumber.equals("", ignoreCase = true)) phoneNumberTV.text = "not defined"
            else phoneNumberTV!!.text = Magic.insertPeriodically(phoneNumber, " ", 4)


            // Show the operator name
            providerTV.text = MyBalDatabase.getInstance(activity).providerDao().getProviderById(providerId).name


            // Show the SIM type
            val simType = simTypePref?.getInt(
                    if (simSlot == BalanceHomeFragment.FIRST_SLOT) Constants.SIM_TYPE_SLOT_1
                    else Constants.SIM_TYPE_SLOT_2,
                    ChangeSimTypeDialogFragment.GSM_PREPAID)
            if (simType == ChangeSimTypeDialogFragment.GSM_PREPAID) simTypeTV.text = "Prepaid"
            else simTypeTV.text = "Postpaid"


            // If custom USSDs are defined, show them, else fall back to the default provider code
            val dao = MyBalDatabase.getInstance(activity).ussdCodeDao()

            val pulsaUssdFromPref = customUssdPref!!.getString(Magic.getCustomUssdPrefKey(simSlot, Message.BALANCE_TYPE_PULSA_MAIN), "")
            if (pulsaUssdFromPref != "") pulsaUssdTV.text = pulsaUssdFromPref
            else pulsaUssdTV.text = dao.getUssdCode(providerId, Message.BALANCE_TYPE_PULSA_MAIN).code

            val dataUssdFromPref = customUssdPref!!.getString(Magic.getCustomUssdPrefKey(simSlot, Message.BALANCE_TYPE_DATA_GENERIC), "")
            if (dataUssdFromPref != "") dataUssdTV.text = dataUssdFromPref
            else dataUssdTV.text = dao.getUssdCode(providerId, Message.BALANCE_TYPE_DATA_GENERIC).code

            val smsUssdFromPref = customUssdPref!!.getString(Magic.getCustomUssdPrefKey(simSlot, Message.BALANCE_TYPE_SMS), "")
            if (smsUssdFromPref != "") smsUssdTV.text = smsUssdFromPref
            else smsUssdTV.text = dao.getUssdCode(providerId, Message.BALANCE_TYPE_SMS).code

            val callUssdFromPref = customUssdPref!!.getString(Magic.getCustomUssdPrefKey(simSlot, Message.BALANCE_TYPE_CALL), "")
            if (callUssdFromPref != "") callUssdTV.text = callUssdFromPref
            else callUssdTV.text = dao.getUssdCode(providerId, Message.BALANCE_TYPE_CALL).code
        }
    }

    // If the provider ID is XL or Axis, check if there's a custom value in the Shared Preferences
    private fun changeIfXlAxis() {

        when (providerId) {
            3, 4 -> { // Provider 3 is XL, provider 4 is Axis
                val xlAxisPref = activity!!.getSharedPreferences(SimSettingsFragment.KEY_PREF_XL_AXIS, Context.MODE_PRIVATE)
                val simSlotKey = if (simSlot == BalanceHomeFragment.FIRST_SLOT) Constants.XL_AXIS_SLOT_1 else Constants.XL_AXIS_SLOT_2
                if (xlAxisPref.getInt(simSlotKey, 0) > 0) providerId = xlAxisPref.getInt(simSlotKey, 0)
            }
            else -> return
        }
    }
}
