package com.mybal.id.settings

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Toast
import com.mybal.id.R
import com.mybal.id.db.Message
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.network.RestClient
import com.mybal.id.siminfo.TelephonyInfo
import com.mybal.id.util.Constants
import kotlinx.android.synthetic.main.widget_change_ussd_dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangeUssdDialogFragment : DialogFragment() {

    private var ussdPref: SharedPreferences? = null

    private var simSlot: Int = 0
    private var balanceType: Int = 0
    private var providerId: Int = 0

    private var defaultUssdCode: String? = null
    private var customUssdCode: String? = null
    private var customUssdPrefKey: String? = null

    companion object {

        private val KEY_SIM_SLOT = "com.mybal.id.sim_slot"
        private val KEY_BALANCE_TYPE = "com.mybal.id.balance_type"

        fun newInstance(simSlot: Int, balanceType: Int): ChangeUssdDialogFragment {

            val args = Bundle()
            args.putInt(KEY_SIM_SLOT, simSlot) // 0 or 1
            args.putInt(KEY_BALANCE_TYPE, balanceType) // Look in Message.BalanceTypes

            val fragment = ChangeUssdDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        ussdPref = dialog.ownerActivity.getSharedPreferences(SimSettingsFragment.KEY_PREF_USSD_OVERRIDE, Context.MODE_PRIVATE)
        simSlot = arguments!!.getInt(KEY_SIM_SLOT)
        balanceType = arguments!!.getInt(KEY_BALANCE_TYPE)

        val view = View.inflate(context, R.layout.widget_change_ussd_dialog, null)
        return AlertDialog.Builder(activity).setView(view).create()
    }

    override fun onStart() {
        super.onStart()
        getCustomUssdPrefKey()
        loadUssdCodes() // Load both default and custom (if any) codes
        setupRadioGroup()

        closeButton.setOnClickListener { dismiss() }
        confirmButton.setOnClickListener {

            if (dialog.customRB.isChecked) {
                val newUssd = customET.text.toString()
                if (newUssd.equals("", ignoreCase = true))
                    Toast.makeText(activity, "New USSD number cannot be empty", Toast.LENGTH_SHORT).show()
                else {
                    updateCustomUssdKey(newUssd)
                    Toast.makeText(activity, "USSD Number updated", Toast.LENGTH_SHORT).show()
                }
            } else if (dialog.defaultRB.isChecked) {
                updateCustomUssdKey("")
                Toast.makeText(activity, "Using the default USSD", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getCustomUssdPrefKey() {
        when (simSlot) {

            BalanceHomeFragment.FIRST_SLOT ->
                when (balanceType) {
                    Message.BALANCE_TYPE_PULSA_MAIN -> customUssdPrefKey = Constants.USSD_NUMBER_PULSA_OP1
                    Message.BALANCE_TYPE_DATA_GENERIC -> customUssdPrefKey = Constants.USSD_NUMBER_DATA_OP1
                    Message.BALANCE_TYPE_SMS -> customUssdPrefKey = Constants.USSD_NUMBER_SMS_OP1
                    Message.BALANCE_TYPE_CALL -> customUssdPrefKey = Constants.USSD_NUMBER_CALL_OP1
                    else -> {
                    }
                }

            BalanceHomeFragment.SECOND_SLOT ->
                when (balanceType) {
                    Message.BALANCE_TYPE_PULSA_MAIN -> customUssdPrefKey = Constants.USSD_NUMBER_PULSA_OP2
                    Message.BALANCE_TYPE_DATA_GENERIC -> customUssdPrefKey = Constants.USSD_NUMBER_DATA_OP2
                    Message.BALANCE_TYPE_SMS -> customUssdPrefKey = Constants.USSD_NUMBER_SMS_OP2
                    Message.BALANCE_TYPE_CALL -> customUssdPrefKey = Constants.USSD_NUMBER_CALL_OP2
                    else -> {
                    }
                }

            else -> {
            }
        }
    }

    private fun loadUssdCodes() {
        val providerIdPref = activity!!.getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE)
        val firstProvider = providerIdPref.getInt(Constants.PROVIDER_ID_SLOT_1, 0)
        val secondProvider = providerIdPref.getInt(Constants.PROVIDER_ID_SLOT_2, 0)
        providerId = if (simSlot == BalanceHomeFragment.FIRST_SLOT) firstProvider else secondProvider
        defaultUssdCode = MyBalDatabase.getInstance(activity).ussdCodeDao().getUssdCode(providerId, balanceType).code

        // Empty if there isn't any
        customUssdCode = ussdPref!!.getString(customUssdPrefKey, "")
    }

    private fun setupRadioGroup() {

        // Show/hide the custom text
        dialog.defaultRB.text = String.format("Default (%s) ", defaultUssdCode)
        dialog.defaultRB.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) dialog.customET.visibility = View.INVISIBLE
            else dialog.customET.visibility = View.VISIBLE
        }
        dialog.customRB.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) dialog.customET.visibility = View.VISIBLE
            else dialog.customET.visibility = View.INVISIBLE
        }

        // Which one is selected at start
        if (customUssdCode == null || customUssdCode == "") {
            dialog.defaultRB.isChecked = true
        } else {
            dialog.customET.setText(customUssdCode)
            dialog.customRB.isChecked = true
        }
    }

    private fun updateCustomUssdKey(newUssd: String) {
        ussdPref!!.edit().putString(customUssdPrefKey, newUssd).apply()

        RestClient.getClient(context)
                .postComplaintSuggestions(TelephonyInfo.getInstance(dialog.ownerActivity).imsiSIM1.toString(),
                        "Changed USSD code for Provider $providerId from $defaultUssdCode to $newUssd")
                .enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {}
                    override fun onFailure(call: Call<Void>, t: Throwable) {}
                })

        parentFragment!!.onResume()
        dismiss()
    }
}
