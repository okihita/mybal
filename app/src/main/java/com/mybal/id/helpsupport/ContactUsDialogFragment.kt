package com.mybal.id.helpsupport

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.mybal.id.R
import com.mybal.id.network.RestClient
import com.mybal.id.siminfo.TelephonyInfo
import kotlinx.android.synthetic.main.widget_contact_us.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsDialogFragment : DialogFragment() {

    companion object {
        fun newInstance(): ContactUsDialogFragment {
            return ContactUsDialogFragment()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = View.inflate(context, R.layout.widget_contact_us, null)
        return AlertDialog.Builder(activity).setView(view).create()
    }

    override fun onStart() {
        super.onStart()
        dialog.suggestionET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                dialog.charCountTV.text = s.length.toString() + "/100"
                dialog.charCountTV.setTextColor(
                        if (s.length <= 90) ContextCompat.getColor(dialog.ownerActivity, R.color.textColor)
                        else ContextCompat.getColor(dialog.ownerActivity, R.color.palleteRed))
            }
        })

        dialog.submitButton.setOnClickListener { reportMessage() }
        dialog.cancelButton.setOnClickListener { dismiss() }
    }

    private fun reportMessage() {
        RestClient.getClient(context)
                .postComplaintSuggestions(TelephonyInfo.getInstance(dialog.context).imsiSIM1.toString(), dialog.suggestionET.text.toString())
                .enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        Toast.makeText(activity, "Your message has been reported. Thank you.", Toast.LENGTH_SHORT).show()
                        dismiss()
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        Toast.makeText(activity, "Message failed to send. Please try again.", Toast.LENGTH_SHORT).show()
                    }
                })
    }
}
