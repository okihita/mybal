package com.mybal.id.helpsupport

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.mybal.id.R
import kotlinx.android.synthetic.main.activity_help_support.*
import kotlinx.android.synthetic.main.widget_toolbar.*

class HelpSupportActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_help_support)

        setupToolbar()

        faqLL.setOnClickListener({
            val browserIntent = Intent(Intent.ACTION_VIEW)
            browserIntent.data = Uri.parse("http://mybal.id/faq/")
            startActivity(browserIntent)
        })

        contactLL.setOnClickListener({
            val fragment = ContactUsDialogFragment.newInstance()
            fragment.show(supportFragmentManager, "usageDetail")
        })
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setTitle(R.string.helpSupport_title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
