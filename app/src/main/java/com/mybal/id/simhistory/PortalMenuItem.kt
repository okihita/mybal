package com.mybal.id.simhistory

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.telecom.TelecomManager
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.mikepenz.fastadapter.items.AbstractItem
import com.mybal.id.R
import com.mybal.id.db.PortalMenu
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.util.Constants
import kotlinx.android.synthetic.main.item_portal_menu.view.*

class PortalMenuItem(
        val portalMenuProv1: PortalMenu?,
        val portalMenuProv2: PortalMenu?,
        private val imageRes: Int,
        private val title: String
) : AbstractItem<PortalMenuItem, PortalMenuItem.ViewHolder>() {

    var isChosen: Boolean = false

    @RequiresApi(Build.VERSION_CODES.M)
    fun callPortalMenuUssd(context: Context, cardSlot: Int, ussdCode: String) {

        // Prepare the calling intent
        val ussdCallIntent = Intent(Intent.ACTION_CALL).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        ussdCallIntent.putExtra("com.android.phone.force.slot", true)
        ussdCallIntent.data = Uri.parse("tel:" + Uri.encode(ussdCode))
        ussdCallIntent.putExtra("Cdma_Supp", true)

        // Force use the second SIM slot, depending on the Android OS version
        // https://stackoverflow.com/a/44298513
        Constants.simSlotNames.forEach { ussdCallIntent.putExtra(it, cardSlot) }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val telecomManager = context.getSystemService(Context.TELECOM_SERVICE) as TelecomManager
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return
            }

            val phoneAccountHandleList = telecomManager.callCapablePhoneAccounts
            if (phoneAccountHandleList != null && phoneAccountHandleList.size > 0)
                ussdCallIntent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandleList[cardSlot])
        }

        context.startActivity(ussdCallIntent)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun getViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }

    override fun getType(): Int {
        return R.id.fastadapter_portalmenu_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_portal_menu
    }

    // Catch commands from FastAdapter#withOnClickListener()
    override fun isEnabled(): Boolean {
        return true
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        Log.i("###", "Binding view: " + title)

        holder.titleTV.text = title
        holder.iconIV.setImageResource(imageRes)
        if (isChosen) holder.showSimSelection() else holder.hideSimSelection()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var iconIV: ImageView = itemView.iconIV
        var titleTV: TextView = itemView.titleTV
        private var largeButtonCV: CardView = itemView.largeButtonCV
        private var simSelectionLL: LinearLayout = itemView.simSelectionLL
        private var slot1CV: CardView = itemView.slot1CV
        private var slot2CV: CardView = itemView.slot2CV
        private var closeSimCV: CardView = itemView.closeSimCV

        init {

            val context = itemView.context

            hideSimSelection()

            slot1CV.setOnClickListener {
                when (portalMenuProv1) {
                    null -> Toast.makeText(context, "Operation not available", Toast.LENGTH_SHORT).show()
                    else -> callPortalMenuUssd(context, BalanceHomeFragment.FIRST_SLOT, portalMenuProv1.ussdCode)
                }
                hideSimSelection()
            }

            slot2CV.setOnClickListener {
                when (portalMenuProv2) {
                    null -> Toast.makeText(context, "Operation not available", Toast.LENGTH_SHORT).show()
                    else -> callPortalMenuUssd(context, BalanceHomeFragment.SECOND_SLOT, portalMenuProv2.ussdCode)
                }
                hideSimSelection()
            }

            closeSimCV.setOnClickListener { hideSimSelection() }
        }

        fun showSimSelection() {
            itemView.postDelayed({
                largeButtonCV.visibility = View.INVISIBLE
                simSelectionLL.visibility = View.VISIBLE
            }, 300)
        }

        fun hideSimSelection() {
            itemView.postDelayed({
                largeButtonCV.visibility = View.VISIBLE
                simSelectionLL.visibility = View.INVISIBLE
            }, 300)
        }
    }
}
