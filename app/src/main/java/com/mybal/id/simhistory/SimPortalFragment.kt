package com.mybal.id.simhistory

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mybal.id.R
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.util.Constants
import kotlinx.android.synthetic.main.fragment_sim_portal.*
import java.util.*

class SimPortalFragment : Fragment() {

    companion object {
        fun newInstance(): SimPortalFragment {
            return SimPortalFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sim_portal, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onStart() {

        super.onStart()

        val pref = activity!!.getSharedPreferences(Constants.PREF_KEY_PROVIDER_ID, Context.MODE_PRIVATE)
        val provider1 = pref.getInt(Constants.PROVIDER_ID_SLOT_1, 0)
        val provider2 = pref.getInt(Constants.PROVIDER_ID_SLOT_2, 0)

        val dao = MyBalDatabase.getInstance(context).portalMenuDao()
        val portalMenuItems = ArrayList<PortalMenuItem>()

        portalMenuItems.add(PortalMenuItem(
                dao.getPortalMenuForProviderFunction(provider1, Constants.PORTAL_SPECIAL_OFFERS),
                dao.getPortalMenuForProviderFunction(provider2, Constants.PORTAL_SPECIAL_OFFERS),
                R.drawable.ic_simportal_special_offers, getString(R.string.simPortal_specialOffers)))

        portalMenuItems.add(PortalMenuItem(
                dao.getPortalMenuForProviderFunction(provider1, Constants.PORTAL_BUY_DATA),
                dao.getPortalMenuForProviderFunction(provider2, Constants.PORTAL_BUY_DATA),
                R.drawable.ic_simportal_buy_data_pack, getString(R.string.simPortal_buyDataPack)))

        portalMenuItems.add(PortalMenuItem(
                dao.getPortalMenuForProviderFunction(provider1, Constants.PORTAL_BUY_CALL),
                dao.getPortalMenuForProviderFunction(provider2, Constants.PORTAL_BUY_CALL),
                R.drawable.ic_simportal_buy_call_pack, getString(R.string.simPortal_buyCallPack)))

        portalMenuItems.add(PortalMenuItem(
                dao.getPortalMenuForProviderFunction(provider1, Constants.PORTAL_PORTAL_MENU),
                dao.getPortalMenuForProviderFunction(provider2, Constants.PORTAL_PORTAL_MENU),
                R.drawable.ic_simportal_portal_menu, getString(R.string.simPortal_portalMenu)))

        portalMenuItems.add(PortalMenuItem(
                dao.getPortalMenuForProviderFunction(provider1, Constants.PORTAL_SHARE_TALK_TIME),
                dao.getPortalMenuForProviderFunction(provider2, Constants.PORTAL_SHARE_TALK_TIME),
                R.drawable.ic_simportal_share_talk_time, getString(R.string.simPortal_shareTalkTime)))

        val itemAdapter: ItemAdapter<IItem<*, *>> = ItemAdapter()
        val fastAdapter = FastAdapter.with<IItem<*, *>, ItemAdapter<IItem<*, *>>>(itemAdapter)

        menuRV.layoutManager = LinearLayoutManager(activity)
        menuRV.setHasFixedSize(true)
        menuRV.adapter = fastAdapter

        itemAdapter.add(portalMenuItems as List<IItem<*, *>>)

        fastAdapter.withSelectable(true)
        fastAdapter.withOnClickListener { _, adapter, item, _ ->

            item as PortalMenuItem

            // If the SIM Card selection is not shown...
            if (!item.isChosen) {

                // Disable other SIM selection state
                adapter.adapterItems.forEach { (it as PortalMenuItem).isChosen = false }
                if (item.portalMenuProv2 != null) item.isChosen = true
                else when (item.portalMenuProv1) {
                    null -> Toast.makeText(context, "Operation not available", Toast.LENGTH_SHORT).show()
                    else -> item.callPortalMenuUssd(activity!!, BalanceHomeFragment.FIRST_SLOT, item.portalMenuProv1.ussdCode)
                }

                fastAdapter.notifyAdapterDataSetChanged()
            }

            true
        }
    }
}
