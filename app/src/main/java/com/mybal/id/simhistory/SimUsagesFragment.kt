package com.mybal.id.simhistory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mybal.id.R
import com.mybal.id.db.MyBalDatabase
import com.mybal.id.db.SimUsage
import com.mybal.id.util.Constants
import kotlinx.android.synthetic.main.fragment_sim_transaction.*
import java.text.ParseException
import java.util.*

class SimUsagesFragment : Fragment() {

    private lateinit var db: MyBalDatabase
    private var fragmentIndex: Int = 0

    private lateinit var itemAdapter: ItemAdapter<IItem<*, *>>

    companion object {

        const val TAB_USAGE_ALL = 0
        const val TAB_USAGE_PULSA = 1
        const val TAB_USAGE_DATA = 2
        const val TAB_USAGE_TOPUP = 3
        const val TAB_USAGE_CALL = 4
        const val TAB_USAGE_SMS = 5

        private val KEY_FRAGMENT_INDEX = "com.mybal.id.fragment_index"

        fun newInstance(fragmentIndex: Int): SimUsagesFragment {

            val args = Bundle()
            args.putInt(KEY_FRAGMENT_INDEX, fragmentIndex)

            val fragment = SimUsagesFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sim_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentIndex = arguments!!.getInt(KEY_FRAGMENT_INDEX)
        db = MyBalDatabase.getInstance(activity)

        setupRV()
        reloadHistory()
    }

    private fun setupRV() {

        itemAdapter = ItemAdapter()
        val fastAdapter = FastAdapter.with<IItem<*, *>, ItemAdapter<IItem<*, *>>>(itemAdapter)
        fastAdapter.withSelectable(true)
        fastAdapter.setHasStableIds(true)

        usagesRV.layoutManager = LinearLayoutManager(activity)
        usagesRV.itemAnimator = DefaultItemAnimator()
        usagesRV.adapter = fastAdapter

        fastAdapter.withOnClickListener { _, _, item, _ ->

            if (item is SimUsageItem) {
                // On click, add the list of patterned messages into the SimUsage object
                item.simUsage.labelTexts = MyBalDatabase.getInstance(activity).simUsageDao()
                        .getLabelTextsForSimUsage(item.simUsage.id)

                val fragment = SimUsageDetailDialogFragment.newInstance(item)
                fragment.show(childFragmentManager, "usageDetail")
            }

            true
        }

        fastAdapter.withOnLongClickListener { _, _, item, _ ->

            if (item is SimUsageItem) {
                AlertDialog.Builder(activity!!)
                        .setTitle("Delete Item")
                        .setMessage("Are you sure you want to delete this item?")
                        .setPositiveButton("Yes") { _, _ ->
                            db.simUsageDao().deleteSingleSimUsage(item.simUsage)
                            fastAdapter.notifyAdapterDataSetChanged()
                            activity!!.recreate()
                        }
                        .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
                        .show()
            }

            true
        }
    }

    private fun reloadHistory() {

        var simUsages: List<SimUsage> = ArrayList()
        var headerDate = ""
        val dao = db.simUsageDao()

        when (fragmentIndex) {

            TAB_USAGE_ALL ->
                simUsages = dao.loadAllLatestFirst()

            TAB_USAGE_PULSA ->
                simUsages = dao.loadByTransactionTypes(SimUsage.INQUIRY_BALANCE_PULSA, SimUsage.PURCHASE_PULSA_PACK)

            TAB_USAGE_DATA ->
                simUsages = dao.loadByTransactionTypes(SimUsage.INQUIRY_BALANCE_DATA, SimUsage.PURCHASE_DATA_PACK)

            TAB_USAGE_TOPUP ->
                simUsages = dao.loadByTransactionTypes(
                        SimUsage.PURCHASE_PULSA_PACK,
                        SimUsage.PURCHASE_DATA_PACK,
                        SimUsage.PURCHASE_SMS_PACK,
                        SimUsage.PURCHASE_CALL_PACK
                )

            TAB_USAGE_CALL ->
                simUsages = dao.loadByTransactionTypes(SimUsage.INQUIRY_BALANCE_CALL, SimUsage.PURCHASE_CALL_PACK)

            TAB_USAGE_SMS ->
                simUsages = dao.loadByTransactionTypes(SimUsage.INQUIRY_BALANCE_SMS, SimUsage.PURCHASE_SMS_PACK)

            else -> {
            }
        }

        simUsages.forEach { simUsage ->
            try {
                val itemDate = Constants.humanDateFormat.format(Constants.dateStorageFormat.parse(simUsage.createdAt))
                if (headerDate != itemDate) {
                    headerDate = itemDate
                    itemAdapter.add(SimUsageHeader(headerDate))
                }
            } catch (e: ParseException) {
                Log.wtf("###", "reloadHistory: Failed. " + e.localizedMessage)
            }

            itemAdapter.add(SimUsageItem(simUsage))
        }
    }
}
