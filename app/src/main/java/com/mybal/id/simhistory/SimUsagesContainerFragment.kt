package com.mybal.id.simhistory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mybal.id.R

import kotlinx.android.synthetic.main.fragment_sim_usage_container.*

class SimUsagesContainerFragment : Fragment() {

    companion object {
        fun newInstance(): SimUsagesContainerFragment {
            return SimUsagesContainerFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sim_usage_container, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTabs()
    }

    private fun setupTabs() {
        viewPager.adapter = FragmentPagerAdapter(childFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

    inner class FragmentPagerAdapter internal constructor(fm: FragmentManager) : android.support.v4.app.FragmentPagerAdapter(fm) {

        private val tabTitles = arrayOf(getString(R.string.simUsage_tabTitle_all), getString(R.string.simUsage_tabTitle_pulsa), getString(R.string.simUsage_tabTitle_data), getString(R.string.simUsage_tabTitle_topup), getString(R.string.simUsage_tabTitle_call), getString(R.string.simUsage_tabTitle_sms))

        override fun getCount(): Int {
            return tabTitles.size
        }

        override fun getItem(position: Int): Fragment? {
            return SimUsagesFragment.newInstance(position)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return tabTitles[position]
        }
    }
}
