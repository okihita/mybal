package com.mybal.id.simhistory

import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.mikepenz.fastadapter.items.AbstractItem
import com.mybal.id.R
import com.mybal.id.db.SimUsage
import com.mybal.id.home.balance.BalanceHomeFragment
import com.mybal.id.util.Constants
import com.mybal.id.util.Magic
import kotlinx.android.synthetic.main.item_sim_usage.view.*
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * This class uses the SimUsage datatype to put into FastAdapter.
 * Read the corresponding [SimUsage] class to understand more.
 */
class SimUsageItem(val simUsage: SimUsage) : AbstractItem<SimUsageItem, SimUsageItem.ViewHolder>() {

    lateinit var title: String
    lateinit var subtitle: String

    override fun getViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }

    override fun getType(): Int {
        return R.id.fastadapter_simusage_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_sim_usage
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)

        val c = holder.itemView.context

        // LEFT ICON
        when (simUsage.transactionType) {

            SimUsage.INQUIRY_BALANCE_PULSA ->
                holder.leftIconIV.setImageResource(R.drawable.ic_card_phone)

            SimUsage.INQUIRY_BALANCE_DATA ->
                holder.leftIconIV.setImageResource(R.drawable.ic_card_data_active)

            SimUsage.INQUIRY_BALANCE_SMS ->
                holder.leftIconIV.setImageResource(R.drawable.ic_card_sms_active)

            SimUsage.INQUIRY_BALANCE_CALL ->
                holder.leftIconIV.setImageResource(R.drawable.ic_card_call_active)

            SimUsage.PURCHASE_PULSA_PACK,
            SimUsage.PURCHASE_DATA_PACK,
            SimUsage.PURCHASE_SMS_PACK,
            SimUsage.PURCHASE_CALL_PACK ->
                holder.leftIconIV.setImageResource(R.drawable.wallet_icon_pay)

            else -> {
            }
        }


        // SIM ICON
        when (simUsage.slot) {

            BalanceHomeFragment.FIRST_SLOT ->
                holder.simSlotTV.setImageResource(R.drawable.ic_history_slot1)

            BalanceHomeFragment.SECOND_SLOT ->
                holder.simSlotTV.setImageResource(R.drawable.ic_history_slot2)

            else -> {
            }
        }


        // MESSAGE TITLE
        // https://stackoverflow.com/a/39639103
        val localNumberFormat = NumberFormat
                .getInstance(if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    c.resources.configuration.locales.get(0)
                else
                    c.resources.configuration.locale)
                .format(simUsage.amount)

        when (simUsage.transactionType) {

            SimUsage.INQUIRY_BALANCE_PULSA ->
                title = String.format(c.getString(R.string.simUsage_title_inquiryPulsa), simUsage.getUnitName(c), localNumberFormat)
            SimUsage.INQUIRY_BALANCE_DATA ->
                title = String.format(c.getString(R.string.simUsage_title_inquiryData), localNumberFormat, simUsage.getUnitName(c))
            SimUsage.INQUIRY_BALANCE_SMS ->
                title = String.format(c.getString(R.string.simUsage_title_inquirySms), simUsage.amount.toInt(), simUsage.getUnitName(c))
            SimUsage.INQUIRY_BALANCE_CALL ->
                title = String.format(c.getString(R.string.simUsage_title_inquiryCall), simUsage.amount.toInt(), simUsage.getUnitName(c))

            SimUsage.PURCHASE_PULSA_PACK ->
                title = String.format(c.getString(R.string.simUsage_title_purchasePulsa), simUsage.getUnitName(c), localNumberFormat)
            SimUsage.PURCHASE_DATA_PACK ->
                title = String.format(c.getString(R.string.simUsage_title_purchaseData), localNumberFormat, simUsage.getUnitName(c))
            SimUsage.PURCHASE_SMS_PACK ->
                title = String.format(c.getString(R.string.simUsage_title_purchaseSms), simUsage.amount.toInt(), simUsage.getUnitName(c))
            SimUsage.PURCHASE_CALL_PACK ->
                title = String.format(c.getString(R.string.simUsage_title_purchaseCall), simUsage.amount.toInt(), simUsage.getUnitName(c))

            else -> {
            }
        }

        if (simUsage.noPack) title = "You have no pack"
        holder.titleTV.text = title


        // MESSAGE SUBTITLE
        when (simUsage.transactionType) {

            SimUsage.INQUIRY_BALANCE_PULSA,
            SimUsage.INQUIRY_BALANCE_DATA,
            SimUsage.INQUIRY_BALANCE_SMS,
            SimUsage.INQUIRY_BALANCE_CALL,

            SimUsage.PURCHASE_PULSA_PACK,
            SimUsage.PURCHASE_DATA_PACK,
            SimUsage.PURCHASE_SMS_PACK,
            SimUsage.PURCHASE_CALL_PACK -> {

                subtitle = String.format(c.getString(R.string.simUsage_subtitle_validUntil), Magic.expiryCountdown(c, simUsage))
                if (simUsage.noPack) subtitle = "Expiry not available"
                if (simUsage.expiry.equals(SimUsage.EXPIRY_DATE_UNKNOWN, ignoreCase = true) || simUsage.expiry.equals("", ignoreCase = true))
                    subtitle = "n/a"
                holder.descriptionTV.text = subtitle

            }
            else -> {
            }
        }


        // RIGHT ICON
        when (simUsage.transactionType) {
            SimUsage.PURCHASE_PULSA_PACK,
            SimUsage.PURCHASE_DATA_PACK,
            SimUsage.PURCHASE_SMS_PACK,
            SimUsage.PURCHASE_CALL_PACK -> {
                holder.rightIconIV.visibility = View.VISIBLE
                holder.rightIconIV.setImageResource(R.drawable.ic_recharge_thunder)
            }
            else -> holder.rightIconIV.visibility = View.GONE
        }


        // RIGHT HOUR-MINUTE
        try {
            holder.timeTV.text = SimpleDateFormat("HH:mm", Locale.getDefault())
                    .format(Constants.dateStorageFormat.parse(simUsage.createdAt))
        } catch (e: ParseException) {
            holder.timeTV.setText(R.string.simUsage_defaultHourMinute)
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var leftIconIV: ImageView = itemView.leftIconIV
        var simSlotTV: ImageView = itemView.simSlotIV
        var titleTV: TextView = itemView.titleTV
        var descriptionTV: TextView = itemView.descriptionTV
        var timeTV: TextView = itemView.timeTV
        var rightIconIV: ImageView = itemView.rightIconIV
    }
}
