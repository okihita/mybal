package com.mybal.id.simhistory

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.mybal.id.R
import com.mybal.id.db.MyBalDatabase
import kotlinx.android.synthetic.main.activity_sim_history.*
import kotlinx.android.synthetic.main.widget_toolbar.*

class SimHistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sim_history)

        setupToolbar()
        setupTabs()

        changeFragment(SimUsagesContainerFragment.newInstance())
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setTitle(R.string.simHistory_title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupTabs() {
        viewPager.adapter = FragmentPagerAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }

    /**
     * Change the current fragment into the one selected.
     */
    private fun changeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.viewPager, fragment).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.sim_history, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            android.R.id.home -> {
                finish()
                return true
            }

            R.id.resetHistory -> {
                AlertDialog.Builder(this)
                        .setTitle(R.string.simHistory_resetConfirmation)
                        .setMessage(R.string.simHistory_resetExplanation)
                        .setPositiveButton(R.string.simHistory_confirmReset) { _, _ -> clearAllHistory() }
                        .setNegativeButton(R.string.simHistory_cancelReset) { dialog, _ -> dialog.dismiss() }
                        .show()

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun clearAllHistory() {
        MyBalDatabase.getInstance(this).simUsageDao().apply {
            nukeSimUsageTable()
            nukeSimUsageLabelTextTable()
        }
        startActivity(Intent(this, SimHistoryActivity::class.java))
        finish()
    }

    inner class FragmentPagerAdapter internal constructor(fm: FragmentManager) : android.support.v4.app.FragmentPagerAdapter(fm) {

        private val tabTitles = arrayOf(getString(R.string.simHistory_tabTitle_simUsage), getString(R.string.simHistory_tabTitle_simPortal))

        private val tabSimUsage = 0
        private val tabSimPortal = 1

        override fun getCount(): Int {
            return tabTitles.size
        }

        override fun getItem(position: Int): Fragment? {
            return when (position) {
                tabSimUsage -> SimUsagesContainerFragment.newInstance()
                tabSimPortal -> SimPortalFragment.newInstance()
                else -> null
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitles[position]
        }
    }
}
