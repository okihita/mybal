package com.mybal.id.simhistory

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.mikepenz.fastadapter.items.AbstractItem
import com.mybal.id.R
import kotlinx.android.synthetic.main.item_sim_usage_header.view.*

class SimUsageHeader(private val date: String) : AbstractItem<SimUsageHeader, SimUsageHeader.ViewHolder>() {

    override fun getViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }

    override fun getType(): Int {
        return R.id.fastadapter_simusageheader_id
    }

    override fun getLayoutRes(): Int {
        return R.layout.item_sim_usage_header
    }

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)
        holder.dateTV.text = date
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dateTV: TextView = itemView.dateTV
    }
}
