package com.mybal.id.simhistory

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.mybal.id.R
import com.mybal.id.db.SimUsage
import com.mybal.id.network.RestClient
import com.mybal.id.siminfo.TelephonyInfo
import com.mybal.id.util.Magic
import kotlinx.android.synthetic.main.widget_sim_usage_detail.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SimUsageDetailDialogFragment : DialogFragment() {

    private var msgId: Int = 0
    private var msgRegexId: Int = 0

    companion object {

        private val KEY_ITEM_TITLE = "com.mybal.id.item_title"
        private val KEY_ITEM_SUBTITLE = "com.mybal.id.item_subtitle"
        private val KEY_SIM_USAGE = "com.mybal.id.labeled_patterns"

        fun newInstance(simUsageItem: SimUsageItem): SimUsageDetailDialogFragment {

            val args = Bundle()

            args.putString(KEY_ITEM_TITLE, simUsageItem.title)
            args.putString(KEY_ITEM_SUBTITLE, simUsageItem.subtitle)
            args.putString(KEY_SIM_USAGE, GsonBuilder().create().toJson(simUsageItem.simUsage))

            val fragment = SimUsageDetailDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = View.inflate(context, R.layout.widget_sim_usage_detail, null)
        return AlertDialog.Builder(activity).setView(view).create()
    }

    override fun onStart() {
        super.onStart()
        dialog.briefTV.text = arguments!!.getString(KEY_ITEM_TITLE) + "\n" + arguments!!.getString(KEY_ITEM_SUBTITLE)

        val typeOfHashMap = object : TypeToken<SimUsage>() {

        }.type
        val simUsage = GsonBuilder().create()
                .fromJson<SimUsage>(arguments!!.getString(KEY_SIM_USAGE), typeOfHashMap)

        val labels = StringBuilder()
        simUsage.labelTexts.forEach {
            if (it.label == Magic.KEY_FOUND_MESSAGE_ID)
                msgId = Integer.valueOf(it.text)!!
            if (it.label == Magic.KEY_FOUND_REGEX_ID)
                msgRegexId = Integer.valueOf(it.text)!!
            labels.append(it.text).append("\n")
        }

        dialog.fullMessageTV.text = simUsage.originalMessage
        dialog.patternListTV.text = labels
        dialog.charCountTV.text = "0/100"

        dialog.reportButton.setOnClickListener { displaySuggestionBox() }
        dialog.closeButton.setOnClickListener { dismiss() }

    }

    private fun displaySuggestionBox() {
        dialog.reportPromptContainerLL.visibility = View.GONE
        dialog.suggestionContainerLL.visibility = View.VISIBLE

        dialog.suggestionET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                dialog.charCountTV.text = s.length.toString() + "/100"
                dialog.charCountTV.setTextColor(
                        if (s.length > 90) ContextCompat.getColor(dialog.ownerActivity, R.color.palleteRed)
                        else ContextCompat.getColor(dialog.ownerActivity, R.color.textColor))
            }
        })

        dialog.submitButton.setOnClickListener { reportMessage() }
        dialog.cancelButton.setOnClickListener { dismiss() }
    }

    private fun reportMessage() {
        RestClient.getClient(context)
                .postReportMessage(msgId, msgRegexId, TelephonyInfo.getInstance(dialog.ownerActivity).imsiSIM1.toString(), dialog.suggestionET.text.toString())
                .enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        Toast.makeText(activity, "Message successfully reported. Thank you.", Toast.LENGTH_SHORT).show()
                        dismiss()
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        Toast.makeText(activity, "Message failed to report. Please try again.", Toast.LENGTH_SHORT).show()
                    }
                })
    }
}
